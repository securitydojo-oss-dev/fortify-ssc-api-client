#!/usr/bin/env python

# coding: utf-8

from datetime import datetime
from typing import overload
from fortifyclientapi.controller.controller import Controller
from fortifyclientapi.sscclientapi.endpoints.auth_entity_of_project_version_controller_api import AuthEntityOfProjectVersionControllerApi
from fortifyclientapi.sscclientapi.models.api_result_list_authentication_entity import ApiResultListAuthenticationEntity
from fortifyclientapi.sscclientapi.models.authentication_entity import AuthenticationEntity
from fortifyclientapi.sscclientapi.rest import ApiException

from fortifyclientapi.controller.controller_exception import ControllerException


class ProjectVersionAuthEntityController(Controller):
    """Project Version Auth Entity API Controller
    """

    def get_user_access_list(self, project_version_id: int) -> list[AuthenticationEntity]:
        auth_entities: list[AuthenticationEntity] = None
        
        try:
            response: ApiResultListAuthenticationEntity = AuthEntityOfProjectVersionControllerApi(self.api).list_auth_entity_of_project_version(
                project_version_id
            )

            if response.error_code == None and response.response_code == 200:
                auth_entities = response.data
            else:
                raise ControllerException(
                    "ProjectVersionAuthEntityController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionAuthEntityController",
                status="error",
                reason="{}".format(e)
            )

        return auth_entities
    
    def update_user_access_list(self, project_version_id: int, user_list: list[AuthenticationEntity]) -> list[AuthenticationEntity]:
        auth_entities: list[AuthenticationEntity] = None
        
        try:
            response: ApiResultListAuthenticationEntity = AuthEntityOfProjectVersionControllerApi(self.api).update_collection_auth_entity_of_project_version(
                async_req=False, parent_id=project_version_id, body=user_list
            )

            if response.error_code == None and response.response_code == 200:
                auth_entities = response.data
            else:
                raise ControllerException(
                    "ProjectVersionAuthEntityController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionAuthEntityController",
                status="error",
                reason="{}".format(e)
            )

        return auth_entities

    def get_user_access(self, project_version_id: int, user_id: int) -> AuthenticationEntity:
        auth_entity: AuthenticationEntity = None
        
        try:
            response: ApiResultListAuthenticationEntity = AuthEntityOfProjectVersionControllerApi(self.api).read_auth_entity_of_project_version(
                async_req=False, parent_id=project_version_id, id=user_id
            )

            if response.error_code == None and response.response_code == 200:
                auth_entity = response.data
            else:
                raise ControllerException(
                    "ProjectVersionAuthEntityController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionAuthEntityController",
                status="error",
                reason="{}".format(e)
            )

        return auth_entity
            