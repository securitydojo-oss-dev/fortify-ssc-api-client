#!/usr/bin/env python

# coding: utf-8

from re import A
from fortifyclientapi.controller.controller import Controller
from fortifyclientapi.sscclientapi.endpoints.attribute_of_project_version_controller_api import \
    AttributeOfProjectVersionControllerApi
from fortifyclientapi.sscclientapi.models.api_result_attribute import ApiResultAttribute
from fortifyclientapi.sscclientapi.models.api_result_list_attribute import ApiResultListAttribute
from fortifyclientapi.sscclientapi.models.attribute import Attribute
from fortifyclientapi.sscclientapi.rest import ApiException
from fortifyclientapi.controller.controller_exception import ControllerException


class ProjectVersionAttributeController(Controller):
    """Project Version Attribute API Controller
    """

# Parameter as an Attribute
    def create_project_version_attribute(
        self,
        project_version_id: int,
        attribute: Attribute
    ) -> Attribute:
        project_version_attribute: Attribute = None
        try:
            response: ApiResultAttribute = AttributeOfProjectVersionControllerApi(self.api).create_attribute_of_project_version(
                body=attribute if attribute.id is None else Attribute(attribute_definition_id=attribute.attribute_definition_id, value=attribute.value, values=attribute.values), 
                parent_id=project_version_id, async_req=False
            )

            if response.error_code == None and response.response_code == 201:
                project_version_attribute = response.data
            else:
                raise ControllerException(
                    "ProjectVersionAttributeController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionAttributeController",
                status="error",
                reason="{}".format(e)
            )

        return project_version_attribute

    def update_project_version_attribute(self, project_version_id: int, *attributes: Attribute) -> list[Attribute]:
        project_version_list_attributes: list[Attribute] = None
        try:
            response: ApiResultListAttribute = AttributeOfProjectVersionControllerApi(self.api).update_collection_attribute_of_project_version(
                parent_id=project_version_id, body=attributes, async_req=False
            )

            if response.error_code == None and response.response_code == 200:
                project_version_list_attributes = response.data
            else:
                raise ControllerException(
                    "ProjectVersionAttributeController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionAttributeController",
                status="error",
                reason="{}".format(e)
            )

        return project_version_list_attributes

    def get_project_version_attributes(self, project_version_id: int) -> list[Attribute]:
        project_version_list_attributes: list[Attribute] = None
        try:
            response: ApiResultListAttribute = AttributeOfProjectVersionControllerApi(self.api).list_attribute_of_project_version(
                parent_id=project_version_id, async_req=False
            )

            if response.error_code == None and response.response_code == 200:
                project_version_list_attributes = response.data
            else:
                raise ControllerException(
                    "ProjectVersionAttributeController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionAttributeController",
                status="error",
                reason="{}".format(e)
            )

        return project_version_list_attributes

    def create_or_update_project_version_attributes(
        self,
        project_version_id: int,
        *attributes: Attribute
    ) -> list[Attribute]:
        list_project_version_attributes: list[Attribute] = []

        try:
            project_version_attributes: list[Attribute] = self.get_project_version_attributes(
                project_version_id=project_version_id
            )

            # list_project_version_attributes = project_version_attributes

            for attribute in attributes:
                update_flag: bool = False

                for project_version_attribute in project_version_attributes:
                    if attribute.attribute_definition_id == project_version_attribute.attribute_definition_id:
                        update_flag = True
                        break

                if not update_flag:
                    list_project_version_attributes += [self.create_project_version_attribute(
                        project_version_id,
                        attribute
                    )]
                else:
                    list_project_version_attributes += self.update_project_version_attribute(
                        project_version_id,
                        attribute
                    )
        except (ControllerException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionAttributeController",
                status="error",
                reason="{}".format(e)
            )

        return list_project_version_attributes
