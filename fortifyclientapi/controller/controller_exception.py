#!/usr/bin/env python

# coding: utf-8


class ControllerException(Exception):

    def __init__(self, controller, status=None, reason=None, http_resp=None):
        if http_resp:
            self.controller = controller
            self.status = http_resp.status
            self.reason = http_resp.reason
            self.body = http_resp.data
            self.headers = http_resp.getheaders()
        else:
            self.controller = controller
            self.status = status
            self.reason = reason
            self.body = None
            self.headers = None

    def __str__(self):
        """Custom error messages for exception"""
        error_message = "{0} ({1}):\n"\
                        "Reason: {2}\n".format(
                            self.controller, self.status, self.reason)

        return error_message
