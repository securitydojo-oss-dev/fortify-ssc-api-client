#!/usr/bin/env python

# coding: utf-8


from fortifyclientapi.helper.configuration_helper import ConfigurationHelper
from fortifyclientapi.sscclientapi.api_client import ApiClient


class Controller(object):

    def __init__(self, configuration: ConfigurationHelper) -> None:
        super().__init__()

        self.configuration: ConfigurationHelper = configuration
        self.api: ApiClient = ApiClient(self.configuration)
