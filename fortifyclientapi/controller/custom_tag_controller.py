#!/usr/bin/env python

# coding: utf-8


from typing import overload
from fortifyclientapi.controller.controller import Controller
from fortifyclientapi.sscclientapi.endpoints.custom_tag_controller_api import \
    CustomTagControllerApi

from fortifyclientapi.sscclientapi.models.api_result_custom_tag import ApiResultCustomTag
from fortifyclientapi.sscclientapi.models.api_result_list_custom_tag import ApiResultListCustomTag

from fortifyclientapi.sscclientapi.models.custom_tag import CustomTag
from fortifyclientapi.sscclientapi.rest import ApiException

from fortifyclientapi.controller.controller_exception import ControllerException


class CustomTagController(Controller):
    """Custom Tag API Controller
    """

    def get_custom_tag_by_name(self, name: str) -> CustomTag:
        """Get the Custom Tag by a given name

        Args:
            name (str): Custom Tag name

        Raises:
            ControllerException: Custom Tag API Controller exception

        Returns:
            IssueTemplate: the Custom Tag found
        """
        custom_tag: CustomTag = None

        try:
            response: ApiResultListCustomTag = CustomTagControllerApi(self.api).list_custom_tag(
                async_req=False, limit=1, q="name:{}".format(name)
            )

            if response.error_code == None and response.response_code == 200:
                custom_tag = response.data[0] if response.data != [
                ] and response.data != None else None
            else:
                raise ControllerException(
                    "CustomTagController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "CustomTagController",
                status="error",
                reason="{}".format(e)
            )

        return custom_tag

    def get_custom_tag_by_id(self, id: int) -> CustomTag:
        """Get the Issue Template by a given id

        Args:
            id (int): Custom Tag ID

        Raises:
            ControllerException: Custom Tag API Controller exception

        Returns:
            IssueTemplate: the Custom Tag found
        """
        custom_tag: CustomTag = None

        try:
            response: ApiResultCustomTag = CustomTagControllerApi(self.api).read_custom_tag(
                async_req=False, id=id
            )

            if response.error_code == None and response.response_code == 200:
                custom_tag = response.data
            else:
                raise ControllerException(
                    "CustomTagController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "CustomTagController",
                status="error",
                reason="{}".format(e)
            )

        return custom_tag
