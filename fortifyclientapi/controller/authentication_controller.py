#!/usr/bin/env python

# coding: utf-8


from typing import Tuple, overload
from fortifyclientapi.controller.controller import Controller
from fortifyclientapi.controller.controller_exception import ControllerException
from fortifyclientapi.helper.configuration_helper import ConfigurationHelper
from fortifyclientapi.sscclientapi.rest import ApiException
from fortifyclientapi.sscclientapi.endpoints.auth_token_controller_api import AuthTokenControllerApi
from fortifyclientapi.sscclientapi.models.authentication_token import AuthenticationToken
from fortifyclientapi.sscclientapi.models.api_result_authentication_token import ApiResultAuthenticationToken
from fortifyclientapi.sscclientapi.models.api_result_void import ApiResultVoid


class AuthenticationController(Controller):
    """Controller to manage the authentication to the API 
    """

    def authenticate(self) -> ConfigurationHelper:
        """Authenticate to the API regaring the Configuration
        Authentication is necessary if the configuration only contains a username and a password

        Raises:
            ControllerException: Error during the authentication request

        Returns:
            dict: the Auth Settings as a dict
        """
        if (self.configuration.api_key != {}):
            return self.configuration
        else:
            try:
                response: ApiResultAuthenticationToken = AuthTokenControllerApi(self.api).create_auth_token(
                    AuthenticationToken(
                        type="UnifiedLoginToken",
                        description="REST API Client Token - FortifyClientAPI"
                    ), async_req=False)

                if response.error_code == None and response.response_code == 201:
                    self.configuration.api_key["Authorization"] = response.data.token
                    self.configuration.api_key_prefix['Authorization'] = "FortifyToken"
                    self.configuration.token_id = response.data.id
                else:
                    raise ControllerException(
                        "AuthenticationController",
                        http_resp=response
                    )
            except (ApiException, ValueError) as e:
                raise ControllerException(
                    "AuthenticationController",
                    status="error - [{}]".format(e.status or "ValueError"),
                    reason="{}".format(e.reason or e)
                )
            return self.configuration

    def remove_token(self, authentication_token: AuthenticationToken):
        """Removes the generated token by the authenticate method

        Args:
            authentication_token (AuthenticationToken): The token to revoke and delete

        Raises:
            ControllerException: Error during the token removal request
        """
        try:
            response: ApiResultVoid = AuthTokenControllerApi(self.api).delete_auth_token(
                authentication_token.id, async_req=False)

            if response.error_code == None and response.response_code == 200:
                self.configuration.api_key["Authorization"] = ""
            else:
                raise ControllerException(
                    "AuthenticationController",
                    http_resp=response
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "AuthenticationController",
                status="error - [{}]".format(e.status or "ValueError"),
                reason="{}".format(e.reason or e)
            )

    def remove_token_by_id(self, authentication_token_id: int):
        """Removes the generated token by the authenticate method

        Args:
            authentication_token_id (int): The token id to revoke and delete

        Raises:
            ControllerException: Error during the token removal request
        """
        try:
            response: ApiResultVoid = AuthTokenControllerApi(self.api).delete_auth_token(
                authentication_token_id, async_req=False)

            if response.error_code == None and response.response_code == 200:
                self.configuration.api_key["Authorization"] = ""
            else:
                raise ControllerException(
                    "AuthenticationController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "AuthenticationController",
                status="error",
                reason="{}".format(e)
            )
