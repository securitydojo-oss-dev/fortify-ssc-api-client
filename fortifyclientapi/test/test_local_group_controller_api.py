# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.local_group_controller_api import LocalGroupControllerApi  # noqa: E501
from swagger_client.rest import ApiException


class TestLocalGroupControllerApi(unittest.TestCase):
    """LocalGroupControllerApi unit test stubs"""

    def setUp(self):
        self.api = LocalGroupControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_list_local_group(self):
        """Test case for list_local_group

        list  # noqa: E501
        """
        pass

    def test_read_local_group(self):
        """Test case for read_local_group

        read  # noqa: E501
        """
        pass

    def test_update_local_group(self):
        """Test case for update_local_group

        update  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
