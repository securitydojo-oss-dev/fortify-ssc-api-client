# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.folder_controller_api import FolderControllerApi  # noqa: E501
from swagger_client.rest import ApiException


class TestFolderControllerApi(unittest.TestCase):
    """FolderControllerApi unit test stubs"""

    def setUp(self):
        self.api = FolderControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_folder(self):
        """Test case for get_folder

        get  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
