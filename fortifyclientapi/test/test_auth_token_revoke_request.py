# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.models.auth_token_revoke_request import AuthTokenRevokeRequest  # noqa: E501
from swagger_client.rest import ApiException


class TestAuthTokenRevokeRequest(unittest.TestCase):
    """AuthTokenRevokeRequest unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAuthTokenRevokeRequest(self):
        """Test AuthTokenRevokeRequest"""
        # FIXME: construct object with mandatory attributes with example values
        # model = swagger_client.models.auth_token_revoke_request.AuthTokenRevokeRequest()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
