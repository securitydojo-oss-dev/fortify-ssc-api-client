# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.plugin_localization_controller_api import PluginLocalizationControllerApi  # noqa: E501
from swagger_client.rest import ApiException


class TestPluginLocalizationControllerApi(unittest.TestCase):
    """PluginLocalizationControllerApi unit test stubs"""

    def setUp(self):
        self.api = PluginLocalizationControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_read_plugin_localization(self):
        """Test case for read_plugin_localization

        read  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
