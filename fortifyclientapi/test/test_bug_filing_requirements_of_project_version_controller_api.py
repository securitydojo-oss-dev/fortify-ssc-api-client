# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.bug_filing_requirements_of_project_version_controller_api import BugFilingRequirementsOfProjectVersionControllerApi  # noqa: E501
from swagger_client.rest import ApiException


class TestBugFilingRequirementsOfProjectVersionControllerApi(unittest.TestCase):
    """BugFilingRequirementsOfProjectVersionControllerApi unit test stubs"""

    def setUp(self):
        self.api = BugFilingRequirementsOfProjectVersionControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_do_action_bug_filing_requirements_of_project_version(self):
        """Test case for do_action_bug_filing_requirements_of_project_version

        doAction  # noqa: E501
        """
        pass

    def test_list_bug_filing_requirements_of_project_version(self):
        """Test case for list_bug_filing_requirements_of_project_version

        list  # noqa: E501
        """
        pass

    def test_login_bug_filing_requirements_of_project_version(self):
        """Test case for login_bug_filing_requirements_of_project_version

        Authenticate to the bug tracking system and return the initial set of bug filing requirements  # noqa: E501
        """
        pass

    def test_update_collection_bug_filing_requirements_of_project_version(self):
        """Test case for update_collection_bug_filing_requirements_of_project_version

        updateCollection  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
