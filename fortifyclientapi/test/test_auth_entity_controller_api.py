# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.auth_entity_controller_api import AuthEntityControllerApi  # noqa: E501
from swagger_client.rest import ApiException


class TestAuthEntityControllerApi(unittest.TestCase):
    """AuthEntityControllerApi unit test stubs"""

    def setUp(self):
        self.api = AuthEntityControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_list_auth_entity(self):
        """Test case for list_auth_entity

        list  # noqa: E501
        """
        pass

    def test_multi_delete_auth_entity(self):
        """Test case for multi_delete_auth_entity

        multiDelete  # noqa: E501
        """
        pass

    def test_read_auth_entity(self):
        """Test case for read_auth_entity

        read  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
