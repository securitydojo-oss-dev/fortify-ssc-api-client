# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.user_preferences_controller_api import UserPreferencesControllerApi  # noqa: E501
from swagger_client.rest import ApiException


class TestUserPreferencesControllerApi(unittest.TestCase):
    """UserPreferencesControllerApi unit test stubs"""

    def setUp(self):
        self.api = UserPreferencesControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_post_user_preferences(self):
        """Test case for post_user_preferences

        Retrieve the current user's session preferences. (The 'username' parameter is not yet supported)  # noqa: E501
        """
        pass

    def test_update_user_preferences(self):
        """Test case for update_user_preferences

        update  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
