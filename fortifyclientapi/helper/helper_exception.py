#!/usr/bin/env python

# coding: utf-8


class HelperException(Exception):

    def __init__(self, helper: str, status: str = None, reason: str = None):
        self.helper: str = helper
        self.status: str = status
        self.reason: str = reason

    def __str__(self):
        """Custom error messages for exception"""
        error_message = "{0} ({1}):\n"\
                        "Reason: {2}\n".format(
                            self.helper, self.status, self.reason)
        return error_message
