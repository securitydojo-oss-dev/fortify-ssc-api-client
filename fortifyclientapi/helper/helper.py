#!/usr/bin/env python

# coding: utf-8


from typing import Type
from fortifyclientapi.controller.controller import Controller
from fortifyclientapi.helper.helper_exception import HelperException


class Helper(object):

    def __init__(
        self,
        *controllers: Controller
    ):
        super().__init__()
        self.controllers: list[Controller] = list(controllers)

    def add_controllers(self, *controllers: Controller) -> None:
        self.controllers += list(controllers)

    def get_controller(self, controller_type: Type) -> Controller:
        try:
            controller: Controller = None

            for ctrl in self.controllers:
                if type(ctrl) == controller_type:
                    controller = ctrl
                    break
        except (TypeError, ValueError) as e:
            raise HelperException(
                helper="Helper",
                status="error",
                reason="{}".format(e)
            )

        return controller
