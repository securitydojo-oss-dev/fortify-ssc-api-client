#!/usr/bin/env python

# coding: utf-8

from argparse import Namespace
from datetime import datetime
from fortifyclientapi.cli.cli_controller.cli_controller import CliController
from fortifyclientapi.cli.command_exception import CommandException
from fortifyclientapi.controller.attribute_definition_controller import AttributeDefinitionController
from fortifyclientapi.controller.custom_tag_controller import CustomTagController
from fortifyclientapi.controller.issue_template_controller import IssueTemplateController
from fortifyclientapi.controller.project_version_attribute_controller import ProjectVersionAttributeController
from fortifyclientapi.controller.project_version_controller import ProjectVersionController
from fortifyclientapi.controller.project_version_result_processing_rule_controller import ProjectVersionResultProcessingRuleController
from fortifyclientapi.helper.attribute_helper import AttributeHelper
from fortifyclientapi.helper.project_version_helper import ProjectVersionHelper
from fortifyclientapi.sscclientapi.models.attribute import Attribute
from fortifyclientapi.sscclientapi.models.project_version import ProjectVersion
from fortifyclientapi.sscclientapi.models.result_processing_rule import ResultProcessingRule


class CreateProjectVersionCliController(CliController):

    def parse(self, data: Namespace):
        attribute_helper: AttributeHelper = AttributeHelper(
            AttributeDefinitionController(self._configuration)
        )

        self.project_name: str = data.project_version_project_name or None
        self.version_name: str = data.project_version_version_name or None
        self.auto_predict: bool = data.project_version_auto_predict or False
        self.active: bool = data.project_version_active if data.project_version_active is not None else True
        self.bug_tracker_enabled: bool = data.project_version_bug_tracker_enabled or False
        self.bug_tracker_plugin_id: str = data.project_version_bug_tracker_plugin_id or None
        self.bug_tracker_plugin_name: str = data.project_version_bug_tracker_plugin_name or ""
        self.created_by: str = data.project_version_created_by or ""
        self.creation_date: datetime = datetime.now()
        self.custom_tag_values_auto_apply: bool = data.project_version_custom_tag_values_auto_apply if data.project_version_custom_tag_values_auto_apply is not None else True
        self.description: str = data.project_version_description or "REST API - FortifyClientAPI - {project_name} {version_name}".format(
            project_name=self.project_name, version_name=self.version_name)
        self.issue_template_id: str = data.project_version_issue_template_id or "Prioritized-HighRisk-Project-Template"
        self.issue_template_name: str = data.project_version_issue_template_name or "Prioritized High Risk Issue Template"
        self.custom_tag_guid: str = data.project_version_custom_tag_guid or None
        self.custom_tag_name: str = data.project_version_custom_tag_name or "Analysis"
        self.refresh_required: bool = True
        self.server_version: float = 0.0
        self.snapshot_out_of_date: bool = False
        self.stale_issue_template: bool = False
        
        self.user_attributes: list[Attribute] = []
        attributes_dict: dict[str, list[str]] = {}
        try:
            for attr in data.project_version_attributes:
                if attr.split("=")[0] in attributes_dict.keys():
                    attributes_dict[attr.split("=")[0]] += [attr.split("=")[1]]
                else:
                    attributes_dict[attr.split("=")[0]] = [attr.split("=")[1]]

            for key in attributes_dict.keys():
                self.user_attributes += [
                    attribute_helper.configure_project_version_attribute_by_name(
                        attribute_definition_name=key,
                        value=attributes_dict[key][0] if len(
                            attributes_dict[key]) == 1 else None,
                        value_names=attributes_dict[key] if len(
                            attributes_dict[key]) >= 1 else None,
                    )
                ]
        except AttributeError as e:
            self.user_attributes = []

        self.attributes: list[Attribute] = []

        user_attr_ids: list[
            int] = [attr.attribute_definition_id for attr in self.user_attributes]

        for attr in attribute_helper.get_default_required_attributes():
            if attr.attribute_definition_id not in user_attr_ids:
                self.attributes += [attr]

        self.attributes += self.user_attributes

        self.user_processing_rule: list[ResultProcessingRule] = []
        self.user_processing_rule_unparsed: list[
            str] = data.project_version_processing_rules

    def execute(self):
        project_version_helper: ProjectVersionHelper = ProjectVersionHelper(
            ProjectVersionController(self._configuration),
            ProjectVersionAttributeController(self._configuration),
            IssueTemplateController(self._configuration),
            CustomTagController(self._configuration),
            ProjectVersionResultProcessingRuleController(self._configuration)
        )

        project_version: ProjectVersion = project_version_helper.configure_project_version(
            project_name=self.project_name,
            version_name=self.version_name,
            active=self.active if self.active is not None else True,
            auto_predict=self.auto_predict or False,
            bug_tracker_enabled=self.bug_tracker_enabled or False,
            bug_tracker_plugin_name=self.bug_tracker_plugin_name or "",
            created_by=self.created_by or "",
            creation_date=self.creation_date or datetime.now(),
            custom_tag_values_auto_apply=self.custom_tag_values_auto_apply if self.custom_tag_values_auto_apply is not None else True,
            description=self.description or "REST API - FortifyClientAPI - {project_name} {version_name}".format(
                project_name=self.project_name, version_name=self.version_name),
            issue_template_name=self.issue_template_name or "Prioritized High Risk Issue Template",
            issue_template_id= self.issue_template_id or "Prioritized-HighRisk-Project-Template",
            custom_tag_name=self.custom_tag_name or "Analysis"
        )

        if project_version.id is not None:
            raise CommandException(
                command="CreateProjectVersion",
                status="conflict",
                reason="Application Version {} already exists".format(
                    project_version.id)
            )

        project_version.id = project_version_helper.create_project_version(
            project_version=project_version
        ).id

        try:
            for rule in self.user_processing_rule_unparsed:
                self.user_processing_rule += [
                    project_version_helper.configure_project_version_result_processing_rule(
                        project_version_id=project_version.id,
                        processing_rule_identifier=rule.split("=")[0],
                        processing_rule_enabled=True if rule.split(
                            "=")[1] in ["yes", "on", "true", "1"] else False
                    )
                ]
        except AttributeError as e:
            self.user_processing_rule = []

        project_version = project_version_helper.update_project_version(
            project_version=project_version,
            attributes=self.attributes,
            processing_rules=self.user_processing_rule
        )

        print(project_version.id)
