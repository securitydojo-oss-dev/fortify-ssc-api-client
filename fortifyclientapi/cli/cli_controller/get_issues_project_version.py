#!/usr/bin/env python

# coding: utf-8

from argparse import Namespace
from datetime import datetime
from fortifyclientapi.cli.cli_controller.cli_controller import CliController
from fortifyclientapi.cli.command_exception import CommandException
from fortifyclientapi.controller.attribute_definition_controller import AttributeDefinitionController
from fortifyclientapi.controller.custom_tag_controller import CustomTagController
from fortifyclientapi.controller.issue_template_controller import IssueTemplateController
from fortifyclientapi.controller.project_version_attribute_controller import ProjectVersionAttributeController
from fortifyclientapi.controller.project_version_auth_entity_controller import ProjectVersionAuthEntityController
from fortifyclientapi.controller.project_version_controller import ProjectVersionController
from fortifyclientapi.controller.project_version_result_processing_rule_controller import \
    ProjectVersionResultProcessingRuleController
from fortifyclientapi.helper.attribute_helper import AttributeHelper
from fortifyclientapi.helper.project_version_helper import ProjectVersionHelper
from fortifyclientapi.sscclientapi.models.attribute import Attribute
from fortifyclientapi.sscclientapi.models.project_version import ProjectVersion
from fortifyclientapi.sscclientapi.models.api_result_list_project_version_issue import ApiResultListProjectVersionIssue


class GetIssuesProjectVersionCliController(CliController):

    def parse(self, data: Namespace):
        self.id: int = data.project_version_project_version_id or None
        self.project_name: str = data.project_version_project_name or None
        self.version_name: str = data.project_version_version_name or None

        self.show_hidden: bool = data.get_issues_show_hidden or False
        self.show_removed: bool = data.get_issues_show_removed or False
        self.show_suppressed: bool = data.get_issues_show_suppressed or False
        self.query: str = data.get_issues_query or None

    def execute(self):
        project_version_helper: ProjectVersionHelper = ProjectVersionHelper(
            ProjectVersionController(self._configuration),
            ProjectVersionAttributeController(self._configuration),
            IssueTemplateController(self._configuration),
            CustomTagController(self._configuration),
            ProjectVersionResultProcessingRuleController(self._configuration),
            ProjectVersionAuthEntityController(self._configuration)
        )

        project_version: ProjectVersion = project_version_helper.configure_project_version(
            project_name=self.project_name,
            version_name=self.version_name,
            project_version_id=self.id
        )

        issues_list: ApiResultListProjectVersionIssue = project_version_helper.get_issues_project_version(
            project_version_id=project_version.id,
            query=self.query,
            show_suppressed=self.show_suppressed,
            show_hidden=self.show_hidden,
            show_removed=self.show_removed
        )

        if issues_list is not None:
            if hasattr(issues_list, 'count'):
                print(issues_list.count)
            else:
                print(0)
        else:
            print(0)
