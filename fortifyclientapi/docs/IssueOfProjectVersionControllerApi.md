# fortifyclientapi.sscclientapi.IssueOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                         | HTTP request                                                  | Description                                                                                        |
| ------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| [**assign_user_for_issue_of_project_version**](IssueOfProjectVersionControllerApi.md#assign_user_for_issue_of_project_version) | **POST** /projectVersions/{parentId}/issues/action/assignUser | Assign issue to user                                                                               |
| [**audit_issue_of_project_version**](IssueOfProjectVersionControllerApi.md#audit_issue_of_project_version)                     | **POST** /projectVersions/{parentId}/issues/action/audit      | Perform an auditing action on the specified issues                                                 |
| [**do_action_issue_of_project_version**](IssueOfProjectVersionControllerApi.md#do_action_issue_of_project_version)             | **POST** /projectVersions/{parentId}/issues/action            | doAction                                                                                           |
| [**file_bug_for_issue_of_project_version**](IssueOfProjectVersionControllerApi.md#file_bug_for_issue_of_project_version)       | **POST** /projectVersions/{parentId}/issues/action/fileBug    | Submit a bug (defect) for the specified issues in the external bug tracking system (if configured) |
| [**list_issue_of_project_version**](IssueOfProjectVersionControllerApi.md#list_issue_of_project_version)                       | **GET** /projectVersions/{parentId}/issues                    | list                                                                                               |
| [**read_issue_of_project_version**](IssueOfProjectVersionControllerApi.md#read_issue_of_project_version)                       | **GET** /projectVersions/{parentId}/issues/{id}               | read                                                                                               |
| [**suppress_issue_of_project_version**](IssueOfProjectVersionControllerApi.md#suppress_issue_of_project_version)               | **POST** /projectVersions/{parentId}/issues/action/suppress   | Suppress or unsuppress an issue                                                                    |
| [**update_tag_for_issue_of_project_version**](IssueOfProjectVersionControllerApi.md#update_tag_for_issue_of_project_version)   | **POST** /projectVersions/{parentId}/issues/action/updateTag  | Update the custom tag for the specified issues                                                     |

# **assign_user_for_issue_of_project_version**
> ApiResultIssueActionResponse assign_user_for_issue_of_project_version(body, parent_id)

Assign issue to user

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.IssueAssignUserRequest() # IssueAssignUserRequest | resource
parent_id = 789 # int | parentId

try:
    # Assign issue to user
    api_response = api_instance.assign_user_for_issue_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueOfProjectVersionControllerApi->assign_user_for_issue_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                                    | Description | Notes |
| ------------- | ------------------------------------------------------- | ----------- | ----- |
| **body**      | [**IssueAssignUserRequest**](IssueAssignUserRequest.md) | resource    |
| **parent_id** | **int**                                                 | parentId    |

### Return type

[**ApiResultIssueActionResponse**](ApiResultIssueActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **audit_issue_of_project_version**
> ApiResultIssueActionResponse audit_issue_of_project_version(body, parent_id)

Perform an auditing action on the specified issues

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.IssueAuditRequest() # IssueAuditRequest | resource
parent_id = 789 # int | parentId

try:
    # Perform an auditing action on the specified issues
    api_response = api_instance.audit_issue_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueOfProjectVersionControllerApi->audit_issue_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                          | Description | Notes |
| ------------- | --------------------------------------------- | ----------- | ----- |
| **body**      | [**IssueAuditRequest**](IssueAuditRequest.md) | resource    |
| **parent_id** | **int**                                       | parentId    |

### Return type

[**ApiResultIssueActionResponse**](ApiResultIssueActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_action_issue_of_project_version**
> ApiResultApiActionResponse do_action_issue_of_project_version(body, parent_id)

doAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiCollectionActionlong() # ApiCollectionActionlong | collectionAction
parent_id = 789 # int | parentId

try:
    # doAction
    api_response = api_instance.do_action_issue_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueOfProjectVersionControllerApi->do_action_issue_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                                      | Description      | Notes |
| ------------- | --------------------------------------------------------- | ---------------- | ----- |
| **body**      | [**ApiCollectionActionlong**](ApiCollectionActionlong.md) | collectionAction |
| **parent_id** | **int**                                                   | parentId         |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **file_bug_for_issue_of_project_version**
> ApiResultIssueFileBugResponse file_bug_for_issue_of_project_version(body, parent_id)

Submit a bug (defect) for the specified issues in the external bug tracking system (if configured)

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.IssueFileBugRequest() # IssueFileBugRequest | resource
parent_id = 789 # int | parentId

try:
    # Submit a bug (defect) for the specified issues in the external bug tracking system (if configured)
    api_response = api_instance.file_bug_for_issue_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueOfProjectVersionControllerApi->file_bug_for_issue_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                              | Description | Notes |
| ------------- | ------------------------------------------------- | ----------- | ----- |
| **body**      | [**IssueFileBugRequest**](IssueFileBugRequest.md) | resource    |
| **parent_id** | **int**                                           | parentId    |

### Return type

[**ApiResultIssueFileBugResponse**](ApiResultIssueFileBugResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_issue_of_project_version**
> ApiResultListProjectVersionIssue list_issue_of_project_version(parent_id, start=start, limit=limit, q=q, qm=qm, orderby=orderby, filterset=filterset, fields=fields, showhidden=showhidden, showremoved=showremoved, showsuppressed=showsuppressed, showshortfilenames=showshortfilenames, filter=filter, groupid=groupid, groupingtype=groupingtype, ids=ids)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | An issue query expression, must be used together with the 'qm' parameter (optional)
qm = 'qm_example' # str | Syntax mode for the 'q' parameter, mandatory if the 'q' parameter is used (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)
filterset = 'filterset_example' # str | Filter set to use (optional)
fields = 'fields_example' # str | Output fields (optional)
showhidden = false # bool | If 'true', include hidden issues in search results. If 'false', exclude hidden issues from search results. If no options are set, use application version profile settings to get value of this option. (optional) (default to false)
showremoved = false # bool | If 'true', include removed issues in search results. If 'false', exclude removed issues from search results. If no options are set, use application version profile settings to get value of this option. (optional) (default to false)
showsuppressed = false # bool | If 'true', include suppressed issues in search results. If 'false', exclude suppressed issues from search results. If no options are set, use application version profile settings to get value of this option. (optional) (default to false)
showshortfilenames = false # bool | If 'true', only short file names will be displayed in issues list. (optional) (default to false)
filter = 'filter_example' # str | filter (optional)
groupid = 'groupid_example' # str | groupid (optional)
groupingtype = 'groupingtype_example' # str | groupingtype (optional)
ids = 'ids_example' # str | A comma-separated value list of issue ids. If provided, other filtering and ordering parameters can not be used. (optional)

try:
    # list
    api_response = api_instance.list_issue_of_project_version(parent_id, start=start, limit=limit, q=q, qm=qm, orderby=orderby, filterset=filterset, fields=fields, showhidden=showhidden, showremoved=showremoved, showsuppressed=showsuppressed, showshortfilenames=showshortfilenames, filter=filter, groupid=groupid, groupingtype=groupingtype, ids=ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueOfProjectVersionControllerApi->list_issue_of_project_version: %s\n" % e)
```

### Parameters

| Name                   | Type     | Description                                                                                                                                                                                                                         | Notes                         |
| ---------------------- | -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------- |
| **parent_id**          | **int**  | parentId                                                                                                                                                                                                                            |
| **start**              | **int**  | A start offset in object listing                                                                                                                                                                                                    | [optional] [default to 0]     |
| **limit**              | **int**  | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied                                                                                                                             | [optional] [default to 200]   |
| **q**                  | **str**  | An issue query expression, must be used together with the &#x27;qm&#x27; parameter                                                                                                                                                  | [optional]                    |
| **qm**                 | **str**  | Syntax mode for the &#x27;q&#x27; parameter, mandatory if the &#x27;q&#x27; parameter is used                                                                                                                                       | [optional]                    |
| **orderby**            | **str**  | Fields to order by                                                                                                                                                                                                                  | [optional]                    |
| **filterset**          | **str**  | Filter set to use                                                                                                                                                                                                                   | [optional]                    |
| **fields**             | **str**  | Output fields                                                                                                                                                                                                                       | [optional]                    |
| **showhidden**         | **bool** | If &#x27;true&#x27;, include hidden issues in search results. If &#x27;false&#x27;, exclude hidden issues from search results. If no options are set, use application version profile settings to get value of this option.         | [optional] [default to false] |
| **showremoved**        | **bool** | If &#x27;true&#x27;, include removed issues in search results. If &#x27;false&#x27;, exclude removed issues from search results. If no options are set, use application version profile settings to get value of this option.       | [optional] [default to false] |
| **showsuppressed**     | **bool** | If &#x27;true&#x27;, include suppressed issues in search results. If &#x27;false&#x27;, exclude suppressed issues from search results. If no options are set, use application version profile settings to get value of this option. | [optional] [default to false] |
| **showshortfilenames** | **bool** | If &#x27;true&#x27;, only short file names will be displayed in issues list.                                                                                                                                                        | [optional] [default to false] |
| **filter**             | **str**  | filter                                                                                                                                                                                                                              | [optional]                    |
| **groupid**            | **str**  | groupid                                                                                                                                                                                                                             | [optional]                    |
| **groupingtype**       | **str**  | groupingtype                                                                                                                                                                                                                        | [optional]                    |
| **ids**                | **str**  | A comma-separated value list of issue ids. If provided, other filtering and ordering parameters can not be used.                                                                                                                    | [optional]                    |

### Return type

[**ApiResultListProjectVersionIssue**](ApiResultListProjectVersionIssue.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_issue_of_project_version**
> ApiResultProjectVersionIssue read_issue_of_project_version(parent_id, id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_issue_of_project_version(parent_id, id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueOfProjectVersionControllerApi->read_issue_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description   | Notes      |
| ------------- | ------- | ------------- | ---------- |
| **parent_id** | **int** | parentId      |
| **id**        | **int** | id            |
| **fields**    | **str** | Output fields | [optional] |

### Return type

[**ApiResultProjectVersionIssue**](ApiResultProjectVersionIssue.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **suppress_issue_of_project_version**
> ApiResultIssueActionResponse suppress_issue_of_project_version(body, parent_id)

Suppress or unsuppress an issue

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.IssueSuppressRequest() # IssueSuppressRequest | resource
parent_id = 789 # int | parentId

try:
    # Suppress or unsuppress an issue
    api_response = api_instance.suppress_issue_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueOfProjectVersionControllerApi->suppress_issue_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                                | Description | Notes |
| ------------- | --------------------------------------------------- | ----------- | ----- |
| **body**      | [**IssueSuppressRequest**](IssueSuppressRequest.md) | resource    |
| **parent_id** | **int**                                             | parentId    |

### Return type

[**ApiResultIssueActionResponse**](ApiResultIssueActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_tag_for_issue_of_project_version**
> ApiResultIssueActionResponse update_tag_for_issue_of_project_version(body, parent_id)

Update the custom tag for the specified issues

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.IssueUpdateTagRequest() # IssueUpdateTagRequest | resource
parent_id = 789 # int | parentId

try:
    # Update the custom tag for the specified issues
    api_response = api_instance.update_tag_for_issue_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueOfProjectVersionControllerApi->update_tag_for_issue_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                                  | Description | Notes |
| ------------- | ----------------------------------------------------- | ----------- | ----- |
| **body**      | [**IssueUpdateTagRequest**](IssueUpdateTagRequest.md) | resource    |
| **parent_id** | **int**                                               | parentId    |

### Return type

[**ApiResultIssueActionResponse**](ApiResultIssueActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

