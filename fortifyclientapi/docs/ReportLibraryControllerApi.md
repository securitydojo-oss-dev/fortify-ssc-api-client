# fortifyclientapi.sscclientapi.ReportLibraryControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                       | HTTP request                     | Description |
| -------------------------------------------------------------------------------------------- | -------------------------------- | ----------- |
| [**delete_report_library**](ReportLibraryControllerApi.md#delete_report_library)             | **DELETE** /reportLibraries/{id} | delete      |
| [**list_report_library**](ReportLibraryControllerApi.md#list_report_library)                 | **GET** /reportLibraries         | list        |
| [**multi_delete_report_library**](ReportLibraryControllerApi.md#multi_delete_report_library) | **DELETE** /reportLibraries      | multiDelete |
| [**read_report_library**](ReportLibraryControllerApi.md#read_report_library)                 | **GET** /reportLibraries/{id}    | read        |
| [**update_report_library**](ReportLibraryControllerApi.md#update_report_library)             | **PUT** /reportLibraries/{id}    | update      |
| [**upload_report_library**](ReportLibraryControllerApi.md#upload_report_library)             | **POST** /reportLibraries        | upload      |

# **delete_report_library**
> ApiResultVoid delete_report_library(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ReportLibraryControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_report_library(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReportLibraryControllerApi->delete_report_library: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_report_library**
> ApiResultListReportLibrary list_report_library(fields=fields, start=start, limit=limit)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ReportLibraryControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)

try:
    # list
    api_response = api_instance.list_report_library(fields=fields, start=start, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReportLibraryControllerApi->list_report_library: %s\n" % e)
```

### Parameters

| Name       | Type    | Description                                                                                             | Notes                       |
| ---------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields** | **str** | Output fields                                                                                           | [optional]                  |
| **start**  | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**  | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |

### Return type

[**ApiResultListReportLibrary**](ApiResultListReportLibrary.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_report_library**
> ApiResultVoid multi_delete_report_library(ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ReportLibraryControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_report_library(ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReportLibraryControllerApi->multi_delete_report_library: %s\n" % e)
```

### Parameters

| Name    | Type    | Description                                    | Notes |
| ------- | ------- | ---------------------------------------------- | ----- |
| **ids** | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_report_library**
> ApiResultReportLibrary read_report_library(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ReportLibraryControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_report_library(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReportLibraryControllerApi->read_report_library: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultReportLibrary**](ApiResultReportLibrary.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_report_library**
> ApiResultReportLibrary update_report_library(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ReportLibraryControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ReportLibrary() # ReportLibrary | resource
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_report_library(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReportLibraryControllerApi->update_report_library: %s\n" % e)
```

### Parameters

| Name     | Type                                  | Description | Notes |
| -------- | ------------------------------------- | ----------- | ----- |
| **body** | [**ReportLibrary**](ReportLibrary.md) | resource    |
| **id**   | **int**                               | id          |

### Return type

[**ApiResultReportLibrary**](ApiResultReportLibrary.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **upload_report_library**
> ApiResultReportLibrary upload_report_library(file, description=description)

upload

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ReportLibraryControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
file = 'file_example' # str | 
description = 'description_example' # str | description (optional)

try:
    # upload
    api_response = api_instance.upload_report_library(file, description=description)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReportLibraryControllerApi->upload_report_library: %s\n" % e)
```

### Parameters

| Name            | Type    | Description | Notes      |
| --------------- | ------- | ----------- | ---------- |
| **file**        | **str** |             |
| **description** | **str** | description | [optional] |

### Return type

[**ApiResultReportLibrary**](ApiResultReportLibrary.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

