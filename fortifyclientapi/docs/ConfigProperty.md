# ConfigProperty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applied_after_restarting** | **bool** | Should the SSC server be restarted after changing value of the property to apply the changes. | [optional] 
**config_property_value_validation** | [**ConfigPropertyValueValidation**](ConfigPropertyValueValidation.md) |  | [optional] 
**description** | **str** | Property description. | [optional] 
**group** | **str** | Parent group name the property belongs to. | [optional] 
**group_switch_enabled** | **bool** | Flag is set to TRUE if property is allowed to be edited on UI. | [optional] 
**name** | **str** | Configuration property unique name. | 
**property_type** | **str** | Property value type. | [optional] 
**protected_option** | **bool** | If special permission is required to get value of this property. | [optional] 
**required** | **bool** | Flag is set to TRUE if this property is required and always must have non empty value. | [optional] 
**sub_group** | **str** | Parent subgroup name the property belongs to. | [optional] 
**value** | **str** | Configuration property value. | 
**values_list** | [**list[ConfigPropertyValueItem]**](ConfigPropertyValueItem.md) | List of allowed property values if property type is OPTIONLIST or DYNAMIC_OPTIONLIST. | [optional] 
**version** | **int** | Configuration property version stored on the server. This value is used for optimistic locking of the property object to prevent concurrent changes of the property value by different users at the same time. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

