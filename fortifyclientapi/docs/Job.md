# Job

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**artifact_name** | **str** | Artifact name related to this job | 
**cancellable** | **bool** | Set to true if job is cancelable | 
**execution_order** | **float** | Job execution order | 
**finish_time** | **datetime** | End time of job | 
**job_class** | **str** | Job class | 
**job_data** | **object** | Job data | 
**job_group** | **str** | Job group | 
**job_name** | **str** | identifier of job | [optional] 
**priority** | **int** | Job priority | 
**project_name** | **str** | Application name related to this job | 
**project_version_id** | **int** | Application version identifier related to this job | 
**project_version_name** | **str** | Application version name related to this job | 
**start_time** | **datetime** | Start time of job | 
**state** | **str** | Job State | 
**user_name** | **str** | Name of user this job was created by | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

