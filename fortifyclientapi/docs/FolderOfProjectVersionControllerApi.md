# fortifyclientapi.sscclientapi.FolderOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                      | HTTP request                                     | Description |
| ----------------------------------------------------------------------------------------------------------- | ------------------------------------------------ | ----------- |
| [**list_folder_of_project_version**](FolderOfProjectVersionControllerApi.md#list_folder_of_project_version) | **GET** /projectVersions/{parentId}/folders      | list        |
| [**read_folder_of_project_version**](FolderOfProjectVersionControllerApi.md#read_folder_of_project_version) | **GET** /projectVersions/{parentId}/folders/{id} | read        |

# **list_folder_of_project_version**
> ApiResultListFolder list_folder_of_project_version(parent_id)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.FolderOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId

try:
    # list
    api_response = api_instance.list_folder_of_project_version(parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FolderOfProjectVersionControllerApi->list_folder_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description | Notes |
| ------------- | ------- | ----------- | ----- |
| **parent_id** | **int** | parentId    |

### Return type

[**ApiResultListFolder**](ApiResultListFolder.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_folder_of_project_version**
> ApiResultFolder read_folder_of_project_version(parent_id, id)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.FolderOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
id = 789 # int | id

try:
    # read
    api_response = api_instance.read_folder_of_project_version(parent_id, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FolderOfProjectVersionControllerApi->read_folder_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description | Notes |
| ------------- | ------- | ----------- | ----- |
| **parent_id** | **int** | parentId    |
| **id**        | **int** | id          |

### Return type

[**ApiResultFolder**](ApiResultFolder.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

