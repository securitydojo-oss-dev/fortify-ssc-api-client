# DataExport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_version_id** | **int** | Application version id - required if DatasetName &#x3D; \&quot;Audit\&quot; | [optional] 
**app_version_name** | **str** | Application version name | [optional] 
**dataset_name** | **str** | Dataset name - e.g. \&quot;Audit\&quot;, \&quot;Issue Stats\&quot; - corresponds to page from which data is exported | 
**document_info_id** | **int** | Document Info id for file blob | [optional] 
**expiration** | **int** | Data export file expiration in days | [optional] 
**export_date** | **datetime** | Export date | [optional] 
**file_name** | **str** | Name of data export file | 
**file_type** | **str** | File type, e.g. CSV | [optional] 
**id** | **int** |  | [optional] 
**note** | **str** | Note, i.e. comments or info related to data being exported | [optional] 
**status** | **str** | Data export status | [optional] 
**user_name** | **str** | User name of user who initiated the data export | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

