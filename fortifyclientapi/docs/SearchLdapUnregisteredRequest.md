# SearchLdapUnregisteredRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filter** | **str** | The specified query string is matched against the name, firstName, lastName, and email fields for LDAP User entities. For Group and OrgUnit entities, only the name is matched. The query string must match the entire field value. For partial matches you can add asterisk (&#x27;*&#x27;) wildcards to your query. | [optional] 
**ldap_type** | **str** | Type of ldap objects to be matched | 
**limit** | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; no limit is applied | [optional] 
**start** | **int** | A start offset in object listing | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

