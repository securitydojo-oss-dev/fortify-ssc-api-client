# DynamicScanRequestParameterDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | [optional] 
**guid** | **str** | Unique string identifier for this parameter definition | 
**id** | **int** | Unique id for this parameter definition | [optional] 
**name** | **str** |  | 
**required** | **bool** | Set to true if required | 
**sequence_number** | **int** |  | [optional] 
**type** | **str** | Attribute Definition Type from which this parameter definition is based on | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

