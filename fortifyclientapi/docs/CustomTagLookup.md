# CustomTagLookup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**considered_issue** | **bool** | Flag that should be used to mark values that mean \&quot;not an issue\&quot; or \&quot;false positive\&quot;. | [optional] 
**custom_tag_guid** | **str** | GUID of the parent custom tag this value belongs to. | [optional] 
**default_value** | **bool** | Flag that says if this value is default custom tag value and should be selected automatically in the values list on issue audit screen. | [optional] 
**deletable** | **bool** | Flag that says if this value can be removed from custom tag. Value cannot be removed if it is selected for some issue in the system. | [optional] 
**description** | **str** | Custom tag value description. | [optional] 
**hidden** | **bool** | Flag that says that this value is hidden and cannot be selected in issue audit mode. | [optional] 
**lookup_index** | **int** | Current value index in values list. | 
**lookup_value** | **str** | Plain text custom tag value. | 
**relying_custom_tags** | [**list[CustomTagInfo]**](CustomTagInfo.md) | List of relying custom tags and their values this value depends on. This list should be set to automatically reflect relying custom tags values changes to this custom tag value | [optional] 
**seq_number** | **int** | Custom tag value sequence number. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

