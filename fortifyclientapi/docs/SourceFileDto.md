# SourceFileDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**checksum** | **str** | Source file checksum. | 
**encoding** | **str** | Source file encoding. | 
**file_content** | **str** | Scanned source file content. | 
**file_path** | **str** | Full path to source file on the machine where scan was performed. | 
**language_name** | **str** | Source file programming language name. | 
**project_version_id** | **int** | Application version identifier that contains this source file. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

