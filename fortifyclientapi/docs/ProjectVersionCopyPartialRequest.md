# ProjectVersionCopyPartialRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**copy_analysis_processing_rules** | **bool** | Will copy analysis processing rules | 
**copy_bug_tracker_configuration** | **bool** | Will copy bugtracker configuration | 
**copy_custom_tags** | **bool** | Will copy custom tags | 
**previous_project_version_id** | **int** | Previous application version id | 
**project_version_id** | **int** | Application version id | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

