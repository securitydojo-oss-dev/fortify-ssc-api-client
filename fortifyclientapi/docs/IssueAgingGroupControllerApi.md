# fortifyclientapi.sscclientapi.IssueAgingGroupControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                               | HTTP request             | Description |
| ------------------------------------------------------------------------------------ | ------------------------ | ----------- |
| [**list_issue_aging_group**](IssueAgingGroupControllerApi.md#list_issue_aging_group) | **GET** /issueaginggroup | list        |

# **list_issue_aging_group**
> ApiResultListIssueAgingGroupDto list_issue_aging_group(groupby=groupby, fields=fields, start=start, limit=limit, filterby=filterby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueAgingGroupControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
groupby = 'groupby_example' # str | Fields to group by (optional)
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
filterby = 'filterby_example' # str | filterby (optional)

try:
    # list
    api_response = api_instance.list_issue_aging_group(groupby=groupby, fields=fields, start=start, limit=limit, filterby=filterby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueAgingGroupControllerApi->list_issue_aging_group: %s\n" % e)
```

### Parameters

| Name         | Type    | Description                                                                                             | Notes                       |
| ------------ | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **groupby**  | **str** | Fields to group by                                                                                      | [optional]                  |
| **fields**   | **str** | Output fields                                                                                           | [optional]                  |
| **start**    | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**    | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **filterby** | **str** | filterby                                                                                                | [optional]                  |

### Return type

[**ApiResultListIssueAgingGroupDto**](ApiResultListIssueAgingGroupDto.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

