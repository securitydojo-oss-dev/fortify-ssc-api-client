# ExternalBugInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_bug_deep_link** | **str** | Link to a bug the user can navigate to by clicking the link | 
**external_bug_id** | **str** | ID of the bug | 
**file_bug_for_select_all** | **bool** | True if user wants to file a bug for all selected issues in UI | 
**issue_count** | **int** | Count of issues for which a bug is filed | 
**issue_instance_ids** | **list[str]** | List of Issue Instance IDs | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

