# ApiResultListProjectVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** |  | [optional] 
**data** | [**list[ProjectVersion]**](ProjectVersion.md) |  | [optional] 
**error_code** | **int** |  | [optional] 
**links** | **object** |  | [optional] 
**message** | **str** |  | [optional] 
**response_code** | **int** |  | [optional] 
**stack_trace** | **str** |  | [optional] 
**success_count** | **int** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

