# ValidationStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msg** | **str** | Validation status message | 
**valid** | **bool** | Indicates whether validation was successful | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

