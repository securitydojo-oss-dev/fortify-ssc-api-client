# fortifyclientapi.sscclientapi.ProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                  | HTTP request                                           | Description                                                                      |
| ----------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------ | -------------------------------------------------------------------------------- |
| [**audit_by_audit_assistant_project_version**](ProjectVersionControllerApi.md#audit_by_audit_assistant_project_version) | **POST** /projectVersions/action/auditByAuditAssistant | Send issue data to Audit Assistant for assessment                                |
| [**copy_current_state_for_project_version**](ProjectVersionControllerApi.md#copy_current_state_for_project_version)     | **POST** /projectVersions/action/copyCurrentState      | Copy current audit state from one project version into another                   |
| [**copy_project_version**](ProjectVersionControllerApi.md#copy_project_version)                                         | **POST** /projectVersions/action/copyFromPartial       | Copy selected data from one project version into another                         |
| [**create_project_version**](ProjectVersionControllerApi.md#create_project_version)                                     | **POST** /projectVersions                              | create                                                                           |
| [**delete_project_version**](ProjectVersionControllerApi.md#delete_project_version)                                     | **DELETE** /projectVersions/{id}                       | delete                                                                           |
| [**do_action_project_version**](ProjectVersionControllerApi.md#do_action_project_version)                               | **POST** /projectVersions/{id}/action                  | doAction                                                                         |
| [**do_collection_action_project_version**](ProjectVersionControllerApi.md#do_collection_action_project_version)         | **POST** /projectVersions/action                       | doCollectionAction                                                               |
| [**list_project_version**](ProjectVersionControllerApi.md#list_project_version)                                         | **GET** /projectVersions                               | list                                                                             |
| [**purge_project_version**](ProjectVersionControllerApi.md#purge_project_version)                                       | **POST** /projectVersions/action/purge                 | Purge the specified project version from the system and remove all its data      |
| [**read_project_version**](ProjectVersionControllerApi.md#read_project_version)                                         | **GET** /projectVersions/{id}                          | read                                                                             |
| [**refresh_project_version**](ProjectVersionControllerApi.md#refresh_project_version)                                   | **POST** /projectVersions/action/refresh               | Re-calculate the metrics for the project version                                 |
| [**test_project_version**](ProjectVersionControllerApi.md#test_project_version)                                         | **POST** /projectVersions/action/test                  | Check whether the specified application version is already defined in the system |
| [**train_audit_assistant_project_version**](ProjectVersionControllerApi.md#train_audit_assistant_project_version)       | **POST** /projectVersions/action/trainAuditAssistant   | Send audited issue data to Audit Assistant to help train the engine              |
| [**update_project_version**](ProjectVersionControllerApi.md#update_project_version)                                     | **PUT** /projectVersions/{id}                          | update                                                                           |

# **audit_by_audit_assistant_project_version**
> ApiResultVoid audit_by_audit_assistant_project_version(body)

Send issue data to Audit Assistant for assessment

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ProjectVersionAuditByAssistantRequest() # ProjectVersionAuditByAssistantRequest | resource

try:
    # Send issue data to Audit Assistant for assessment
    api_response = api_instance.audit_by_audit_assistant_project_version(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->audit_by_audit_assistant_project_version: %s\n" % e)
```

### Parameters

| Name     | Type                                                                                  | Description | Notes |
| -------- | ------------------------------------------------------------------------------------- | ----------- | ----- |
| **body** | [**ProjectVersionAuditByAssistantRequest**](ProjectVersionAuditByAssistantRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **copy_current_state_for_project_version**
> ApiResultVoid copy_current_state_for_project_version(body)

Copy current audit state from one project version into another

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ProjectVersionCopyCurrentStateRequest() # ProjectVersionCopyCurrentStateRequest | resource

try:
    # Copy current audit state from one project version into another
    api_response = api_instance.copy_current_state_for_project_version(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->copy_current_state_for_project_version: %s\n" % e)
```

### Parameters

| Name     | Type                                                                                  | Description | Notes |
| -------- | ------------------------------------------------------------------------------------- | ----------- | ----- |
| **body** | [**ProjectVersionCopyCurrentStateRequest**](ProjectVersionCopyCurrentStateRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **copy_project_version**
> ApiResultVoid copy_project_version(body)

Copy selected data from one project version into another

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ProjectVersionCopyPartialRequest() # ProjectVersionCopyPartialRequest | resource

try:
    # Copy selected data from one project version into another
    api_response = api_instance.copy_project_version(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->copy_project_version: %s\n" % e)
```

### Parameters

| Name     | Type                                                                        | Description | Notes |
| -------- | --------------------------------------------------------------------------- | ----------- | ----- |
| **body** | [**ProjectVersionCopyPartialRequest**](ProjectVersionCopyPartialRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **create_project_version**
> ApiResultProjectVersion create_project_version(body)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ProjectVersion() # ProjectVersion | resource

try:
    # create
    api_response = api_instance.create_project_version(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->create_project_version: %s\n" % e)
```

### Parameters

| Name     | Type                                    | Description | Notes |
| -------- | --------------------------------------- | ----------- | ----- |
| **body** | [**ProjectVersion**](ProjectVersion.md) | resource    |

### Return type

[**ApiResultProjectVersion**](ApiResultProjectVersion.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_project_version**
> ApiResultVoid delete_project_version(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_project_version(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->delete_project_version: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_action_project_version**
> ApiResultApiActionResponse do_action_project_version(body, id)

doAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiResourceAction() # ApiResourceAction | resourceAction
id = 789 # int | id

try:
    # doAction
    api_response = api_instance.do_action_project_version(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->do_action_project_version: %s\n" % e)
```

### Parameters

| Name     | Type                                          | Description    | Notes |
| -------- | --------------------------------------------- | -------------- | ----- |
| **body** | [**ApiResourceAction**](ApiResourceAction.md) | resourceAction |
| **id**   | **int**                                       | id             |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_collection_action_project_version**
> ApiResultApiActionResponse do_collection_action_project_version(body)

doCollectionAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiCollectionActionlong() # ApiCollectionActionlong | collectionAction

try:
    # doCollectionAction
    api_response = api_instance.do_collection_action_project_version(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->do_collection_action_project_version: %s\n" % e)
```

### Parameters

| Name     | Type                                                      | Description      | Notes |
| -------- | --------------------------------------------------------- | ---------------- | ----- |
| **body** | [**ApiCollectionActionlong**](ApiCollectionActionlong.md) | collectionAction |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_project_version**
> ApiResultListProjectVersion list_project_version(fields=fields, start=start, limit=limit, q=q, fulltextsearch=fulltextsearch, orderby=orderby, include_inactive=include_inactive, my_assigned_issues=my_assigned_issues, only_if_has_issues=only_if_has_issues)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search-spec of full text search query (see fulltextsearch parameter) (optional)
fulltextsearch = false # bool | If 'true', interpret 'q' parameter as full text search query, defaults to 'false' (optional) (default to false)
orderby = 'orderby_example' # str | Fields to order by (optional)
include_inactive = false # bool | includeInactive (optional) (default to false)
my_assigned_issues = false # bool | myAssignedIssues (optional) (default to false)
only_if_has_issues = false # bool | onlyIfHasIssues (optional) (default to false)

try:
    # list
    api_response = api_instance.list_project_version(fields=fields, start=start, limit=limit, q=q, fulltextsearch=fulltextsearch, orderby=orderby, include_inactive=include_inactive, my_assigned_issues=my_assigned_issues, only_if_has_issues=only_if_has_issues)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->list_project_version: %s\n" % e)
```

### Parameters

| Name                   | Type     | Description                                                                                                     | Notes                         |
| ---------------------- | -------- | --------------------------------------------------------------------------------------------------------------- | ----------------------------- |
| **fields**             | **str**  | Output fields                                                                                                   | [optional]                    |
| **start**              | **int**  | A start offset in object listing                                                                                | [optional] [default to 0]     |
| **limit**              | **int**  | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied         | [optional] [default to 200]   |
| **q**                  | **str**  | A search-spec of full text search query (see fulltextsearch parameter)                                          | [optional]                    |
| **fulltextsearch**     | **bool** | If &#x27;true&#x27;, interpret &#x27;q&#x27; parameter as full text search query, defaults to &#x27;false&#x27; | [optional] [default to false] |
| **orderby**            | **str**  | Fields to order by                                                                                              | [optional]                    |
| **include_inactive**   | **bool** | includeInactive                                                                                                 | [optional] [default to false] |
| **my_assigned_issues** | **bool** | myAssignedIssues                                                                                                | [optional] [default to false] |
| **only_if_has_issues** | **bool** | onlyIfHasIssues                                                                                                 | [optional] [default to false] |

### Return type

[**ApiResultListProjectVersion**](ApiResultListProjectVersion.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **purge_project_version**
> ApiResultVoid purge_project_version(body)

Purge the specified project version from the system and remove all its data

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ProjectVersionPurgeRequest() # ProjectVersionPurgeRequest | resource

try:
    # Purge the specified project version from the system and remove all its data
    api_response = api_instance.purge_project_version(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->purge_project_version: %s\n" % e)
```

### Parameters

| Name     | Type                                                            | Description | Notes |
| -------- | --------------------------------------------------------------- | ----------- | ----- |
| **body** | [**ProjectVersionPurgeRequest**](ProjectVersionPurgeRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_project_version**
> ApiResultProjectVersion read_project_version(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_project_version(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->read_project_version: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultProjectVersion**](ApiResultProjectVersion.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **refresh_project_version**
> ApiResultProjectVersionRefreshResponse refresh_project_version(body)

Re-calculate the metrics for the project version

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ProjectVersionRefreshRequest() # ProjectVersionRefreshRequest | resource

try:
    # Re-calculate the metrics for the project version
    api_response = api_instance.refresh_project_version(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->refresh_project_version: %s\n" % e)
```

### Parameters

| Name     | Type                                                                | Description | Notes |
| -------- | ------------------------------------------------------------------- | ----------- | ----- |
| **body** | [**ProjectVersionRefreshRequest**](ProjectVersionRefreshRequest.md) | resource    |

### Return type

[**ApiResultProjectVersionRefreshResponse**](ApiResultProjectVersionRefreshResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **test_project_version**
> ApiResultProjectVersionTestResponse test_project_version(body)

Check whether the specified application version is already defined in the system

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ProjectVersionTestRequest() # ProjectVersionTestRequest | projectVersionTestRequest

try:
    # Check whether the specified application version is already defined in the system
    api_response = api_instance.test_project_version(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->test_project_version: %s\n" % e)
```

### Parameters

| Name     | Type                                                          | Description               | Notes |
| -------- | ------------------------------------------------------------- | ------------------------- | ----- |
| **body** | [**ProjectVersionTestRequest**](ProjectVersionTestRequest.md) | projectVersionTestRequest |

### Return type

[**ApiResultProjectVersionTestResponse**](ApiResultProjectVersionTestResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **train_audit_assistant_project_version**
> ApiResultVoid train_audit_assistant_project_version(body)

Send audited issue data to Audit Assistant to help train the engine

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ProjectVersionTrainAuditAssistantRequest() # ProjectVersionTrainAuditAssistantRequest | resource

try:
    # Send audited issue data to Audit Assistant to help train the engine
    api_response = api_instance.train_audit_assistant_project_version(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->train_audit_assistant_project_version: %s\n" % e)
```

### Parameters

| Name     | Type                                                                                        | Description | Notes |
| -------- | ------------------------------------------------------------------------------------------- | ----------- | ----- |
| **body** | [**ProjectVersionTrainAuditAssistantRequest**](ProjectVersionTrainAuditAssistantRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_project_version**
> ApiResultProjectVersion update_project_version(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ProjectVersion() # ProjectVersion | resource
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_project_version(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionControllerApi->update_project_version: %s\n" % e)
```

### Parameters

| Name     | Type                                    | Description | Notes |
| -------- | --------------------------------------- | ----------- | ----- |
| **body** | [**ProjectVersion**](ProjectVersion.md) | resource    |
| **id**   | **int**                                 | id          |

### Return type

[**ApiResultProjectVersion**](ApiResultProjectVersion.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

