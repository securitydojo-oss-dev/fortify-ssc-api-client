# WebHookResendRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**web_hook_history_ids** | **list[int]** | List containing single webhook history ID to resend | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

