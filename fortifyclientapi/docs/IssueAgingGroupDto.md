# IssueAgingGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | ID of issue aging table grouping attribute. This ID should be passed to issue aging endpoint to return issue aging items that belongs only to this group | 
**name** | **str** | Name of the group to display it on UI. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

