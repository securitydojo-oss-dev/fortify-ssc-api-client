# fortifyclientapi.sscclientapi.IssueControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                             | HTTP request         | Description |
| -------------------------------------------------- | -------------------- | ----------- |
| [**list_issue**](IssueControllerApi.md#list_issue) | **GET** /issues      | list        |
| [**read_issue**](IssueControllerApi.md#read_issue) | **GET** /issues/{id} | read        |

# **list_issue**
> ApiResultListProjectVersionIssue list_issue(q, fulltextsearch, fields=fields, start=start, limit=limit)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
q = 'q_example' # str | A full text search query
fulltextsearch = 'true' # str | Only 'true' is supported (default to true)
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)

try:
    # list
    api_response = api_instance.list_issue(q, fulltextsearch, fields=fields, start=start, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueControllerApi->list_issue: %s\n" % e)
```

### Parameters

| Name               | Type    | Description                                                                                             | Notes                       |
| ------------------ | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **q**              | **str** | A full text search query                                                                                |
| **fulltextsearch** | **str** | Only &#x27;true&#x27; is supported                                                                      | [default to true]           |
| **fields**         | **str** | Output fields                                                                                           | [optional]                  |
| **start**          | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**          | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |

### Return type

[**ApiResultListProjectVersionIssue**](ApiResultListProjectVersionIssue.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_issue**
> ApiResultProjectVersionIssue read_issue(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_issue(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueControllerApi->read_issue: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultProjectVersionIssue**](ApiResultProjectVersionIssue.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

