# UserPhoto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**photo** | **str** | Photo in byte array | 
**photo_mime_type** | **str** | Mime type of user photo. Eg .bmp, .jpg etc | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

