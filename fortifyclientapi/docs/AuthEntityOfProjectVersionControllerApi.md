# fortifyclientapi.sscclientapi.AuthEntityOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                              | HTTP request                                          | Description      |
| --------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------- | ---------------- |
| [**list_auth_entity_of_project_version**](AuthEntityOfProjectVersionControllerApi.md#list_auth_entity_of_project_version)                           | **GET** /projectVersions/{parentId}/authEntities      | list             |
| [**read_auth_entity_of_project_version**](AuthEntityOfProjectVersionControllerApi.md#read_auth_entity_of_project_version)                           | **GET** /projectVersions/{parentId}/authEntities/{id} | read             |
| [**update_collection_auth_entity_of_project_version**](AuthEntityOfProjectVersionControllerApi.md#update_collection_auth_entity_of_project_version) | **PUT** /projectVersions/{parentId}/authEntities      | updateCollection |

# **list_auth_entity_of_project_version**
> ApiResultListAuthenticationEntity list_auth_entity_of_project_version(parent_id, fields=fields, embed=embed, extractusersfromgroups=extractusersfromgroups, includeuniversalaccessentities=includeuniversalaccessentities, entityname=entityname)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthEntityOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
fields = 'fields_example' # str | Output fields (optional)
embed = 'embed_example' # str | Fields to embed (optional)
extractusersfromgroups = true # bool | extractusersfromgroups (optional)
includeuniversalaccessentities = true # bool | includeuniversalaccessentities (optional)
entityname = 'entityname_example' # str | entityname (optional)

try:
    # list
    api_response = api_instance.list_auth_entity_of_project_version(parent_id, fields=fields, embed=embed, extractusersfromgroups=extractusersfromgroups, includeuniversalaccessentities=includeuniversalaccessentities, entityname=entityname)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthEntityOfProjectVersionControllerApi->list_auth_entity_of_project_version: %s\n" % e)
```

### Parameters

| Name                               | Type     | Description                    | Notes      |
| ---------------------------------- | -------- | ------------------------------ | ---------- |
| **parent_id**                      | **int**  | parentId                       |
| **fields**                         | **str**  | Output fields                  | [optional] |
| **embed**                          | **str**  | Fields to embed                | [optional] |
| **extractusersfromgroups**         | **bool** | extractusersfromgroups         | [optional] |
| **includeuniversalaccessentities** | **bool** | includeuniversalaccessentities | [optional] |
| **entityname**                     | **str**  | entityname                     | [optional] |

### Return type

[**ApiResultListAuthenticationEntity**](ApiResultListAuthenticationEntity.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_auth_entity_of_project_version**
> ApiResultAuthenticationEntity read_auth_entity_of_project_version(parent_id, id, fields=fields, embed=embed)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthEntityOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)
embed = 'embed_example' # str | Fields to embed (optional)

try:
    # read
    api_response = api_instance.read_auth_entity_of_project_version(parent_id, id, fields=fields, embed=embed)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthEntityOfProjectVersionControllerApi->read_auth_entity_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description     | Notes      |
| ------------- | ------- | --------------- | ---------- |
| **parent_id** | **int** | parentId        |
| **id**        | **int** | id              |
| **fields**    | **str** | Output fields   | [optional] |
| **embed**     | **str** | Fields to embed | [optional] |

### Return type

[**ApiResultAuthenticationEntity**](ApiResultAuthenticationEntity.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_collection_auth_entity_of_project_version**
> ApiResultListAuthenticationEntity update_collection_auth_entity_of_project_version(body, parent_id)

updateCollection

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthEntityOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = [fortifyclientapi.sscclientapi.AuthenticationEntity()] # list[AuthenticationEntity] | data
parent_id = 789 # int | parentId

try:
    # updateCollection
    api_response = api_instance.update_collection_auth_entity_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthEntityOfProjectVersionControllerApi->update_collection_auth_entity_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                                      | Description | Notes |
| ------------- | --------------------------------------------------------- | ----------- | ----- |
| **body**      | [**list[AuthenticationEntity]**](AuthenticationEntity.md) | data        |
| **parent_id** | **int**                                                   | parentId    |

### Return type

[**ApiResultListAuthenticationEntity**](ApiResultListAuthenticationEntity.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

