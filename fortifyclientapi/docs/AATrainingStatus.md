# AATrainingStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_training_time** | **datetime** |  | [optional] 
**message** | **str** |  | [optional] 
**project_version_id** | **int** |  | [optional] 
**status** | **str** |  | [optional] 
**user_name** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

