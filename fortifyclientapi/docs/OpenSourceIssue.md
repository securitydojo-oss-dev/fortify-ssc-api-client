# OpenSourceIssue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **str** | Category - The category for this issue. | 
**component** | **str** | Component - usually in the format &lt;group&gt;:&lt;artifact&gt; | 
**controllable** | **bool** | Does the application send user input to the vulnerable open source code? | 
**cve** | **str** | CVE ID in the format CVE-xxxxx-xxxx | 
**cve_url** | **str** | CVE URL - URL to get more information about this CVE. | 
**cwe** | **str** | CWE ID in the format CWE-xxxx | 
**cwe_url** | **str** | CWE URL - URL to get more information about the related CWE | 
**engine_type** | **str** | Engine type | 
**filename** | **str** | Filename of the component where the CVE exists | 
**hidden** | **bool** | Is this issue hidden? | [optional] 
**id** | **int** | Application version issue identifier | [optional] 
**invoked** | **bool** | Does the application call the vulnerable open source code? | 
**issue_instance_id** | **str** | Issue instance identifier | 
**license** | **str** | Component license | 
**priority** | **str** | Priority | 
**project_version_id** | **int** | Application version identifier | 
**removed** | **bool** | Is this issue removed? | [optional] 
**suppressed** | **bool** | Is this issue suppressed? | [optional] 
**type** | **str** | Component type | 
**upgrade_to_version** | **str** | Component version to upgrade to | 
**version** | **str** | Component version containing the CVE vulnerability | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

