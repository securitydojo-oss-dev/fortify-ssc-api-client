# BugTracker

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authentication_required** | **bool** | Authentication required | 
**bug_tracker_configs** | [**list[BugTrackerConfiguration]**](BugTrackerConfiguration.md) | Bug tracker configuration | [optional] 
**id** | **str** | Bug tracker identifier | [optional] 
**long_display_name** | **str** | Long display name | [optional] 
**plugin_class_name** | **str** | Class name | 
**plugin_id** | **str** | Bug tracker plugin identifier | 
**short_display_name** | **str** | Short display name | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

