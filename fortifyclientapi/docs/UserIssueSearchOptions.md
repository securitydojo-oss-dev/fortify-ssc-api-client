# UserIssueSearchOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**include_comments_in_history** | **bool** | If &#x27;true&#x27;, comments are included in issue audit history. Used only by flex UI. | [optional] 
**project_version_id** | **int** | Application version ID the option values belong to. Value is empty for default options that can be applied to any application version user has access to if application version specific options are not defined. | [optional] 
**show_hidden** | **bool** | If &#x27;true&#x27;, include hidden issues in search results. If &#x27;false&#x27;, exclude hidden issues from search results. If no options are set, use application version profile settings to get value of this option. | [optional] 
**show_removed** | **bool** | If &#x27;true&#x27;, include removed issues in search results. If &#x27;false&#x27;, exclude removed issues from search results. If no options are set, use application version profile settings to get value of this option. | [optional] 
**show_short_file_names** | **bool** | If &#x27;true&#x27;, only short file names will be displayed in issues list. | [optional] 
**show_suppressed** | **bool** | If &#x27;true&#x27;, include suppressed issues in search results. If &#x27;false&#x27;, exclude suppressed issues from search results. If no options are set, use application version profile settings to get value of this option. | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

