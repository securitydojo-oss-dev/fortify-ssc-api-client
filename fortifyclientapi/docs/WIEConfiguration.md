# WIEConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**wie_instance_url** | **str** | WebInspect Enterprise url | 
**wie_registered** | **bool** | Set to true if WebInspect Enterprise is registered | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

