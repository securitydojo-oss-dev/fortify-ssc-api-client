# ConfigProperties

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**properties** | [**list[ConfigProperty]**](ConfigProperty.md) | Collection of configuration properties defined in the system | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

