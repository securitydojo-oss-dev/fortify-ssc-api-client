# fortifyclientapi.sscclientapi.RoleControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                          | HTTP request           | Description |
| --------------------------------------------------------------- | ---------------------- | ----------- |
| [**create_role**](RoleControllerApi.md#create_role)             | **POST** /roles        | create      |
| [**delete_role**](RoleControllerApi.md#delete_role)             | **DELETE** /roles/{id} | delete      |
| [**list_role**](RoleControllerApi.md#list_role)                 | **GET** /roles         | list        |
| [**multi_delete_role**](RoleControllerApi.md#multi_delete_role) | **DELETE** /roles      | multiDelete |
| [**read_role**](RoleControllerApi.md#read_role)                 | **GET** /roles/{id}    | read        |
| [**update_role**](RoleControllerApi.md#update_role)             | **PUT** /roles/{id}    | update      |

# **create_role**
> ApiResultRole create_role(body)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.RoleControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.Role() # Role | resource

try:
    # create
    api_response = api_instance.create_role(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleControllerApi->create_role: %s\n" % e)
```

### Parameters

| Name     | Type                | Description | Notes |
| -------- | ------------------- | ----------- | ----- |
| **body** | [**Role**](Role.md) | resource    |

### Return type

[**ApiResultRole**](ApiResultRole.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_role**
> ApiResultVoid delete_role(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.RoleControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 'id_example' # str | id

try:
    # delete
    api_response = api_instance.delete_role(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleControllerApi->delete_role: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **str** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_role**
> ApiResultListRole list_role(start=start, limit=limit, q=q, orderby=orderby, fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.RoleControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_role(start=start, limit=limit, q=q, orderby=orderby, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleControllerApi->list_role: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **orderby** | **str** | Fields to order by                                                                                      | [optional]                  |
| **fields**  | **str** | Output fields                                                                                           | [optional]                  |

### Return type

[**ApiResultListRole**](ApiResultListRole.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_role**
> ApiResultVoid multi_delete_role(ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.RoleControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_role(ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleControllerApi->multi_delete_role: %s\n" % e)
```

### Parameters

| Name    | Type    | Description                                    | Notes |
| ------- | ------- | ---------------------------------------------- | ----- |
| **ids** | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_role**
> ApiResultRole read_role(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.RoleControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 'id_example' # str | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_role(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleControllerApi->read_role: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **str** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultRole**](ApiResultRole.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_role**
> ApiResultRole update_role(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.RoleControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.Role() # Role | resource
id = 'id_example' # str | id

try:
    # update
    api_response = api_instance.update_role(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RoleControllerApi->update_role: %s\n" % e)
```

### Parameters

| Name     | Type                | Description | Notes |
| -------- | ------------------- | ----------- | ----- |
| **body** | [**Role**](Role.md) | resource    |
| **id**   | **str**             | id          |

### Return type

[**ApiResultRole**](ApiResultRole.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

