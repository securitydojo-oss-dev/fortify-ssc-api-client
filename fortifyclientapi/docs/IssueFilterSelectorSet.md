# IssueFilterSelectorSet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filter_by_set** | [**list[IssueFilterSelector]**](IssueFilterSelector.md) | List of all possible issues filterring attributes. | 
**group_by_set** | [**list[IssueSelector]**](IssueSelector.md) | List of all possible issues grouping attributes. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

