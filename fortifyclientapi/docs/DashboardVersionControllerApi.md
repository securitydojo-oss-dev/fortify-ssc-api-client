# fortifyclientapi.sscclientapi.DashboardVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                | HTTP request               | Description |
| ------------------------------------------------------------------------------------- | -------------------------- | ----------- |
| [**list_dashboard_version**](DashboardVersionControllerApi.md#list_dashboard_version) | **GET** /dashboardVersions | list        |

# **list_dashboard_version**
> ApiResultListDashboardVersion list_dashboard_version(fields=fields, orderby=orderby, groupby=groupby, start=start, limit=limit, aggregateby=aggregateby, startdate=startdate, enddate=enddate, attributes=attributes, variables=variables, performanceindicators=performanceindicators, attributefilter=attributefilter)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DashboardVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)
groupby = 'groupby_example' # str | Fields to group by (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
aggregateby = 'aggregateby_example' # str | aggregateby (optional)
startdate = 'startdate_example' # str | startdate (optional)
enddate = 'enddate_example' # str | enddate (optional)
attributes = 'attributes_example' # str | attributes (optional)
variables = 'variables_example' # str | variables (optional)
performanceindicators = 'performanceindicators_example' # str | performanceindicators (optional)
attributefilter = 'attributefilter_example' # str | attributefilter (optional)

try:
    # list
    api_response = api_instance.list_dashboard_version(fields=fields, orderby=orderby, groupby=groupby, start=start, limit=limit, aggregateby=aggregateby, startdate=startdate, enddate=enddate, attributes=attributes, variables=variables, performanceindicators=performanceindicators, attributefilter=attributefilter)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DashboardVersionControllerApi->list_dashboard_version: %s\n" % e)
```

### Parameters

| Name                      | Type    | Description                                                                                             | Notes                       |
| ------------------------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields**                | **str** | Output fields                                                                                           | [optional]                  |
| **orderby**               | **str** | Fields to order by                                                                                      | [optional]                  |
| **groupby**               | **str** | Fields to group by                                                                                      | [optional]                  |
| **start**                 | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**                 | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **aggregateby**           | **str** | aggregateby                                                                                             | [optional]                  |
| **startdate**             | **str** | startdate                                                                                               | [optional]                  |
| **enddate**               | **str** | enddate                                                                                                 | [optional]                  |
| **attributes**            | **str** | attributes                                                                                              | [optional]                  |
| **variables**             | **str** | variables                                                                                               | [optional]                  |
| **performanceindicators** | **str** | performanceindicators                                                                                   | [optional]                  |
| **attributefilter**       | **str** | attributefilter                                                                                         | [optional]                  |

### Return type

[**ApiResultListDashboardVersion**](ApiResultListDashboardVersion.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

