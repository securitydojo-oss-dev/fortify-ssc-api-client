# IssueAuditRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**comment** | **str** | Comment | [optional] 
**custom_tag_audit** | [**list[CustomTag]**](CustomTag.md) | Custom tag values that are set for the issue. | [optional] 
**issues** | [**list[EntityStateIdentifier]**](EntityStateIdentifier.md) | Issues to audit | 
**suppressed** | **bool** | Will suppress the issue | [optional] 
**user** | **str** | Username to assign | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

