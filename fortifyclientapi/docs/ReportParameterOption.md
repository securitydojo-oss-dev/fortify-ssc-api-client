# ReportParameterOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**default_value** | **bool** | Indicates whether option is the default value | [optional] 
**description** | **str** |  | [optional] 
**display_value** | **str** | Report parameter option display value | 
**id** | **int** |  | [optional] 
**order** | **int** | Order in which the parameter option should appear | [optional] 
**report_value** | **str** | Report parameter option report value | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

