# AlertSetStatusRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alert_history_ids** | **list[int]** | Alert history ids to set status | 
**status** | **str** | Status of alerts, classified as read or unread | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

