# fortifyclientapi.sscclientapi.CloudSystemMetricsControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                      | HTTP request                 | Description |
| ------------------------------------------------------------------------------------------- | ---------------------------- | ----------- |
| [**get_cloud_system_metrics**](CloudSystemMetricsControllerApi.md#get_cloud_system_metrics) | **GET** /cloudsystem/metrics | get         |

# **get_cloud_system_metrics**
> ApiResultCloudSystemMetrics get_cloud_system_metrics()

get

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudSystemMetricsControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))

try:
    # get
    api_response = api_instance.get_cloud_system_metrics()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudSystemMetricsControllerApi->get_cloud_system_metrics: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiResultCloudSystemMetrics**](ApiResultCloudSystemMetrics.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

