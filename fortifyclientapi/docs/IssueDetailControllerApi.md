# fortifyclientapi.sscclientapi.IssueDetailControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                 | HTTP request               | Description |
| ---------------------------------------------------------------------- | -------------------------- | ----------- |
| [**list_issue_detail**](IssueDetailControllerApi.md#list_issue_detail) | **GET** /issueDetails      | list        |
| [**read_issue_detail**](IssueDetailControllerApi.md#read_issue_detail) | **GET** /issueDetails/{id} | read        |

# **list_issue_detail**
> ApiResultListProjectVersionIssueDetails list_issue_detail(project_name, project_version_name, instance_id)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueDetailControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
project_name = 'project_name_example' # str | projectName
project_version_name = 'project_version_name_example' # str | projectVersionName
instance_id = 'instance_id_example' # str | instanceId

try:
    # list
    api_response = api_instance.list_issue_detail(project_name, project_version_name, instance_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueDetailControllerApi->list_issue_detail: %s\n" % e)
```

### Parameters

| Name                     | Type    | Description        | Notes |
| ------------------------ | ------- | ------------------ | ----- |
| **project_name**         | **str** | projectName        |
| **project_version_name** | **str** | projectVersionName |
| **instance_id**          | **str** | instanceId         |

### Return type

[**ApiResultListProjectVersionIssueDetails**](ApiResultListProjectVersionIssueDetails.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_issue_detail**
> ApiResultProjectVersionIssueDetails read_issue_detail(id, filterset=filterset)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueDetailControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
filterset = 'filterset_example' # str | Filter set to use (optional)

try:
    # read
    api_response = api_instance.read_issue_detail(id, filterset=filterset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueDetailControllerApi->read_issue_detail: %s\n" % e)
```

### Parameters

| Name          | Type    | Description       | Notes      |
| ------------- | ------- | ----------------- | ---------- |
| **id**        | **int** | id                |
| **filterset** | **str** | Filter set to use | [optional] |

### Return type

[**ApiResultProjectVersionIssueDetails**](ApiResultProjectVersionIssueDetails.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

