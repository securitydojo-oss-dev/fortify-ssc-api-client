# ExportOscToCsvRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataset_name** | **str** | Dataset name | [optional] 
**file_name** | **str** | File name to save | 
**include_hidden** | **bool** | Will include hidden issues | [optional] 
**include_removed** | **bool** | Will include removed issues | [optional] 
**include_suppressed** | **bool** | Will include suppressed issues | [optional] 
**limit** | **int** | Limit | [optional] 
**note** | **str** | Note | [optional] 
**project_version_id** | **int** | Application version id to export open source component data | 
**start** | **int** | Start | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

