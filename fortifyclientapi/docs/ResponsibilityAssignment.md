# ResponsibilityAssignment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **str** | Email of user assigned to responsibility | 
**first_name** | **str** | First name of user assigned to responsibility | 
**last_name** | **str** | Last name of user assigned to responsibility | 
**responsibility_description** | **str** | Responsibility description | 
**responsibility_guid** | **str** | Responsibility global unique identifier | 
**responsibility_name** | **str** | Responsibility name | 
**user_id** | **int** | ID of user assigned to responsibility | 
**user_name** | **str** | Name of user assigned to responsibility | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

