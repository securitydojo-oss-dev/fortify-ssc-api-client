# fortifyclientapi.sscclientapi.IssueTemplateControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                       | HTTP request                    | Description |
| -------------------------------------------------------------------------------------------- | ------------------------------- | ----------- |
| [**delete_issue_template**](IssueTemplateControllerApi.md#delete_issue_template)             | **DELETE** /issueTemplates/{id} | delete      |
| [**list_issue_template**](IssueTemplateControllerApi.md#list_issue_template)                 | **GET** /issueTemplates         | list        |
| [**multi_delete_issue_template**](IssueTemplateControllerApi.md#multi_delete_issue_template) | **DELETE** /issueTemplates      | multiDelete |
| [**read_issue_template**](IssueTemplateControllerApi.md#read_issue_template)                 | **GET** /issueTemplates/{id}    | read        |
| [**update_issue_template**](IssueTemplateControllerApi.md#update_issue_template)             | **PUT** /issueTemplates/{id}    | update      |
| [**upload_issue_template**](IssueTemplateControllerApi.md#upload_issue_template)             | **POST** /issueTemplates        | upload      |

# **delete_issue_template**
> ApiResultVoid delete_issue_template(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueTemplateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 'id_example' # str | id

try:
    # delete
    api_response = api_instance.delete_issue_template(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTemplateControllerApi->delete_issue_template: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **str** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_issue_template**
> ApiResultListIssueTemplate list_issue_template(fields=fields, start=start, limit=limit, q=q, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueTemplateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_issue_template(fields=fields, start=start, limit=limit, q=q, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTemplateControllerApi->list_issue_template: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields**  | **str** | Output fields                                                                                           | [optional]                  |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **orderby** | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListIssueTemplate**](ApiResultListIssueTemplate.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_issue_template**
> ApiResultVoid multi_delete_issue_template(ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueTemplateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_issue_template(ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTemplateControllerApi->multi_delete_issue_template: %s\n" % e)
```

### Parameters

| Name    | Type    | Description                                    | Notes |
| ------- | ------- | ---------------------------------------------- | ----- |
| **ids** | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_issue_template**
> ApiResultIssueTemplate read_issue_template(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueTemplateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 'id_example' # str | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_issue_template(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTemplateControllerApi->read_issue_template: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **str** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultIssueTemplate**](ApiResultIssueTemplate.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_issue_template**
> ApiResultIssueTemplate update_issue_template(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueTemplateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.IssueTemplate() # IssueTemplate | resource
id = 'id_example' # str | id

try:
    # update
    api_response = api_instance.update_issue_template(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTemplateControllerApi->update_issue_template: %s\n" % e)
```

### Parameters

| Name     | Type                                  | Description | Notes |
| -------- | ------------------------------------- | ----------- | ----- |
| **body** | [**IssueTemplate**](IssueTemplate.md) | resource    |
| **id**   | **str**                               | id          |

### Return type

[**ApiResultIssueTemplate**](ApiResultIssueTemplate.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **upload_issue_template**
> ApiResultIssueTemplate upload_issue_template(file, name=name, description=description, confirm_ignore_custom_tag_updates=confirm_ignore_custom_tag_updates)

upload

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueTemplateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
file = 'file_example' # str | 
name = 'name_example' # str | name (optional)
description = 'description_example' # str | description (optional)
confirm_ignore_custom_tag_updates = false # bool | confirmIgnoreCustomTagUpdates (optional) (default to false)

try:
    # upload
    api_response = api_instance.upload_issue_template(file, name=name, description=description, confirm_ignore_custom_tag_updates=confirm_ignore_custom_tag_updates)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueTemplateControllerApi->upload_issue_template: %s\n" % e)
```

### Parameters

| Name                                  | Type     | Description                   | Notes                         |
| ------------------------------------- | -------- | ----------------------------- | ----------------------------- |
| **file**                              | **str**  |                               |
| **name**                              | **str**  | name                          | [optional]                    |
| **description**                       | **str**  | description                   | [optional]                    |
| **confirm_ignore_custom_tag_updates** | **bool** | confirmIgnoreCustomTagUpdates | [optional] [default to false] |

### Return type

[**ApiResultIssueTemplate**](ApiResultIssueTemplate.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

