# AlertHistoryEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** |  | [optional] 
**alert_custom_message** | **str** |  | [optional] 
**alert_definition_name** | **str** |  | [optional] 
**alert_message** | **str** |  | [optional] 
**id** | **int** | alert history id | [optional] 
**monitored_entity_type** | **str** |  | [optional] 
**project_and_version_label** | **str** |  | [optional] 
**project_version_id** | **int** |  | [optional] 
**triggered_date** | **datetime** |  | [optional] 
**user_name** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

