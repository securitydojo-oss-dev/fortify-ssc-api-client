# FileToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file_token_type** | **str** | Type of single-use file token | 
**token** | **str** | Value of single-use token to be used as the &#x27;mat&#x27; URL parameter during a file operation | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

