# Folder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color** | **str** | Color that should be used to represent folder on UI | [optional] 
**description** | **str** | Folder description | [optional] 
**editable** | **bool** | Is folder editable | [optional] 
**guid** | **str** | Folder GUID. Guid is unique across all the folders defined for a application version | [optional] 
**id** | **int** | Folder id | [optional] 
**name** | **str** | Folder name | [optional] 
**order_index** | **int** | Numeric value used for ordering folder from the most important to the least important | [optional] 
**project_version_id** | **int** | ID of the application version for which folder is defined | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

