# fortifyclientapi.sscclientapi.SourceFileOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                    | HTTP request                                    | Description |
| ------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------- | ----------- |
| [**list_source_file_of_project_version**](SourceFileOfProjectVersionControllerApi.md#list_source_file_of_project_version) | **GET** /projectVersions/{parentId}/sourceFiles | list        |

# **list_source_file_of_project_version**
> ApiResultListSourceFileDto list_source_file_of_project_version(parent_id, q=q, fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.SourceFileOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
q = 'q_example' # str | The url-encoded value of a source file query expression \"path:&lt;path_to_file_in_quotes&gt;+AND+scan_id:&lt;s_id&gt;\". For example, \"q=path:%22JavaSource%2Forg%2Fowasp%2Fwebgoat%2Flessons%2FCSRF.java%22%2Band%2Bscan_id:1\". If 'scan_id' is not provided, the search defaults to the latest scan of the application version. (optional)
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_source_file_of_project_version(parent_id, q=q, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SourceFileOfProjectVersionControllerApi->list_source_file_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                                                                                                                                                                                                                                                                                                             | Notes      |
| ------------- | ------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------- |
| **parent_id** | **int** | parentId                                                                                                                                                                                                                                                                                                                                                                                |
| **q**         | **str** | The url-encoded value of a source file query expression \&quot;path:&amp;lt;path_to_file_in_quotes&amp;gt;+AND+scan_id:&amp;lt;s_id&amp;gt;\&quot;. For example, \&quot;q&#x3D;path:%22JavaSource%2Forg%2Fowasp%2Fwebgoat%2Flessons%2FCSRF.java%22%2Band%2Bscan_id:1\&quot;. If &#x27;scan_id&#x27; is not provided, the search defaults to the latest scan of the application version. | [optional] |
| **fields**    | **str** | Output fields                                                                                                                                                                                                                                                                                                                                                                           | [optional] |

### Return type

[**ApiResultListSourceFileDto**](ApiResultListSourceFileDto.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

