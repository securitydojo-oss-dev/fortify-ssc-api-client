# AuthenticationCodeResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auth_code** | **str** | Authentication Code | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

