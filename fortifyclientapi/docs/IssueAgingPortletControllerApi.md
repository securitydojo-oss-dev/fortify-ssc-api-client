# fortifyclientapi.sscclientapi.IssueAgingPortletControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                   | HTTP request                 | Description |
| ---------------------------------------------------------------------------------------- | ---------------------------- | ----------- |
| [**get_issue_aging_portlet**](IssueAgingPortletControllerApi.md#get_issue_aging_portlet) | **GET** /portlets/issueaging | get         |

# **get_issue_aging_portlet**
> ApiResultIssueAgingPortlet get_issue_aging_portlet()

get

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueAgingPortletControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))

try:
    # get
    api_response = api_instance.get_issue_aging_portlet()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueAgingPortletControllerApi->get_issue_aging_portlet: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiResultIssueAgingPortlet**](ApiResultIssueAgingPortlet.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

