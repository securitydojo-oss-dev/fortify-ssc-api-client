# WebHookDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** | If this webhook is active | [optional] 
**allow_insecure_tls** | **bool** | Allow connection even if webhook destination server certificate cannot be verified. Both certificate trust and server name validation are suppressed. Applies only to HTTPS protocol. | [optional] 
**content_type** | **str** | The way how webhook will be delivered | 
**created_by** | **str** | User who created this webhook | [optional] 
**creation_date** | **datetime** | Date when this webhook was created | [optional] 
**description** | **str** | Description | [optional] 
**id** | **int** | ID required when referencing an existing webhook definition | [optional] 
**last_status** | **str** | Status of this webhook&#x27;s last completed request | [optional] 
**monitor_apps** | **str** | If this webhook is set to monitor all application versions or a custom selection. If set to all application versions, it takes precedence over any custom selection application version ids. | [optional] 
**monitor_events** | **str** | If this webhook is set to monitor all events or custom selection of events. If set to all events, it takes precedence over any custom selection of global/application events. | [optional] 
**monitored_app_version_events** | **list[str]** | Custom selection of application version based events to monitor by this webhook. No need to provide custom selection in case you choose to monitor all events. | [optional] 
**monitored_app_version_ids** | **list[int]** | Custom selection of application versions monitored by this webhook. No need to provide custom selection in case you choose to monitor all application versions. | [optional] 
**monitored_global_events** | **list[str]** | Custom selection of global events (events that are not triggered by an action in specific application version) to monitor by this webhook. No need to provide custom selection in case you choose to monitor all events. | [optional] 
**object_version** | **int** | Webhook version stored on the server. This value is used for optimistic locking to prevent concurrent modification by different users at the same time. | [optional] 
**restricted** | **bool** | True if current user is missing some permissions to fully manage this webhook | [optional] 
**secret_value** | **str** | Secret value will be used to sign webhook request. Requirements: min 8 and max 128 of printable ASCII characters (alphanumeric, punctuation, space). | [optional] 
**url** | **str** | Destination URL | 
**use_ssc_proxy** | **bool** | Enable using SSC proxy configuration if webhook receiver is behind a proxy.SSC proxy must be also enabled and configured. | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

