# ProjectVersionRefreshRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**project_version_ids** | **list[int]** | List containing single application version ID to refresh | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

