# IssuePrimaryTag

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag_guid** | **str** |  | [optional] 
**tag_id** | **int** |  | [optional] 
**tag_name** | **str** |  | [optional] 
**tag_value** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

