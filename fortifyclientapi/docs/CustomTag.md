# CustomTag

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom_tag_type** | **str** | Custom tag type. | 
**default_value** | **str** | Default value of the custom tag. Actual string value is presented here. | [optional] 
**default_value_index** | **int** | Index of default value of the custom tag. This is ordinal number of the item in CustomTagLookup collection. | [optional] 
**deletable** | **bool** | Flag that says if custom tag can be deleted. Custom tag which values are currently in use cannot be deleted. | [optional] 
**description** | **str** | Custom tag description. | [optional] 
**extensible** | **bool** | Flag that says if custom tag is extensible or not. | 
**guid** | **str** | Custom tag GUID. | 
**hidden** | **bool** | Is custom tag hidden or not. | 
**id** | **int** | Custom tag id | [optional] 
**in_use** | **bool** | Is custom tag values are selected for any issues in the system. | [optional] 
**name** | **str** | Custom tag unique name. | 
**object_version** | **int** | Custom tag version stored on the server. This value is used for optimistic locking of the custom tag object to prevent concurrent modification of the custom tag by different users at the same time. | 
**primary_tag** | **bool** | If this custom tag is set as primary tag for a specific application version. This value is initialized only if custom tags for specific application version are requested. | [optional] 
**restriction** | **bool** | Flag is set to true if special permission is required to set values of this custom tag. | [optional] 
**restriction_type** | **str** | Special permission type if restriction is set to TRUE. | [optional] 
**value_list** | [**list[CustomTagLookup]**](CustomTagLookup.md) | Collection of all possible custom tag values. | [optional] 
**value_type** | **str** | Custom tag value type. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

