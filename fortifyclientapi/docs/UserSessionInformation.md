# UserSessionInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application_context_path** | **str** | Application context path | 
**cloud_scan_config** | [**CloudScanConfig**](CloudScanConfig.md) |  | 
**collab_module_enabled** | **bool** | Set to true if collaboration module is enabled | 
**display_user_details** | **bool** | Display user details if set to true | 
**edast_config** | [**EdastConfig**](EdastConfig.md) |  | 
**email** | **str** | User email | 
**first_name** | **str** | User first name | 
**fortify_user_type** | **str** | Fortify user type eg. LOCAL | 
**last_name** | **str** | User last name | 
**license_capabilities** | [**list[LicenseCapability]**](LicenseCapability.md) | List of license capabilities | 
**on_demand_mode** | **bool** | Set to true if on demand mode is enabled | 
**permissions** | **list[str]** | User permissions | 
**preferences** | [**UserPreferences**](UserPreferences.md) |  | 
**roles** | [**list[Role]**](Role.md) | User roles | 
**session_id** | **str** | User session id | 
**user_photo** | [**UserPhoto**](UserPhoto.md) |  | 
**user_type** | **int** | User Type integer eg. 0 for Local | 
**username** | **str** | User name | 
**webapp_version** | **str** | Web application version | 
**wie_config** | [**WIEConfiguration**](WIEConfiguration.md) |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

