# EventLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**detailed_note** | **str** |  | [optional] 
**entity_id** | **int** |  | [optional] 
**event_date** | **datetime** |  | [optional] 
**event_type** | **str** |  | [optional] 
**id** | **int** |  | [optional] 
**project_version_id** | **int** |  | [optional] 
**user_name** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

