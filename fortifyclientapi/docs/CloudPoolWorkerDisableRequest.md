# CloudPoolWorkerDisableRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**worker_uuids** | **list[str]** | List of worker UUIds to disable from the cloud pool | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

