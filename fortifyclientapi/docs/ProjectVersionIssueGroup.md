# ProjectVersionIssueGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audited_count** | **int** | Audited issue count | 
**clean_name** | **str** | Issue group name not containing count, dashes etc | 
**id** | **str** | Identifier | 
**name** | **str** | Issue group name | 
**total_count** | **int** | Issue total count | 
**visible_count** | **int** | Visible issues count | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

