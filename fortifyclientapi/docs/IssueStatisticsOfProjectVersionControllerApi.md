# fortifyclientapi.sscclientapi.IssueStatisticsOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                   | HTTP request                                        | Description |
| ---------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------- | ----------- |
| [**list_issue_statistics_of_project_version**](IssueStatisticsOfProjectVersionControllerApi.md#list_issue_statistics_of_project_version) | **GET** /projectVersions/{parentId}/issueStatistics | list        |

# **list_issue_statistics_of_project_version**
> ApiResultListIssueStatistics list_issue_statistics_of_project_version(parent_id, filterset=filterset)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueStatisticsOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
filterset = 'filterset_example' # str | Filter set to use (optional)

try:
    # list
    api_response = api_instance.list_issue_statistics_of_project_version(parent_id, filterset=filterset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueStatisticsOfProjectVersionControllerApi->list_issue_statistics_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description       | Notes      |
| ------------- | ------- | ----------------- | ---------- |
| **parent_id** | **int** | parentId          |
| **filterset** | **str** | Filter set to use | [optional] |

### Return type

[**ApiResultListIssueStatistics**](ApiResultListIssueStatistics.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

