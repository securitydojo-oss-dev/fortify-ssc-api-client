# CustomTagInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**guid** | **str** | Custom tag GUID | [optional] 
**id** | **int** | Custom tag id | 
**name** | **str** | Custom tag unique name | [optional] 
**value_list** | [**list[CustomTagLookupInfo]**](CustomTagLookupInfo.md) | Collection of custom tag values | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

