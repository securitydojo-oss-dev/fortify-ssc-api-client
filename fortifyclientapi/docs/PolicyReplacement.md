# PolicyReplacement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from_policy** | **str** |  | [optional] 
**to_policy** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

