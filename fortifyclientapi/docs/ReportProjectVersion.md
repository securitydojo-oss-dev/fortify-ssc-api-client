# ReportProjectVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**development_phase** | **str** | Report application version development phase | 
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

