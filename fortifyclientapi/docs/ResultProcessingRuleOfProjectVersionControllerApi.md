# fortifyclientapi.sscclientapi.ResultProcessingRuleOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                                                              | HTTP request                                              | Description      |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------- | ---------------- |
| [**list_result_processing_rule_of_project_version**](ResultProcessingRuleOfProjectVersionControllerApi.md#list_result_processing_rule_of_project_version)                           | **GET** /projectVersions/{parentId}/resultProcessingRules | list             |
| [**update_collection_result_processing_rule_of_project_version**](ResultProcessingRuleOfProjectVersionControllerApi.md#update_collection_result_processing_rule_of_project_version) | **PUT** /projectVersions/{parentId}/resultProcessingRules | updateCollection |

# **list_result_processing_rule_of_project_version**
> ApiResultListResultProcessingRule list_result_processing_rule_of_project_version(parent_id, fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ResultProcessingRuleOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_result_processing_rule_of_project_version(parent_id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResultProcessingRuleOfProjectVersionControllerApi->list_result_processing_rule_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description   | Notes      |
| ------------- | ------- | ------------- | ---------- |
| **parent_id** | **int** | parentId      |
| **fields**    | **str** | Output fields | [optional] |

### Return type

[**ApiResultListResultProcessingRule**](ApiResultListResultProcessingRule.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_collection_result_processing_rule_of_project_version**
> ApiResultListResultProcessingRule update_collection_result_processing_rule_of_project_version(body, parent_id)

updateCollection

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ResultProcessingRuleOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = [fortifyclientapi.sscclientapi.ResultProcessingRule()] # list[ResultProcessingRule] | data
parent_id = 789 # int | parentId

try:
    # updateCollection
    api_response = api_instance.update_collection_result_processing_rule_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResultProcessingRuleOfProjectVersionControllerApi->update_collection_result_processing_rule_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                                      | Description | Notes |
| ------------- | --------------------------------------------------------- | ----------- | ----- |
| **body**      | [**list[ResultProcessingRule]**](ResultProcessingRule.md) | data        |
| **parent_id** | **int**                                                   | parentId    |

### Return type

[**ApiResultListResultProcessingRule**](ApiResultListResultProcessingRule.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

