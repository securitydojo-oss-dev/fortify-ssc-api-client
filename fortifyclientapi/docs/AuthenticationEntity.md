# AuthenticationEntity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**embed** | [**EmbeddedRoles**](EmbeddedRoles.md) |  | [optional] 
**display_name** | **str** | Display Name supports the use of wildcard matching. So, for example, \&quot;L*\&quot; will match Lando Calrissian and Lara Croft. | [optional] 
**email** | **str** |  | [optional] 
**entity_name** | **str** |  | [optional] 
**first_name** | **str** |  | [optional] 
**id** | **int** | Authentication entity id | [optional] 
**is_ldap** | **bool** |  | [optional] 
**last_name** | **str** |  | [optional] 
**ldap_dn** | **str** | Distinguished Name (DN) that is only set for LDAP user accounts | [optional] 
**type** | **str** |  | [optional] 
**user_photo** | [**UserPhoto**](UserPhoto.md) |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

