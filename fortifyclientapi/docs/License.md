# License

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capabilities** | [**list[LicenseCapability]**](LicenseCapability.md) | List of all license capabilities | [optional] 
**creation_date** | **datetime** | Date when license was created | [optional] 
**description** | **str** | License description | [optional] 
**expiration_date** | **datetime** | Date when license is going to be expired | [optional] 
**owner_name** | **str** | Name of the license owner | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

