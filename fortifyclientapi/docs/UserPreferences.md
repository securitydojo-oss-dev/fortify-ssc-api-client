# UserPreferences

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_format** | **str** | Preferred date format | 
**email** | **str** | User email | 
**email_alerts** | **bool** | Receive email alerts if set to true | 
**monitor_all_runtime_apps** | **bool** | Monitors all runtime applications if set to true | 
**project_version_list_mode** | **str** | Enum for ProjectVersionListMode with values DEFAULT, CUSTOM, ALL | 
**receive_runtime_alerts** | **bool** | Receive runtime alerts if set to true | 
**runtime_alert_definition_id** | **int** | Runtime alert definition identifier | 
**time_format** | **str** | Preferred time format | 
**username** | **str** | User name | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

