# TraceNodeDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**child_nodes** | [**list[TraceNodeDto]**](TraceNodeDto.md) | Child nodes of the current node. | 
**detail** | **bool** | Detail node marker. | 
**evidence** | **str** | Node evidence text. | 
**file** | **str** | Name of the file associated with this node. | 
**full_path** | **str** | Full path to source file associated with this node. | 
**line** | **int** | Source file line number where issue was found. | 
**node_type** | **str** | Type of the trace node. | 
**primary** | **bool** | Primry node marker. | 
**text** | **str** | Node display text. | 
**tool_tip** | **str** | Node description/tooltip. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

