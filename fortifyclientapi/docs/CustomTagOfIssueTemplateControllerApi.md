# fortifyclientapi.sscclientapi.CustomTagOfIssueTemplateControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                  | HTTP request                                  | Description |
| ----------------------------------------------------------------------------------------------------------------------- | --------------------------------------------- | ----------- |
| [**list_custom_tag_of_issue_template**](CustomTagOfIssueTemplateControllerApi.md#list_custom_tag_of_issue_template)     | **GET** /issueTemplates/{parentId}/customTags | list        |
| [**update_custom_tag_of_issue_template**](CustomTagOfIssueTemplateControllerApi.md#update_custom_tag_of_issue_template) | **PUT** /issueTemplates/{parentId}/customTags | update      |

# **list_custom_tag_of_issue_template**
> ApiResultListCustomTag list_custom_tag_of_issue_template(parent_id, fields=fields, start=start, limit=limit, q=q, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CustomTagOfIssueTemplateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 'parent_id_example' # str | parentId
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_custom_tag_of_issue_template(parent_id, fields=fields, start=start, limit=limit, q=q, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomTagOfIssueTemplateControllerApi->list_custom_tag_of_issue_template: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                             | Notes                       |
| ------------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **parent_id** | **str** | parentId                                                                                                |
| **fields**    | **str** | Output fields                                                                                           | [optional]                  |
| **start**     | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**     | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**         | **str** | A search query                                                                                          | [optional]                  |
| **orderby**   | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListCustomTag**](ApiResultListCustomTag.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_custom_tag_of_issue_template**
> ApiResultListCustomTag update_custom_tag_of_issue_template(body, parent_id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CustomTagOfIssueTemplateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = [fortifyclientapi.sscclientapi.CustomTag()] # list[CustomTag] | customTags
parent_id = 'parent_id_example' # str | parentId

try:
    # update
    api_response = api_instance.update_custom_tag_of_issue_template(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomTagOfIssueTemplateControllerApi->update_custom_tag_of_issue_template: %s\n" % e)
```

### Parameters

| Name          | Type                                | Description | Notes |
| ------------- | ----------------------------------- | ----------- | ----- |
| **body**      | [**list[CustomTag]**](CustomTag.md) | customTags  |
| **parent_id** | **str**                             | parentId    |

### Return type

[**ApiResultListCustomTag**](ApiResultListCustomTag.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

