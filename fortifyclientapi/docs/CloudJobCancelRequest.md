# CloudJobCancelRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_tokens** | **list[str]** | List containing single ScanCentral token to cancel | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

