# UserSessionState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **str** |  | 
**id** | **int** |  | [optional] 
**name** | **str** |  | 
**project_version_id** | **int** |  | [optional] 
**username** | **str** |  | 
**value** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

