# ApplicationState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_configuration_mode** | **bool** | Indicates whether SSC application is in auto configuration mode | [optional] 
**config_visit_required** | **bool** | Indicates whether a config visit is needed | [optional] 
**maintenance_mode** | **bool** | Indicates whether SSC application is in maintenance mode | [optional] 
**restart_required** | **bool** | Indicates whether a restart is needed | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

