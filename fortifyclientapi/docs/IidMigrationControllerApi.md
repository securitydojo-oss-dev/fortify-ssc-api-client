# fortifyclientapi.sscclientapi.IidMigrationControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                    | HTTP request                | Description |
| ------------------------------------------------------------------------- | --------------------------- | ----------- |
| [**read_iid_migration**](IidMigrationControllerApi.md#read_iid_migration) | **GET** /iidMigrations/{id} | read        |

# **read_iid_migration**
> ApiResultIIDMigration read_iid_migration(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IidMigrationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_iid_migration(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IidMigrationControllerApi->read_iid_migration: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultIIDMigration**](ApiResultIIDMigration.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

