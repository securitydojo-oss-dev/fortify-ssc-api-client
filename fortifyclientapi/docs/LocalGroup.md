# LocalGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**externally_managed** | **bool** | If Local Group is managed by an external service | [optional] 
**id** | **int** | ID required when referencing an existing Local Group | [optional] 
**name** | **str** | Local Group&#x27;s name. Note: value is readonly for externally managed groups. | 
**object_version** | **int** | Local Group version stored on the server. This value is used to prevent concurrent updates. | [optional] 
**roles** | [**list[Role]**](Role.md) | List of Roles assigned to a Local Group | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

