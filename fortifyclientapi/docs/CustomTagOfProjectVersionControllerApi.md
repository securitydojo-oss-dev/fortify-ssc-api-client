# fortifyclientapi.sscclientapi.CustomTagOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                           | HTTP request                                        | Description      |
| ------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------- | ---------------- |
| [**create_custom_tag_of_project_version**](CustomTagOfProjectVersionControllerApi.md#create_custom_tag_of_project_version)                       | **POST** /projectVersions/{parentId}/customTags     | create           |
| [**list_custom_tag_of_project_version**](CustomTagOfProjectVersionControllerApi.md#list_custom_tag_of_project_version)                           | **GET** /projectVersions/{parentId}/customTags      | list             |
| [**update_collection_custom_tag_of_project_version**](CustomTagOfProjectVersionControllerApi.md#update_collection_custom_tag_of_project_version) | **PUT** /projectVersions/{parentId}/customTags      | updateCollection |
| [**update_custom_tag_of_project_version**](CustomTagOfProjectVersionControllerApi.md#update_custom_tag_of_project_version)                       | **PUT** /projectVersions/{parentId}/customTags/{id} | update           |

# **create_custom_tag_of_project_version**
> ApiResultCustomTag create_custom_tag_of_project_version(body, parent_id)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CustomTagOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.CustomTag() # CustomTag | resource
parent_id = 789 # int | parentId

try:
    # create
    api_response = api_instance.create_custom_tag_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomTagOfProjectVersionControllerApi->create_custom_tag_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                          | Description | Notes |
| ------------- | ----------------------------- | ----------- | ----- |
| **body**      | [**CustomTag**](CustomTag.md) | resource    |
| **parent_id** | **int**                       | parentId    |

### Return type

[**ApiResultCustomTag**](ApiResultCustomTag.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_custom_tag_of_project_version**
> ApiResultListCustomTag list_custom_tag_of_project_version(parent_id, fields=fields, start=start, limit=limit, q=q, orderby=orderby, includeall=includeall)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CustomTagOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)
includeall = false # bool | includeall (optional) (default to false)

try:
    # list
    api_response = api_instance.list_custom_tag_of_project_version(parent_id, fields=fields, start=start, limit=limit, q=q, orderby=orderby, includeall=includeall)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomTagOfProjectVersionControllerApi->list_custom_tag_of_project_version: %s\n" % e)
```

### Parameters

| Name           | Type     | Description                                                                                             | Notes                         |
| -------------- | -------- | ------------------------------------------------------------------------------------------------------- | ----------------------------- |
| **parent_id**  | **int**  | parentId                                                                                                |
| **fields**     | **str**  | Output fields                                                                                           | [optional]                    |
| **start**      | **int**  | A start offset in object listing                                                                        | [optional] [default to 0]     |
| **limit**      | **int**  | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200]   |
| **q**          | **str**  | A search query                                                                                          | [optional]                    |
| **orderby**    | **str**  | Fields to order by                                                                                      | [optional]                    |
| **includeall** | **bool** | includeall                                                                                              | [optional] [default to false] |

### Return type

[**ApiResultListCustomTag**](ApiResultListCustomTag.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_collection_custom_tag_of_project_version**
> ApiResultListCustomTag update_collection_custom_tag_of_project_version(body, parent_id)

updateCollection

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CustomTagOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = [fortifyclientapi.sscclientapi.CustomTag()] # list[CustomTag] | data
parent_id = 789 # int | parentId

try:
    # updateCollection
    api_response = api_instance.update_collection_custom_tag_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomTagOfProjectVersionControllerApi->update_collection_custom_tag_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                | Description | Notes |
| ------------- | ----------------------------------- | ----------- | ----- |
| **body**      | [**list[CustomTag]**](CustomTag.md) | data        |
| **parent_id** | **int**                             | parentId    |

### Return type

[**ApiResultListCustomTag**](ApiResultListCustomTag.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_custom_tag_of_project_version**
> ApiResultCustomTag update_custom_tag_of_project_version(body, parent_id, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CustomTagOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.CustomTag() # CustomTag | data
parent_id = 789 # int | parentId
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_custom_tag_of_project_version(body, parent_id, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomTagOfProjectVersionControllerApi->update_custom_tag_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                          | Description | Notes |
| ------------- | ----------------------------- | ----------- | ----- |
| **body**      | [**CustomTag**](CustomTag.md) | data        |
| **parent_id** | **int**                       | parentId    |
| **id**        | **int**                       | id          |

### Return type

[**ApiResultCustomTag**](ApiResultCustomTag.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

