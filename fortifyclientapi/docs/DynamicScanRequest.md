# DynamicScanRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique identifier of dynamic scan request | 
**last_update_date** | **datetime** | The date that the dynamic scan request was updated | [optional] 
**object_version** | **int** | The object version of the dynamic scan request | [optional] 
**parameters** | [**list[DynamicScanRequestParameter]**](DynamicScanRequestParameter.md) | Parameters that are needed for dynamic scan request | 
**requested_date** | **datetime** | The date that the dynamic scan request was submitted | [optional] 
**status** | **str** | The status of the dynamic scan request | [optional] 
**submitter** | **str** | The id of the user who submitted the dynamic scan request | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

