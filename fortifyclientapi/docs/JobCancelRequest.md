# JobCancelRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_ids** | **list[str]** | List containing single job ID to cancel | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

