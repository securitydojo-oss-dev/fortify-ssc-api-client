# fortifyclientapi.sscclientapi.CloudWorkerOfCloudPoolControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                  | HTTP request                                           | Description                       |
| ----------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------ | --------------------------------- |
| [**assign_cloud_worker_of_cloud_pool**](CloudWorkerOfCloudPoolControllerApi.md#assign_cloud_worker_of_cloud_pool)       | **POST** /cloudpools/{parentId}/workers/action/assign  | Assign workers to the cloud pool  |
| [**disable_cloud_worker_of_cloud_pool**](CloudWorkerOfCloudPoolControllerApi.md#disable_cloud_worker_of_cloud_pool)     | **POST** /cloudpools/{parentId}/workers/action/disable | Disable workers in the cloud pool |
| [**do_action_cloud_worker_of_cloud_pool**](CloudWorkerOfCloudPoolControllerApi.md#do_action_cloud_worker_of_cloud_pool) | **POST** /cloudpools/{parentId}/workers/action         | doAction                          |
| [**list_cloud_worker_of_cloud_pool**](CloudWorkerOfCloudPoolControllerApi.md#list_cloud_worker_of_cloud_pool)           | **GET** /cloudpools/{parentId}/workers                 | list                              |
| [**replace_cloud_worker_of_cloud_pool**](CloudWorkerOfCloudPoolControllerApi.md#replace_cloud_worker_of_cloud_pool)     | **POST** /cloudpools/{parentId}/workers/action/replace | Replace workers in the cloud pool |

# **assign_cloud_worker_of_cloud_pool**
> ApiResultCloudPoolWorkerActionResponse assign_cloud_worker_of_cloud_pool(body, parent_id)

Assign workers to the cloud pool

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudWorkerOfCloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.CloudPoolWorkerAssignRequest() # CloudPoolWorkerAssignRequest | resource
parent_id = 'parent_id_example' # str | parentId

try:
    # Assign workers to the cloud pool
    api_response = api_instance.assign_cloud_worker_of_cloud_pool(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudWorkerOfCloudPoolControllerApi->assign_cloud_worker_of_cloud_pool: %s\n" % e)
```

### Parameters

| Name          | Type                                                                | Description | Notes |
| ------------- | ------------------------------------------------------------------- | ----------- | ----- |
| **body**      | [**CloudPoolWorkerAssignRequest**](CloudPoolWorkerAssignRequest.md) | resource    |
| **parent_id** | **str**                                                             | parentId    |

### Return type

[**ApiResultCloudPoolWorkerActionResponse**](ApiResultCloudPoolWorkerActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **disable_cloud_worker_of_cloud_pool**
> ApiResultCloudPoolWorkerActionResponse disable_cloud_worker_of_cloud_pool(body, parent_id)

Disable workers in the cloud pool

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudWorkerOfCloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.CloudPoolWorkerDisableRequest() # CloudPoolWorkerDisableRequest | resource
parent_id = 'parent_id_example' # str | parentId

try:
    # Disable workers in the cloud pool
    api_response = api_instance.disable_cloud_worker_of_cloud_pool(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudWorkerOfCloudPoolControllerApi->disable_cloud_worker_of_cloud_pool: %s\n" % e)
```

### Parameters

| Name          | Type                                                                  | Description | Notes |
| ------------- | --------------------------------------------------------------------- | ----------- | ----- |
| **body**      | [**CloudPoolWorkerDisableRequest**](CloudPoolWorkerDisableRequest.md) | resource    |
| **parent_id** | **str**                                                               | parentId    |

### Return type

[**ApiResultCloudPoolWorkerActionResponse**](ApiResultCloudPoolWorkerActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_action_cloud_worker_of_cloud_pool**
> ApiResultApiActionResponse do_action_cloud_worker_of_cloud_pool(body, parent_id)

doAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudWorkerOfCloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiCollectionActionstring() # ApiCollectionActionstring | collectionAction
parent_id = 'parent_id_example' # str | parentId

try:
    # doAction
    api_response = api_instance.do_action_cloud_worker_of_cloud_pool(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudWorkerOfCloudPoolControllerApi->do_action_cloud_worker_of_cloud_pool: %s\n" % e)
```

### Parameters

| Name          | Type                                                          | Description      | Notes |
| ------------- | ------------------------------------------------------------- | ---------------- | ----- |
| **body**      | [**ApiCollectionActionstring**](ApiCollectionActionstring.md) | collectionAction |
| **parent_id** | **str**                                                       | parentId         |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_cloud_worker_of_cloud_pool**
> ApiResultListCloudWorker list_cloud_worker_of_cloud_pool(parent_id, fields=fields, start=start, limit=limit, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudWorkerOfCloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 'parent_id_example' # str | parentId
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_cloud_worker_of_cloud_pool(parent_id, fields=fields, start=start, limit=limit, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudWorkerOfCloudPoolControllerApi->list_cloud_worker_of_cloud_pool: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                             | Notes                       |
| ------------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **parent_id** | **str** | parentId                                                                                                |
| **fields**    | **str** | Output fields                                                                                           | [optional]                  |
| **start**     | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**     | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **orderby**   | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListCloudWorker**](ApiResultListCloudWorker.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **replace_cloud_worker_of_cloud_pool**
> ApiResultCloudPoolWorkerActionResponse replace_cloud_worker_of_cloud_pool(body, parent_id)

Replace workers in the cloud pool

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudWorkerOfCloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.CloudPoolWorkerReplaceRequest() # CloudPoolWorkerReplaceRequest | resource
parent_id = 'parent_id_example' # str | parentId

try:
    # Replace workers in the cloud pool
    api_response = api_instance.replace_cloud_worker_of_cloud_pool(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudWorkerOfCloudPoolControllerApi->replace_cloud_worker_of_cloud_pool: %s\n" % e)
```

### Parameters

| Name          | Type                                                                  | Description | Notes |
| ------------- | --------------------------------------------------------------------- | ----------- | ----- |
| **body**      | [**CloudPoolWorkerReplaceRequest**](CloudPoolWorkerReplaceRequest.md) | resource    |
| **parent_id** | **str**                                                               | parentId    |

### Return type

[**ApiResultCloudPoolWorkerActionResponse**](ApiResultCloudPoolWorkerActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

