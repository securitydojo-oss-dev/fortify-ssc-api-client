# ArtifactApproveRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**artifact_ids** | **list[int]** | List containing single artifact ID to approve | 
**comment** | **str** | Approval comments | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

