# fortifyclientapi.sscclientapi.AlertHistoryControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                        | HTTP request                      | Description                                    |
| --------------------------------------------------------------------------------------------- | --------------------------------- | ---------------------------------------------- |
| [**do_action_alert_history**](AlertHistoryControllerApi.md#do_action_alert_history)           | **POST** /alerts/action           | doAction                                       |
| [**list_alert_history**](AlertHistoryControllerApi.md#list_alert_history)                     | **GET** /alerts                   | list                                           |
| [**set_status_for_alert_history**](AlertHistoryControllerApi.md#set_status_for_alert_history) | **POST** /alerts/action/setStatus | Mark a triggered alert event as read or unread |

# **do_action_alert_history**
> ApiResultApiActionResponse do_action_alert_history(body)

doAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AlertHistoryControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiCollectionActionlong() # ApiCollectionActionlong | collectionAction

try:
    # doAction
    api_response = api_instance.do_action_alert_history(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AlertHistoryControllerApi->do_action_alert_history: %s\n" % e)
```

### Parameters

| Name     | Type                                                      | Description      | Notes |
| -------- | --------------------------------------------------------- | ---------------- | ----- |
| **body** | [**ApiCollectionActionlong**](ApiCollectionActionlong.md) | collectionAction |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_alert_history**
> ApiResultListAlertHistoryEntry list_alert_history(start=start, limit=limit, q=q, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AlertHistoryControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_alert_history(start=start, limit=limit, q=q, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AlertHistoryControllerApi->list_alert_history: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **orderby** | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListAlertHistoryEntry**](ApiResultListAlertHistoryEntry.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **set_status_for_alert_history**
> ApiResultVoid set_status_for_alert_history(body)

Mark a triggered alert event as read or unread

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AlertHistoryControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.AlertSetStatusRequest() # AlertSetStatusRequest | resource

try:
    # Mark a triggered alert event as read or unread
    api_response = api_instance.set_status_for_alert_history(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AlertHistoryControllerApi->set_status_for_alert_history: %s\n" % e)
```

### Parameters

| Name     | Type                                                  | Description | Notes |
| -------- | ----------------------------------------------------- | ----------- | ----- |
| **body** | [**AlertSetStatusRequest**](AlertSetStatusRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

