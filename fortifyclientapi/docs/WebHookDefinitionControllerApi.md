# fortifyclientapi.sscclientapi.WebHookDefinitionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                     | HTTP request              | Description |
| ---------------------------------------------------------------------------------------------------------- | ------------------------- | ----------- |
| [**create_web_hook_definition**](WebHookDefinitionControllerApi.md#create_web_hook_definition)             | **POST** /webhooks        | create      |
| [**delete_web_hook_definition**](WebHookDefinitionControllerApi.md#delete_web_hook_definition)             | **DELETE** /webhooks/{id} | delete      |
| [**list_web_hook_definition**](WebHookDefinitionControllerApi.md#list_web_hook_definition)                 | **GET** /webhooks         | list        |
| [**multi_delete_web_hook_definition**](WebHookDefinitionControllerApi.md#multi_delete_web_hook_definition) | **DELETE** /webhooks      | multiDelete |
| [**read_web_hook_definition**](WebHookDefinitionControllerApi.md#read_web_hook_definition)                 | **GET** /webhooks/{id}    | read        |
| [**update_web_hook_definition**](WebHookDefinitionControllerApi.md#update_web_hook_definition)             | **PUT** /webhooks/{id}    | update      |

# **create_web_hook_definition**
> ApiResultWebHookDefinition create_web_hook_definition(body)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.WebHookDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.WebHookDefinition() # WebHookDefinition | webHookDefinition

try:
    # create
    api_response = api_instance.create_web_hook_definition(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WebHookDefinitionControllerApi->create_web_hook_definition: %s\n" % e)
```

### Parameters

| Name     | Type                                          | Description       | Notes |
| -------- | --------------------------------------------- | ----------------- | ----- |
| **body** | [**WebHookDefinition**](WebHookDefinition.md) | webHookDefinition |

### Return type

[**ApiResultWebHookDefinition**](ApiResultWebHookDefinition.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_web_hook_definition**
> ApiResultVoid delete_web_hook_definition(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.WebHookDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_web_hook_definition(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WebHookDefinitionControllerApi->delete_web_hook_definition: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_web_hook_definition**
> ApiResultListWebHookDefinition list_web_hook_definition(fields=fields, start=start, limit=limit, q=q, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.WebHookDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_web_hook_definition(fields=fields, start=start, limit=limit, q=q, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WebHookDefinitionControllerApi->list_web_hook_definition: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields**  | **str** | Output fields                                                                                           | [optional]                  |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **orderby** | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListWebHookDefinition**](ApiResultListWebHookDefinition.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_web_hook_definition**
> ApiResultVoid multi_delete_web_hook_definition(ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.WebHookDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_web_hook_definition(ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WebHookDefinitionControllerApi->multi_delete_web_hook_definition: %s\n" % e)
```

### Parameters

| Name    | Type    | Description                                    | Notes |
| ------- | ------- | ---------------------------------------------- | ----- |
| **ids** | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_web_hook_definition**
> ApiResultWebHookDefinition read_web_hook_definition(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.WebHookDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_web_hook_definition(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WebHookDefinitionControllerApi->read_web_hook_definition: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultWebHookDefinition**](ApiResultWebHookDefinition.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_web_hook_definition**
> ApiResultWebHookDefinition update_web_hook_definition(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.WebHookDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.WebHookDefinition() # WebHookDefinition | resource
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_web_hook_definition(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WebHookDefinitionControllerApi->update_web_hook_definition: %s\n" % e)
```

### Parameters

| Name     | Type                                          | Description | Notes |
| -------- | --------------------------------------------- | ----------- | ----- |
| **body** | [**WebHookDefinition**](WebHookDefinition.md) | resource    |
| **id**   | **int**                                       | id          |

### Return type

[**ApiResultWebHookDefinition**](ApiResultWebHookDefinition.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

