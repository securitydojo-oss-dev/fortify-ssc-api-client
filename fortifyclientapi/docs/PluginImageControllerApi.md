# fortifyclientapi.sscclientapi.PluginImageControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                               | HTTP request         | Description |
| -------------------------------------------------------------------- | -------------------- | ----------- |
| [**get_plugin_image**](PluginImageControllerApi.md#get_plugin_image) | **GET** /pluginimage | get         |

# **get_plugin_image**
> str get_plugin_image(image_type, metadata_id)

get

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.PluginImageControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
image_type = 'image_type_example' # str | imageType
metadata_id = 789 # int | metadataId

try:
    # get
    api_response = api_instance.get_plugin_image(image_type, metadata_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PluginImageControllerApi->get_plugin_image: %s\n" % e)
```

### Parameters

| Name            | Type    | Description | Notes |
| --------------- | ------- | ----------- | ----- |
| **image_type**  | **str** | imageType   |
| **metadata_id** | **int** | metadataId  |

### Return type

**str**

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/png

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

