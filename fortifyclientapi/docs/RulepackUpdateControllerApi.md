# fortifyclientapi.sscclientapi.RulepackUpdateControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                      | HTTP request             | Description |
| --------------------------------------------------------------------------- | ------------------------ | ----------- |
| [**do_rulepack_update**](RulepackUpdateControllerApi.md#do_rulepack_update) | **GET** /updateRulepacks | DoImport    |

# **do_rulepack_update**
> ApiResultListRulepacksBatchProcessStatus do_rulepack_update()

DoImport

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.RulepackUpdateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))

try:
    # DoImport
    api_response = api_instance.do_rulepack_update()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RulepackUpdateControllerApi->do_rulepack_update: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiResultListRulepacksBatchProcessStatus**](ApiResultListRulepacksBatchProcessStatus.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

