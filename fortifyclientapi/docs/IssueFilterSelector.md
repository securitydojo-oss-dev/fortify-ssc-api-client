# IssueFilterSelector

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | Selector set&#x27;s description. | 
**display_name** | **str** | Selector set&#x27;s display name. | 
**entity_type** | **str** | Type of the issue attriute that can be used for ordering or filtering. | 
**filter_selector_type** | **str** | If value of the selector can be chose from the list of predefined values, or if it can contain any value defined by user. | 
**guid** | **str** | Selector set&#x27;s GUID. | 
**selector_options** | [**list[SelectorOption]**](SelectorOption.md) | Selector set&#x27;s options. | 
**value** | **str** | Selector set&#x27;s value that must be sent to backend when this option is selected. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

