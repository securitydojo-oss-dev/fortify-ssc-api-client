# fortifyclientapi.sscclientapi.IssueGroupOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                    | HTTP request                                    | Description |
| ------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------- | ----------- |
| [**list_issue_group_of_project_version**](IssueGroupOfProjectVersionControllerApi.md#list_issue_group_of_project_version) | **GET** /projectVersions/{parentId}/issueGroups | list        |

# **list_issue_group_of_project_version**
> ApiResultListProjectVersionIssueGroup list_issue_group_of_project_version(parent_id, start=start, limit=limit, q=q, qm=qm, filterset=filterset, fields=fields, showhidden=showhidden, showremoved=showremoved, showsuppressed=showsuppressed, showshortfilenames=showshortfilenames, filter=filter, groupingtype=groupingtype)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueGroupOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | An issue query expression, must be used together with the 'qm' parameter (optional)
qm = 'qm_example' # str | Syntax mode for the 'q' parameter, mandatory if the 'q' parameter is used (optional)
filterset = 'filterset_example' # str | Filter set to use (optional)
fields = 'fields_example' # str | Output fields (optional)
showhidden = false # bool | If 'true', include hidden issues in search results. If 'false', exclude hidden issues from search results. If no options are set, use application version profile settings to get value of this option. (optional) (default to false)
showremoved = false # bool | If 'true', include removed issues in search results. If 'false', exclude removed issues from search results. If no options are set, use application version profile settings to get value of this option. (optional) (default to false)
showsuppressed = false # bool | If 'true', include suppressed issues in search results. If 'false', exclude suppressed issues from search results. If no options are set, use application version profile settings to get value of this option. (optional) (default to false)
showshortfilenames = false # bool | If 'true', only short file names will be displayed in issues list. (optional) (default to false)
filter = 'filter_example' # str | filter (optional)
groupingtype = 'groupingtype_example' # str | groupingtype (optional)

try:
    # list
    api_response = api_instance.list_issue_group_of_project_version(parent_id, start=start, limit=limit, q=q, qm=qm, filterset=filterset, fields=fields, showhidden=showhidden, showremoved=showremoved, showsuppressed=showsuppressed, showshortfilenames=showshortfilenames, filter=filter, groupingtype=groupingtype)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueGroupOfProjectVersionControllerApi->list_issue_group_of_project_version: %s\n" % e)
```

### Parameters

| Name                   | Type     | Description                                                                                                                                                                                                                         | Notes                         |
| ---------------------- | -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------- |
| **parent_id**          | **int**  | parentId                                                                                                                                                                                                                            |
| **start**              | **int**  | A start offset in object listing                                                                                                                                                                                                    | [optional] [default to 0]     |
| **limit**              | **int**  | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied                                                                                                                             | [optional] [default to 200]   |
| **q**                  | **str**  | An issue query expression, must be used together with the &#x27;qm&#x27; parameter                                                                                                                                                  | [optional]                    |
| **qm**                 | **str**  | Syntax mode for the &#x27;q&#x27; parameter, mandatory if the &#x27;q&#x27; parameter is used                                                                                                                                       | [optional]                    |
| **filterset**          | **str**  | Filter set to use                                                                                                                                                                                                                   | [optional]                    |
| **fields**             | **str**  | Output fields                                                                                                                                                                                                                       | [optional]                    |
| **showhidden**         | **bool** | If &#x27;true&#x27;, include hidden issues in search results. If &#x27;false&#x27;, exclude hidden issues from search results. If no options are set, use application version profile settings to get value of this option.         | [optional] [default to false] |
| **showremoved**        | **bool** | If &#x27;true&#x27;, include removed issues in search results. If &#x27;false&#x27;, exclude removed issues from search results. If no options are set, use application version profile settings to get value of this option.       | [optional] [default to false] |
| **showsuppressed**     | **bool** | If &#x27;true&#x27;, include suppressed issues in search results. If &#x27;false&#x27;, exclude suppressed issues from search results. If no options are set, use application version profile settings to get value of this option. | [optional] [default to false] |
| **showshortfilenames** | **bool** | If &#x27;true&#x27;, only short file names will be displayed in issues list.                                                                                                                                                        | [optional] [default to false] |
| **filter**             | **str**  | filter                                                                                                                                                                                                                              | [optional]                    |
| **groupingtype**       | **str**  | groupingtype                                                                                                                                                                                                                        | [optional]                    |

### Return type

[**ApiResultListProjectVersionIssueGroup**](ApiResultListProjectVersionIssueGroup.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

