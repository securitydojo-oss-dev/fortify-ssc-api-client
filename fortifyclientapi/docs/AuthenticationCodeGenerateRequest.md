# AuthenticationCodeGenerateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code_challenge** | **str** | Code Challenge (base64url_encode(sha256(code_verifier))) | 
**description** | **str** | Purpose for which the token will be requested. | [optional] 
**terminal_date** | **datetime** | Date and time the token expires (in ISO 8601 format). If not specified, it will default to the maximum lifetime for this token type. | [optional] 
**type** | **str** | Token type | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

