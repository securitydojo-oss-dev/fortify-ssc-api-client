# AttributeDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_entity_type** | **str** | Application Entity Type | 
**category** | **str** | Attribute Definition Category | 
**description** | **str** |  | [optional] 
**guid** | **str** | Unique string identifier for this Attribute Definition | 
**has_default** | **bool** | Set to true if this Attribute Definition should be the default | 
**hidden** | **bool** | Set to true if this Attribute Definition is hidden | [optional] 
**id** | **int** | ID required when referencing an existing Attribute Definition | [optional] 
**in_use** | **bool** | True when Attribute Definition is associated with one or more application versions | [optional] 
**is_deletable** | **bool** | True if this Attribute Definition can be deleted with force parameter | [optional] 
**name** | **str** |  | 
**object_version** | **int** |  | [optional] 
**options** | [**list[AttributeOption]**](AttributeOption.md) | List of options (i.e. values) associated with this Attribute Definition | [optional] 
**publish_version** | **int** |  | [optional] 
**required** | **bool** | Set to true if this Attribute Definition is required | 
**sequence_number** | **int** |  | [optional] 
**system_usage** | **str** | System Usage Type | 
**type** | **str** | Attribute Definition Type | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

