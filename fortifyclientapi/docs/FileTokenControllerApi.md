# fortifyclientapi.sscclientapi.FileTokenControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                           | HTTP request           | Description                                          |
| -------------------------------------------------------------------------------- | ---------------------- | ---------------------------------------------------- |
| [**create_file_token**](FileTokenControllerApi.md#create_file_token)             | **POST** /fileTokens   | Create single-use file transfer token                |
| [**multi_delete_file_token**](FileTokenControllerApi.md#multi_delete_file_token) | **DELETE** /fileTokens | Call this operation after every file upload activity |

# **create_file_token**
> ApiResultFileToken create_file_token(body)

Create single-use file transfer token

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.FileTokenControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.FileToken() # FileToken | resource

try:
    # Create single-use file transfer token
    api_response = api_instance.create_file_token(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FileTokenControllerApi->create_file_token: %s\n" % e)
```

### Parameters

| Name     | Type                          | Description | Notes |
| -------- | ----------------------------- | ----------- | ----- |
| **body** | [**FileToken**](FileToken.md) | resource    |

### Return type

[**ApiResultFileToken**](ApiResultFileToken.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_file_token**
> ApiResultVoid multi_delete_file_token()

Call this operation after every file upload activity

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.FileTokenControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))

try:
    # Call this operation after every file upload activity
    api_response = api_instance.multi_delete_file_token()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FileTokenControllerApi->multi_delete_file_token: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

