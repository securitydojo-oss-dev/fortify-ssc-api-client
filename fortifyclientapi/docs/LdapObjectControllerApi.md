# fortifyclientapi.sscclientapi.LdapObjectControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                              | HTTP request                                    | Description                                                                                                                                                                                |
| --------------------------------------------------------------------------------------------------- | ----------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [**create_ldap_object**](LdapObjectControllerApi.md#create_ldap_object)                             | **POST** /ldapObjects                           | create                                                                                                                                                                                     |
| [**delete_ldap_object**](LdapObjectControllerApi.md#delete_ldap_object)                             | **DELETE** /ldapObjects/{id}                    | delete                                                                                                                                                                                     |
| [**do_collection_action_ldap_object**](LdapObjectControllerApi.md#do_collection_action_ldap_object) | **POST** /ldapObjects/action                    | doCollectionAction                                                                                                                                                                         |
| [**list_ldap_object**](LdapObjectControllerApi.md#list_ldap_object)                                 | **GET** /ldapObjects                            | Retrieve ldap entities registered with SSC                                                                                                                                                 |
| [**multi_delete_ldap_object**](LdapObjectControllerApi.md#multi_delete_ldap_object)                 | **DELETE** /ldapObjects                         | multiDelete                                                                                                                                                                                |
| [**read_ldap_object**](LdapObjectControllerApi.md#read_ldap_object)                                 | **GET** /ldapObjects/{id}                       | read                                                                                                                                                                                       |
| [**refresh_ldap_object**](LdapObjectControllerApi.md#refresh_ldap_object)                           | **POST** /ldapObjects/action/refresh            | Refresh the ldap cache                                                                                                                                                                     |
| [**search_unregistered_ldap_object**](LdapObjectControllerApi.md#search_unregistered_ldap_object)   | **POST** /ldapObjects/action/searchUnregistered | Retrieve ldap entities matching the search request which are currently unregistered with SSC. The overall scope of searchable ldap objects is defined by the SSC ldap server configuration |
| [**update_ldap_object**](LdapObjectControllerApi.md#update_ldap_object)                             | **PUT** /ldapObjects/{id}                       | update                                                                                                                                                                                     |

# **create_ldap_object**
> ApiResultLDAPEntity create_ldap_object(body)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapObjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.LDAPEntity() # LDAPEntity | resource

try:
    # create
    api_response = api_instance.create_ldap_object(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapObjectControllerApi->create_ldap_object: %s\n" % e)
```

### Parameters

| Name     | Type                            | Description | Notes |
| -------- | ------------------------------- | ----------- | ----- |
| **body** | [**LDAPEntity**](LDAPEntity.md) | resource    |

### Return type

[**ApiResultLDAPEntity**](ApiResultLDAPEntity.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_ldap_object**
> ApiResultVoid delete_ldap_object(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapObjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_ldap_object(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapObjectControllerApi->delete_ldap_object: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_collection_action_ldap_object**
> ApiResultApiActionResponse do_collection_action_ldap_object(body)

doCollectionAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapObjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiCollectionActionlong() # ApiCollectionActionlong | collectionAction

try:
    # doCollectionAction
    api_response = api_instance.do_collection_action_ldap_object(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapObjectControllerApi->do_collection_action_ldap_object: %s\n" % e)
```

### Parameters

| Name     | Type                                                      | Description      | Notes |
| -------- | --------------------------------------------------------- | ---------------- | ----- |
| **body** | [**ApiCollectionActionlong**](ApiCollectionActionlong.md) | collectionAction |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_ldap_object**
> ApiResultListLDAPEntity list_ldap_object(fields=fields, start=start, limit=limit, q=q, orderby=orderby, ldap_type=ldap_type, viewby=viewby, ldaptype=ldaptype)

Retrieve ldap entities registered with SSC

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapObjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | The specified query string is matched against the name, firstName, lastName, and email fields for LDAP User entities. For Group and OrgUnit entities, only the name is matched. The query string must match the entire field value. For partial matches you can add asterisk ('*') wildcards to your query. (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)
ldap_type = 'ldap_type_example' # str | If specified, restrict the results to ldap entities of the specified type. (optional)
viewby = 'viewby_example' # str | Deprecated. Use the 'ldapType' parameter instead. (optional)
ldaptype = 'ldaptype_example' # str | Deprecated. Use the 'action/searchUnregistered' api instead. (optional)

try:
    # Retrieve ldap entities registered with SSC
    api_response = api_instance.list_ldap_object(fields=fields, start=start, limit=limit, q=q, orderby=orderby, ldap_type=ldap_type, viewby=viewby, ldaptype=ldaptype)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapObjectControllerApi->list_ldap_object: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                                                                                                                                                                                                                                           | Notes                       |
| ------------- | ------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields**    | **str** | Output fields                                                                                                                                                                                                                                                                                                         | [optional]                  |
| **start**     | **int** | A start offset in object listing                                                                                                                                                                                                                                                                                      | [optional] [default to 0]   |
| **limit**     | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied                                                                                                                                                                                                               | [optional] [default to 200] |
| **q**         | **str** | The specified query string is matched against the name, firstName, lastName, and email fields for LDAP User entities. For Group and OrgUnit entities, only the name is matched. The query string must match the entire field value. For partial matches you can add asterisk (&#x27;*&#x27;) wildcards to your query. | [optional]                  |
| **orderby**   | **str** | Fields to order by                                                                                                                                                                                                                                                                                                    | [optional]                  |
| **ldap_type** | **str** | If specified, restrict the results to ldap entities of the specified type.                                                                                                                                                                                                                                            | [optional]                  |
| **viewby**    | **str** | Deprecated. Use the &#x27;ldapType&#x27; parameter instead.                                                                                                                                                                                                                                                           | [optional]                  |
| **ldaptype**  | **str** | Deprecated. Use the &#x27;action/searchUnregistered&#x27; api instead.                                                                                                                                                                                                                                                | [optional]                  |

### Return type

[**ApiResultListLDAPEntity**](ApiResultListLDAPEntity.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_ldap_object**
> ApiResultVoid multi_delete_ldap_object(ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapObjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_ldap_object(ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapObjectControllerApi->multi_delete_ldap_object: %s\n" % e)
```

### Parameters

| Name    | Type    | Description                                    | Notes |
| ------- | ------- | ---------------------------------------------- | ----- |
| **ids** | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_ldap_object**
> ApiResultLDAPEntity read_ldap_object(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapObjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_ldap_object(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapObjectControllerApi->read_ldap_object: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultLDAPEntity**](ApiResultLDAPEntity.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **refresh_ldap_object**
> ApiResultLdapRefreshResponse refresh_ldap_object()

Refresh the ldap cache

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapObjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))

try:
    # Refresh the ldap cache
    api_response = api_instance.refresh_ldap_object()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapObjectControllerApi->refresh_ldap_object: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiResultLdapRefreshResponse**](ApiResultLdapRefreshResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **search_unregistered_ldap_object**
> ApiResultListLDAPEntity search_unregistered_ldap_object(body)

Retrieve ldap entities matching the search request which are currently unregistered with SSC. The overall scope of searchable ldap objects is defined by the SSC ldap server configuration

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapObjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.SearchLdapUnregisteredRequest() # SearchLdapUnregisteredRequest | searchLdapUnregisteredRequest

try:
    # Retrieve ldap entities matching the search request which are currently unregistered with SSC. The overall scope of searchable ldap objects is defined by the SSC ldap server configuration
    api_response = api_instance.search_unregistered_ldap_object(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapObjectControllerApi->search_unregistered_ldap_object: %s\n" % e)
```

### Parameters

| Name     | Type                                                                  | Description                   | Notes |
| -------- | --------------------------------------------------------------------- | ----------------------------- | ----- |
| **body** | [**SearchLdapUnregisteredRequest**](SearchLdapUnregisteredRequest.md) | searchLdapUnregisteredRequest |

### Return type

[**ApiResultListLDAPEntity**](ApiResultListLDAPEntity.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_ldap_object**
> ApiResultLDAPEntity update_ldap_object(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapObjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.LDAPEntity() # LDAPEntity | resource
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_ldap_object(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapObjectControllerApi->update_ldap_object: %s\n" % e)
```

### Parameters

| Name     | Type                            | Description | Notes |
| -------- | ------------------------------- | ----------- | ----- |
| **body** | [**LDAPEntity**](LDAPEntity.md) | resource    |
| **id**   | **int**                         | id          |

### Return type

[**ApiResultLDAPEntity**](ApiResultLDAPEntity.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

