# fortifyclientapi.sscclientapi.PerformanceIndicatorControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                            | HTTP request                           | Description |
| ----------------------------------------------------------------------------------------------------------------- | -------------------------------------- | ----------- |
| [**create_performance_indicator**](PerformanceIndicatorControllerApi.md#create_performance_indicator)             | **POST** /performanceIndicators        | create      |
| [**delete_performance_indicator**](PerformanceIndicatorControllerApi.md#delete_performance_indicator)             | **DELETE** /performanceIndicators/{id} | delete      |
| [**list_performance_indicator**](PerformanceIndicatorControllerApi.md#list_performance_indicator)                 | **GET** /performanceIndicators         | list        |
| [**multi_delete_performance_indicator**](PerformanceIndicatorControllerApi.md#multi_delete_performance_indicator) | **DELETE** /performanceIndicators      | multiDelete |
| [**update_performance_indicator**](PerformanceIndicatorControllerApi.md#update_performance_indicator)             | **PUT** /performanceIndicators/{id}    | update      |

# **create_performance_indicator**
> ApiResultPerformanceIndicator create_performance_indicator(body)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.PerformanceIndicatorControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.PerformanceIndicator() # PerformanceIndicator | data

try:
    # create
    api_response = api_instance.create_performance_indicator(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PerformanceIndicatorControllerApi->create_performance_indicator: %s\n" % e)
```

### Parameters

| Name     | Type                                                | Description | Notes |
| -------- | --------------------------------------------------- | ----------- | ----- |
| **body** | [**PerformanceIndicator**](PerformanceIndicator.md) | data        |

### Return type

[**ApiResultPerformanceIndicator**](ApiResultPerformanceIndicator.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_performance_indicator**
> ApiResultVoid delete_performance_indicator(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.PerformanceIndicatorControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_performance_indicator(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PerformanceIndicatorControllerApi->delete_performance_indicator: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_performance_indicator**
> ApiResultListPerformanceIndicator list_performance_indicator(fields=fields, start=start, limit=limit, q=q, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.PerformanceIndicatorControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_performance_indicator(fields=fields, start=start, limit=limit, q=q, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PerformanceIndicatorControllerApi->list_performance_indicator: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields**  | **str** | Output fields                                                                                           | [optional]                  |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **orderby** | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListPerformanceIndicator**](ApiResultListPerformanceIndicator.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_performance_indicator**
> ApiResultVoid multi_delete_performance_indicator(ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.PerformanceIndicatorControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_performance_indicator(ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PerformanceIndicatorControllerApi->multi_delete_performance_indicator: %s\n" % e)
```

### Parameters

| Name    | Type    | Description                                    | Notes |
| ------- | ------- | ---------------------------------------------- | ----- |
| **ids** | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_performance_indicator**
> ApiResultPerformanceIndicator update_performance_indicator(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.PerformanceIndicatorControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.PerformanceIndicator() # PerformanceIndicator | resource
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_performance_indicator(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PerformanceIndicatorControllerApi->update_performance_indicator: %s\n" % e)
```

### Parameters

| Name     | Type                                                | Description | Notes |
| -------- | --------------------------------------------------- | ----------- | ----- |
| **body** | [**PerformanceIndicator**](PerformanceIndicator.md) | resource    |
| **id**   | **int**                                             | id          |

### Return type

[**ApiResultPerformanceIndicator**](ApiResultPerformanceIndicator.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

