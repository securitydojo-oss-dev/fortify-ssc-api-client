# fortifyclientapi.sscclientapi.ArtifactOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                | HTTP request                                   | Description |
| --------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- | ----------- |
| [**list_artifact_of_project_version**](ArtifactOfProjectVersionControllerApi.md#list_artifact_of_project_version)     | **GET** /projectVersions/{parentId}/artifacts  | list        |
| [**upload_artifact_of_project_version**](ArtifactOfProjectVersionControllerApi.md#upload_artifact_of_project_version) | **POST** /projectVersions/{parentId}/artifacts | upload      |

# **list_artifact_of_project_version**
> ApiResultListArtifact list_artifact_of_project_version(parent_id, fields=fields, start=start, limit=limit, q=q, embed=embed)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ArtifactOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
embed = 'embed_example' # str | Fields to embed (optional)

try:
    # list
    api_response = api_instance.list_artifact_of_project_version(parent_id, fields=fields, start=start, limit=limit, q=q, embed=embed)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtifactOfProjectVersionControllerApi->list_artifact_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                             | Notes                       |
| ------------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **parent_id** | **int** | parentId                                                                                                |
| **fields**    | **str** | Output fields                                                                                           | [optional]                  |
| **start**     | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**     | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**         | **str** | A search query                                                                                          | [optional]                  |
| **embed**     | **str** | Fields to embed                                                                                         | [optional]                  |

### Return type

[**ApiResultListArtifact**](ApiResultListArtifact.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **upload_artifact_of_project_version**
> ApiResultArtifact upload_artifact_of_project_version(file, parent_id, engine_type=engine_type)

upload

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ArtifactOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
file = 'file_example' # str | 
parent_id = 789 # int | parentId
engine_type = 'engine_type_example' # str | engineType (optional)

try:
    # upload
    api_response = api_instance.upload_artifact_of_project_version(file, parent_id, engine_type=engine_type)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtifactOfProjectVersionControllerApi->upload_artifact_of_project_version: %s\n" % e)
```

### Parameters

| Name            | Type    | Description | Notes      |
| --------------- | ------- | ----------- | ---------- |
| **file**        | **str** |             |
| **parent_id**   | **int** | parentId    |
| **engine_type** | **str** | engineType  | [optional] |

### Return type

[**ApiResultArtifact**](ApiResultArtifact.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

