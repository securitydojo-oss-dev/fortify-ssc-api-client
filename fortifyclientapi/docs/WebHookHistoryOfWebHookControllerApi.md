# fortifyclientapi.sscclientapi.WebHookHistoryOfWebHookControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                 | HTTP request                                        | Description                                                       |
| ---------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------- | ----------------------------------------------------------------- |
| [**list_web_hook_history_of_web_hook**](WebHookHistoryOfWebHookControllerApi.md#list_web_hook_history_of_web_hook)     | **GET** /webhooks/{parentId}/history                | list                                                              |
| [**read_web_hook_history_of_web_hook**](WebHookHistoryOfWebHookControllerApi.md#read_web_hook_history_of_web_hook)     | **GET** /webhooks/{parentId}/history/{id}           | read                                                              |
| [**resend_web_hook_history_of_web_hook**](WebHookHistoryOfWebHookControllerApi.md#resend_web_hook_history_of_web_hook) | **POST** /webhooks/{parentId}/history/action/resend | Resend a history item using current webhook&#x27;s configuration. |

# **list_web_hook_history_of_web_hook**
> ApiResultListWebHookHistoryItem list_web_hook_history_of_web_hook(parent_id, start=start, limit=limit)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.WebHookHistoryOfWebHookControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)

try:
    # list
    api_response = api_instance.list_web_hook_history_of_web_hook(parent_id, start=start, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WebHookHistoryOfWebHookControllerApi->list_web_hook_history_of_web_hook: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                             | Notes                       |
| ------------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **parent_id** | **int** | parentId                                                                                                |
| **start**     | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**     | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |

### Return type

[**ApiResultListWebHookHistoryItem**](ApiResultListWebHookHistoryItem.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_web_hook_history_of_web_hook**
> ApiResultWebHookHistory read_web_hook_history_of_web_hook(parent_id, id)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.WebHookHistoryOfWebHookControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
id = 789 # int | id

try:
    # read
    api_response = api_instance.read_web_hook_history_of_web_hook(parent_id, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WebHookHistoryOfWebHookControllerApi->read_web_hook_history_of_web_hook: %s\n" % e)
```

### Parameters

| Name          | Type    | Description | Notes |
| ------------- | ------- | ----------- | ----- |
| **parent_id** | **int** | parentId    |
| **id**        | **int** | id          |

### Return type

[**ApiResultWebHookHistory**](ApiResultWebHookHistory.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **resend_web_hook_history_of_web_hook**
> ApiResultWebHookHistoryItem resend_web_hook_history_of_web_hook(body, parent_id)

Resend a history item using current webhook's configuration.

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.WebHookHistoryOfWebHookControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.WebHookResendRequest() # WebHookResendRequest | resource
parent_id = 789 # int | parentId

try:
    # Resend a history item using current webhook's configuration.
    api_response = api_instance.resend_web_hook_history_of_web_hook(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WebHookHistoryOfWebHookControllerApi->resend_web_hook_history_of_web_hook: %s\n" % e)
```

### Parameters

| Name          | Type                                                | Description | Notes |
| ------------- | --------------------------------------------------- | ----------- | ----- |
| **body**      | [**WebHookResendRequest**](WebHookResendRequest.md) | resource    |
| **parent_id** | **int**                                             | parentId    |

### Return type

[**ApiResultWebHookHistoryItem**](ApiResultWebHookHistoryItem.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

