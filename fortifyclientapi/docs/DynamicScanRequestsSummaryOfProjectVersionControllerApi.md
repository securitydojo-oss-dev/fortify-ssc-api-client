# fortifyclientapi.sscclientapi.DynamicScanRequestsSummaryOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                                                      | HTTP request                                                   | Description |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------- | ----------- |
| [**get_dynamic_scan_requests_summary_of_project_version**](DynamicScanRequestsSummaryOfProjectVersionControllerApi.md#get_dynamic_scan_requests_summary_of_project_version) | **GET** /projectVersions/{parentId}/dynamicScanRequestsSummary | get         |

# **get_dynamic_scan_requests_summary_of_project_version**
> ApiResultDynamicScanRequestsSummary get_dynamic_scan_requests_summary_of_project_version(parent_id)

get

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DynamicScanRequestsSummaryOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId

try:
    # get
    api_response = api_instance.get_dynamic_scan_requests_summary_of_project_version(parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DynamicScanRequestsSummaryOfProjectVersionControllerApi->get_dynamic_scan_requests_summary_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description | Notes |
| ------------- | ------- | ----------- | ----- |
| **parent_id** | **int** | parentId    |

### Return type

[**ApiResultDynamicScanRequestsSummary**](ApiResultDynamicScanRequestsSummary.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

