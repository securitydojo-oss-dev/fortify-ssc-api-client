# ApplicationNameTestResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**found** | **bool** | If application founds | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

