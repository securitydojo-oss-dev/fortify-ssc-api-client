# fortifyclientapi.sscclientapi.JobPriorityChangeCategoryWarningOfJobControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                                         | HTTP request                      | Description |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------- | ----------- |
| [**list_job_priority_change_category_warning_of_job**](JobPriorityChangeCategoryWarningOfJobControllerApi.md#list_job_priority_change_category_warning_of_job) | **GET** /jobs/{parentId}/warnings | list        |

# **list_job_priority_change_category_warning_of_job**
> ApiResultListJobPriorityChangeCategoryWarning list_job_priority_change_category_warning_of_job(parent_id, priority, fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.JobPriorityChangeCategoryWarningOfJobControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 'parent_id_example' # str | parentId
priority = 'priority_example' # str | priority
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_job_priority_change_category_warning_of_job(parent_id, priority, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling JobPriorityChangeCategoryWarningOfJobControllerApi->list_job_priority_change_category_warning_of_job: %s\n" % e)
```

### Parameters

| Name          | Type    | Description   | Notes      |
| ------------- | ------- | ------------- | ---------- |
| **parent_id** | **str** | parentId      |
| **priority**  | **str** | priority      |
| **fields**    | **str** | Output fields | [optional] |

### Return type

[**ApiResultListJobPriorityChangeCategoryWarning**](ApiResultListJobPriorityChangeCategoryWarning.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

