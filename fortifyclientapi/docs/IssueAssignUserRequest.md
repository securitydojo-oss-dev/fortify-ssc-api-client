# IssueAssignUserRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issues** | [**list[EntityStateIdentifier]**](EntityStateIdentifier.md) | Issues to audit | 
**user** | **str** | Username to assign | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

