# fortifyclientapi.sscclientapi.ProjectVersionOfProjectControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                             | HTTP request                           | Description |
| ------------------------------------------------------------------------------------------------------------------ | -------------------------------------- | ----------- |
| [**create_project_version_of_project**](ProjectVersionOfProjectControllerApi.md#create_project_version_of_project) | **POST** /projects/{parentId}/versions | create      |
| [**list_project_version_of_project**](ProjectVersionOfProjectControllerApi.md#list_project_version_of_project)     | **GET** /projects/{parentId}/versions  | list        |

# **create_project_version_of_project**
> ApiResultProjectVersion create_project_version_of_project(body, parent_id)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionOfProjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ProjectVersion() # ProjectVersion | resource
parent_id = 789 # int | parentId

try:
    # create
    api_response = api_instance.create_project_version_of_project(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionOfProjectControllerApi->create_project_version_of_project: %s\n" % e)
```

### Parameters

| Name          | Type                                    | Description | Notes |
| ------------- | --------------------------------------- | ----------- | ----- |
| **body**      | [**ProjectVersion**](ProjectVersion.md) | resource    |
| **parent_id** | **int**                                 | parentId    |

### Return type

[**ApiResultProjectVersion**](ApiResultProjectVersion.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_project_version_of_project**
> ApiResultListProjectVersion list_project_version_of_project(parent_id, fields=fields, start=start, limit=limit, q=q, fulltextsearch=fulltextsearch, orderby=orderby, include_inactive=include_inactive, my_assigned_issues=my_assigned_issues)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionOfProjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search-spec of full text search query (see fulltextsearch parameter) (optional)
fulltextsearch = false # bool | If 'true', interpret 'q' parameter as full text search query, defaults to 'false' (optional) (default to false)
orderby = 'orderby_example' # str | Fields to order by (optional)
include_inactive = false # bool | includeInactive (optional) (default to false)
my_assigned_issues = false # bool | myAssignedIssues (optional) (default to false)

try:
    # list
    api_response = api_instance.list_project_version_of_project(parent_id, fields=fields, start=start, limit=limit, q=q, fulltextsearch=fulltextsearch, orderby=orderby, include_inactive=include_inactive, my_assigned_issues=my_assigned_issues)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionOfProjectControllerApi->list_project_version_of_project: %s\n" % e)
```

### Parameters

| Name                   | Type     | Description                                                                                                     | Notes                         |
| ---------------------- | -------- | --------------------------------------------------------------------------------------------------------------- | ----------------------------- |
| **parent_id**          | **int**  | parentId                                                                                                        |
| **fields**             | **str**  | Output fields                                                                                                   | [optional]                    |
| **start**              | **int**  | A start offset in object listing                                                                                | [optional] [default to 0]     |
| **limit**              | **int**  | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied         | [optional] [default to 200]   |
| **q**                  | **str**  | A search-spec of full text search query (see fulltextsearch parameter)                                          | [optional]                    |
| **fulltextsearch**     | **bool** | If &#x27;true&#x27;, interpret &#x27;q&#x27; parameter as full text search query, defaults to &#x27;false&#x27; | [optional] [default to false] |
| **orderby**            | **str**  | Fields to order by                                                                                              | [optional]                    |
| **include_inactive**   | **bool** | includeInactive                                                                                                 | [optional] [default to false] |
| **my_assigned_issues** | **bool** | myAssignedIssues                                                                                                | [optional] [default to false] |

### Return type

[**ApiResultListProjectVersion**](ApiResultListProjectVersion.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

