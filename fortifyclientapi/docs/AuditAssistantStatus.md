# AuditAssistantStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fpr_file_path** | **str** |  | [optional] 
**message** | **str** |  | [optional] 
**project_version_id** | **int** |  | [optional] 
**server_id** | **int** |  | [optional] 
**server_status** | **int** |  | [optional] 
**status** | **str** |  | [optional] 
**user_name** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

