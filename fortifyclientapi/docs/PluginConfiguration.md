# PluginConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parameter_name** | **str** |  | [optional] 
**parameter_type** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

