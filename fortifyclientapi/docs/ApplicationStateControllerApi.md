# fortifyclientapi.sscclientapi.ApplicationStateControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                              | HTTP request              | Description |
| ----------------------------------------------------------------------------------- | ------------------------- | ----------- |
| [**get_application_state**](ApplicationStateControllerApi.md#get_application_state) | **GET** /applicationState | get         |
| [**put_application_state**](ApplicationStateControllerApi.md#put_application_state) | **PUT** /applicationState | put         |

# **get_application_state**
> ApiResultApplicationState get_application_state()

get

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ApplicationStateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))

try:
    # get
    api_response = api_instance.get_application_state()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApplicationStateControllerApi->get_application_state: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiResultApplicationState**](ApiResultApplicationState.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **put_application_state**
> ApiResultApplicationState put_application_state(body)

put

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ApplicationStateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApplicationState() # ApplicationState | applicationState

try:
    # put
    api_response = api_instance.put_application_state(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApplicationStateControllerApi->put_application_state: %s\n" % e)
```

### Parameters

| Name     | Type                                        | Description      | Notes |
| -------- | ------------------------------------------- | ---------------- | ----- |
| **body** | [**ApplicationState**](ApplicationState.md) | applicationState |

### Return type

[**ApiResultApplicationState**](ApiResultApplicationState.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

