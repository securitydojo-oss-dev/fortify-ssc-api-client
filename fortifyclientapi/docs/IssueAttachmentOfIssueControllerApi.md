# fortifyclientapi.sscclientapi.IssueAttachmentOfIssueControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                      | HTTP request                                   | Description |
| --------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- | ----------- |
| [**delete_issue_attachment_of_issue**](IssueAttachmentOfIssueControllerApi.md#delete_issue_attachment_of_issue)             | **DELETE** /issues/{parentId}/attachments/{id} | delete      |
| [**list_issue_attachment_of_issue**](IssueAttachmentOfIssueControllerApi.md#list_issue_attachment_of_issue)                 | **GET** /issues/{parentId}/attachments         | list        |
| [**multi_delete_issue_attachment_of_issue**](IssueAttachmentOfIssueControllerApi.md#multi_delete_issue_attachment_of_issue) | **DELETE** /issues/{parentId}/attachments      | multiDelete |
| [**read_issue_attachment_of_issue**](IssueAttachmentOfIssueControllerApi.md#read_issue_attachment_of_issue)                 | **GET** /issues/{parentId}/attachments/{id}    | read        |
| [**update_issue_attachment_of_issue**](IssueAttachmentOfIssueControllerApi.md#update_issue_attachment_of_issue)             | **PUT** /issues/{parentId}/attachments/{id}    | update      |
| [**upload_issue_attachment_of_issue**](IssueAttachmentOfIssueControllerApi.md#upload_issue_attachment_of_issue)             | **POST** /issues/{parentId}/attachments        | upload      |

# **delete_issue_attachment_of_issue**
> ApiResultVoid delete_issue_attachment_of_issue(parent_id, id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueAttachmentOfIssueControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_issue_attachment_of_issue(parent_id, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueAttachmentOfIssueControllerApi->delete_issue_attachment_of_issue: %s\n" % e)
```

### Parameters

| Name          | Type    | Description | Notes |
| ------------- | ------- | ----------- | ----- |
| **parent_id** | **int** | parentId    |
| **id**        | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_issue_attachment_of_issue**
> ApiResultListIssueAttachment list_issue_attachment_of_issue(parent_id, start=start, limit=limit)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueAttachmentOfIssueControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)

try:
    # list
    api_response = api_instance.list_issue_attachment_of_issue(parent_id, start=start, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueAttachmentOfIssueControllerApi->list_issue_attachment_of_issue: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                             | Notes                       |
| ------------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **parent_id** | **int** | parentId                                                                                                |
| **start**     | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**     | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |

### Return type

[**ApiResultListIssueAttachment**](ApiResultListIssueAttachment.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_issue_attachment_of_issue**
> ApiResultVoid multi_delete_issue_attachment_of_issue(parent_id, ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueAttachmentOfIssueControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_issue_attachment_of_issue(parent_id, ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueAttachmentOfIssueControllerApi->multi_delete_issue_attachment_of_issue: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                    | Notes |
| ------------- | ------- | ---------------------------------------------- | ----- |
| **parent_id** | **int** | parentId                                       |
| **ids**       | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_issue_attachment_of_issue**
> ApiResultIssueAttachment read_issue_attachment_of_issue(parent_id, id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueAttachmentOfIssueControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_issue_attachment_of_issue(parent_id, id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueAttachmentOfIssueControllerApi->read_issue_attachment_of_issue: %s\n" % e)
```

### Parameters

| Name          | Type    | Description   | Notes      |
| ------------- | ------- | ------------- | ---------- |
| **parent_id** | **int** | parentId      |
| **id**        | **int** | id            |
| **fields**    | **str** | Output fields | [optional] |

### Return type

[**ApiResultIssueAttachment**](ApiResultIssueAttachment.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_issue_attachment_of_issue**
> ApiResultIssueAttachment update_issue_attachment_of_issue(body, parent_id, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueAttachmentOfIssueControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.IssueAttachment() # IssueAttachment | data
parent_id = 789 # int | parentId
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_issue_attachment_of_issue(body, parent_id, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueAttachmentOfIssueControllerApi->update_issue_attachment_of_issue: %s\n" % e)
```

### Parameters

| Name          | Type                                      | Description | Notes |
| ------------- | ----------------------------------------- | ----------- | ----- |
| **body**      | [**IssueAttachment**](IssueAttachment.md) | data        |
| **parent_id** | **int**                                   | parentId    |
| **id**        | **int**                                   | id          |

### Return type

[**ApiResultIssueAttachment**](ApiResultIssueAttachment.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **upload_issue_attachment_of_issue**
> ApiResultIssueAttachment upload_issue_attachment_of_issue(file, parent_id, description=description)

upload

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueAttachmentOfIssueControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
file = 'file_example' # str | 
parent_id = 789 # int | parentId
description = 'description_example' # str | description (optional)

try:
    # upload
    api_response = api_instance.upload_issue_attachment_of_issue(file, parent_id, description=description)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueAttachmentOfIssueControllerApi->upload_issue_attachment_of_issue: %s\n" % e)
```

### Parameters

| Name            | Type    | Description | Notes      |
| --------------- | ------- | ----------- | ---------- |
| **file**        | **str** |             |
| **parent_id**   | **int** | parentId    |
| **description** | **str** | description | [optional] |

### Return type

[**ApiResultIssueAttachment**](ApiResultIssueAttachment.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

