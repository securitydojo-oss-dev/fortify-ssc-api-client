# fortifyclientapi.sscclientapi.SystemConfigurationControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                         | HTTP request                        | Description |
| ---------------------------------------------------------------------------------------------- | ----------------------------------- | ----------- |
| [**list_system_configuration**](SystemConfigurationControllerApi.md#list_system_configuration) | **GET** /systemConfiguration        | list        |
| [**read_system_configuration**](SystemConfigurationControllerApi.md#read_system_configuration) | **GET** /systemConfiguration/{name} | read        |

# **list_system_configuration**
> ApiResultListSystemConfiguration list_system_configuration(fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.SystemConfigurationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_system_configuration(fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SystemConfigurationControllerApi->list_system_configuration: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultListSystemConfiguration**](ApiResultListSystemConfiguration.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_system_configuration**
> ApiResultSystemConfiguration read_system_configuration(name, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.SystemConfigurationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
name = 'name_example' # str | name
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_system_configuration(name, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SystemConfigurationControllerApi->read_system_configuration: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **name**   | **str** | name          |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultSystemConfiguration**](ApiResultSystemConfiguration.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

