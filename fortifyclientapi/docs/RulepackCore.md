# RulepackCore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | [optional] 
**id** | **int** |  | 
**language** | **str** |  | 
**locale** | **str** |  | [optional] 
**name** | **str** |  | 
**rulepack_guid** | **str** |  | 
**rulepack_type** | **str** |  | 
**sku** | **str** |  | 
**version** | **str** |  | 
**versions** | [**list[RulepackCore]**](RulepackCore.md) |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

