# LocalUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**admin_password** | **str** |  | [optional] 
**clear_password** | **str** |  | [optional] 
**date_frozen** | **datetime** | Date user&#x27;s account was frozen | [optional] 
**email** | **str** | User&#x27;s email address | [optional] 
**externally_managed** | **bool** | If user is managed by an external service | [optional] 
**failed_login_attempts** | **int** | Number of failed login attempts | 
**first_name** | **str** | User&#x27;s first name | [optional] 
**id** | **int** | ID required when referencing an existing Local User | [optional] 
**last_name** | **str** | User&#x27;s last name | [optional] 
**object_version** | **int** | LocalUser version stored on the server. This value is used to prevent concurrent updates of LocalUsers. | [optional] 
**password_never_expire** | **bool** | True if user&#x27;s password never expires | [optional] 
**require_password_change** | **bool** | Is user required to change password on first login? | [optional] 
**roles** | [**list[Role]**](Role.md) | List of Roles assigned to user | [optional] 
**suspended** | **bool** | True if user&#x27;s account is locked | [optional] 
**user_name** | **str** | User&#x27;s username | 
**user_type** | **str** | User type | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

