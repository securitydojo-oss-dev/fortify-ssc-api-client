# Artifact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**embed** | [**EmbeddedScans**](EmbeddedScans.md) |  | [optional] 
**allow_approve** | **bool** |  | [optional] 
**allow_delete** | **bool** |  | [optional] 
**allow_purge** | **bool** |  | [optional] 
**approval_comment** | **str** |  | [optional] 
**approval_date** | **datetime** |  | [optional] 
**approval_username** | **str** |  | [optional] 
**artifact_type** | **str** |  | [optional] 
**audit_updated** | **bool** |  | [optional] 
**file_name** | **str** |  | [optional] 
**file_size** | **int** |  | [optional] 
**file_url** | **str** |  | [optional] 
**id** | **int** | Artifact id | [optional] 
**in_modifying_status** | **bool** |  | [optional] 
**indexed** | **bool** |  | [optional] 
**last_scan_date** | **datetime** |  | [optional] 
**message_count** | **int** |  | [optional] 
**messages** | **str** |  | [optional] 
**original_file_name** | **str** |  | [optional] 
**other_status** | **str** |  | [optional] 
**purged** | **bool** |  | [optional] 
**runtime_status** | **str** |  | [optional] 
**sca_status** | **str** |  | [optional] 
**scan_errors_count** | **int** |  | [optional] 
**status** | **str** |  | [optional] 
**upload_date** | **datetime** |  | [optional] 
**upload_ip** | **str** |  | [optional] 
**user_name** | **str** |  | [optional] 
**version_number** | **int** |  | [optional] 
**web_inspect_status** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

