# ExportIssueStatsToCSVRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dataset_name** | **str** | Dataset name (Issue Stat) | 
**file_name** | **str** | Csv file name | 
**limit** | **int** | Limit | [optional] 
**note** | **str** | Note for csv export | [optional] 
**start** | **int** | Start | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

