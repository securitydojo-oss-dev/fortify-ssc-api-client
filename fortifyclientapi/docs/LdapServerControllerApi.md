# fortifyclientapi.sscclientapi.LdapServerControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                              | HTTP request                      | Description                                                                                                      |
| --------------------------------------------------------------------------------------------------- | --------------------------------- | ---------------------------------------------------------------------------------------------------------------- |
| [**create_ldap_server**](LdapServerControllerApi.md#create_ldap_server)                             | **POST** /ldapServers             | create                                                                                                           |
| [**delete_ldap_server**](LdapServerControllerApi.md#delete_ldap_server)                             | **DELETE** /ldapServers/{id}      | delete                                                                                                           |
| [**do_collection_action_ldap_server**](LdapServerControllerApi.md#do_collection_action_ldap_server) | **POST** /ldapServers/action      | doCollectionAction                                                                                               |
| [**list_ldap_server**](LdapServerControllerApi.md#list_ldap_server)                                 | **GET** /ldapServers              | list                                                                                                             |
| [**multi_delete_ldap_server**](LdapServerControllerApi.md#multi_delete_ldap_server)                 | **DELETE** /ldapServers           | multiDelete                                                                                                      |
| [**read_ldap_server**](LdapServerControllerApi.md#read_ldap_server)                                 | **GET** /ldapServers/{id}         | read                                                                                                             |
| [**test_ldap_server**](LdapServerControllerApi.md#test_ldap_server)                                 | **POST** /ldapServers/action/test | Validate that it is possible to authenticate to the ldap server using the configured ldap baseDN and credentials |
| [**update_ldap_server**](LdapServerControllerApi.md#update_ldap_server)                             | **PUT** /ldapServers/{id}         | update                                                                                                           |

# **create_ldap_server**
> ApiResultLdapServerDto create_ldap_server(body)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapServerControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.LdapServerDto() # LdapServerDto | resource

try:
    # create
    api_response = api_instance.create_ldap_server(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapServerControllerApi->create_ldap_server: %s\n" % e)
```

### Parameters

| Name     | Type                                  | Description | Notes |
| -------- | ------------------------------------- | ----------- | ----- |
| **body** | [**LdapServerDto**](LdapServerDto.md) | resource    |

### Return type

[**ApiResultLdapServerDto**](ApiResultLdapServerDto.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_ldap_server**
> ApiResultVoid delete_ldap_server(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapServerControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_ldap_server(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapServerControllerApi->delete_ldap_server: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_collection_action_ldap_server**
> ApiResultValidationStatus do_collection_action_ldap_server(body)

doCollectionAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapServerControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiCollectionActionlong() # ApiCollectionActionlong | collectionAction

try:
    # doCollectionAction
    api_response = api_instance.do_collection_action_ldap_server(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapServerControllerApi->do_collection_action_ldap_server: %s\n" % e)
```

### Parameters

| Name     | Type                                                      | Description      | Notes |
| -------- | --------------------------------------------------------- | ---------------- | ----- |
| **body** | [**ApiCollectionActionlong**](ApiCollectionActionlong.md) | collectionAction |

### Return type

[**ApiResultValidationStatus**](ApiResultValidationStatus.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_ldap_server**
> ApiResultListLdapServerDto list_ldap_server(fields=fields, start=start, limit=limit)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapServerControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)

try:
    # list
    api_response = api_instance.list_ldap_server(fields=fields, start=start, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapServerControllerApi->list_ldap_server: %s\n" % e)
```

### Parameters

| Name       | Type    | Description                                                                                             | Notes                       |
| ---------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields** | **str** | Output fields                                                                                           | [optional]                  |
| **start**  | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**  | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |

### Return type

[**ApiResultListLdapServerDto**](ApiResultListLdapServerDto.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_ldap_server**
> ApiResultVoid multi_delete_ldap_server(ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapServerControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_ldap_server(ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapServerControllerApi->multi_delete_ldap_server: %s\n" % e)
```

### Parameters

| Name    | Type    | Description                                    | Notes |
| ------- | ------- | ---------------------------------------------- | ----- |
| **ids** | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_ldap_server**
> ApiResultLdapServerDto read_ldap_server(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapServerControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_ldap_server(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapServerControllerApi->read_ldap_server: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultLdapServerDto**](ApiResultLdapServerDto.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **test_ldap_server**
> ApiResultValidationStatus test_ldap_server(body)

Validate that it is possible to authenticate to the ldap server using the configured ldap baseDN and credentials

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapServerControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.LdapServerTestRequest() # LdapServerTestRequest | ldapServerTestRequest

try:
    # Validate that it is possible to authenticate to the ldap server using the configured ldap baseDN and credentials
    api_response = api_instance.test_ldap_server(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapServerControllerApi->test_ldap_server: %s\n" % e)
```

### Parameters

| Name     | Type                                                  | Description           | Notes |
| -------- | ----------------------------------------------------- | --------------------- | ----- |
| **body** | [**LdapServerTestRequest**](LdapServerTestRequest.md) | ldapServerTestRequest |

### Return type

[**ApiResultValidationStatus**](ApiResultValidationStatus.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_ldap_server**
> ApiResultLdapServerDto update_ldap_server(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LdapServerControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.LdapServerDto() # LdapServerDto | resource
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_ldap_server(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LdapServerControllerApi->update_ldap_server: %s\n" % e)
```

### Parameters

| Name     | Type                                  | Description | Notes |
| -------- | ------------------------------------- | ----------- | ----- |
| **body** | [**LdapServerDto**](LdapServerDto.md) | resource    |
| **id**   | **int**                               | id          |

### Return type

[**ApiResultLdapServerDto**](ApiResultLdapServerDto.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

