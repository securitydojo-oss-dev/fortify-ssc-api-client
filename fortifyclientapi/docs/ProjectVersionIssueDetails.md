# ProjectVersionIssueDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accuracy** | **float** | Vulnerability accuracy | [optional] 
**analyzer** | **str** | Analyzer | 
**app_sec_training_url** | **str** | A link to access application security training for this issue (available only if the SSC administrator has enabled it) | [optional] 
**assigned_user** | [**AssignedUser**](AssignedUser.md) |  | 
**attack_payload** | **str** | Attack payload | 
**attack_type** | **str** | Attack type | 
**audited** | **bool** | Attribute is set to true if issue is audited (primary tag values is set for this issue) | 
**brief** | **str** | Issue brief | 
**class_name** | **str** | Name of class where the vulnerability has been found | [optional] 
**confidence** | **float** | Issue confidence | 
**cookie** | **str** | Cookie | 
**custom_attributes** | **object** | All the additional custom attributes defined for the issue by parser plugin | [optional] 
**custom_tag_values** | [**list[CustomTag]**](CustomTag.md) | Custom tag values | 
**data_version** | **int** | Version of the issue data. This attribute is initialized only for issues parsed by 3rd party parsers and is not set for the issues parsed by standard parsers included in SSC installation. | [optional] 
**detail** | **str** | Issue detail | 
**display_engine_type** | **str** | Display engine type | 
**download_request** | **bool** | Request contains binary or large data | 
**download_response** | **bool** | Response contains binary or large data\&quot; | 
**engine_category** | **str** | Engine category | 
**engine_type** | **str** | Engine type | 
**found_date** | **datetime** | Issue found date | 
**friority** | **str** | Fortify priority order (Friority) | 
**full_file_name** | **str** | Full file name where issue found | 
**function_name** | **str** | Name of function located in the file where the vulnerability has been found | [optional] 
**has_view_template** | **bool** | Flag that indicates if there is a view template that should be used to display issue details on UI. | [optional] 
**hidden** | **bool** | Set to true if issue is hidden | 
**id** | **int** | Identifier | [optional] 
**impact** | **float** | Issue impact | 
**issue_instance_id** | **str** | Issue instance identifier | 
**issue_name** | **str** | Name of the issue category | 
**issue_state** | **str** | Flag represents issue state and says if issue is not an issue or open issue. Rule to calculate value of thi flag is defined in issue template. | 
**issue_status** | **str** | Flag represents issue review status | [optional] 
**kingdom** | **str** | Issue kingdom | 
**last_scan_id** | **int** | ID of the latest scan that found the issue | [optional] 
**likelihood** | **float** | Issue likelihood | 
**line_number** | **int** | Line number in the file where the vulnerability has been found | [optional] 
**mapped_category** | **str** | Name of the Fortify category of the vulnerability | [optional] 
**method** | **str** | Method where issue found | 
**min_virtual_call_confidence** | **float** | Confidence level which estimates that vulnerability found in virtual (overridden) function will be executed by tainted source | [optional] 
**package_name** | **str** | Name of package where the vulnerability has been found | [optional] 
**primary_rule_guid** | **str** | Primary rule global unique identifier | 
**primary_tag** | [**IssuePrimaryTag**](IssuePrimaryTag.md) |  | [optional] 
**probability** | **float** | Vulnerability probability | [optional] 
**project_version_id** | **int** | Application version identifier | 
**recommendation** | **str** | Issue recommendation | 
**references** | **str** | References | 
**remediation_constant** | **float** | Level of complexity to fix this vulnerability | [optional] 
**removed_date** | **datetime** | Issue removed date | 
**request_body** | **str** | Request body | 
**request_header** | **str** | Request header | 
**request_parameter** | **str** | Request parameter | 
**request_triggers** | [**list[Trigger]**](Trigger.md) | Triggers in the request | [optional] 
**response** | **str** | Response | 
**response_header** | **str** | Response header | 
**response_triggers** | [**list[Trigger]**](Trigger.md) | Triggers in the response | [optional] 
**revision** | **int** | Revision number | 
**scan_status** | **str** | Scan status | 
**severity** | **float** | Issue severity | 
**short_file_name** | **str** | Short file name where issue found | 
**sink** | **str** | Taint sink name | [optional] 
**sink_context** | **str** | Name of the context that contains vulnerability sink | [optional] 
**source** | **str** | Name of the a program point where tainted data enter the program | [optional] 
**source_context** | **str** | Name of the context that contains vulnerability sources | [optional] 
**source_file** | **str** | File name where vulnerability source is located. | [optional] 
**source_line** | **int** | Line number in the source file where vulnerability source is located | [optional] 
**suppressed** | **bool** | Set to true if issue is suppressed | 
**taint_flag** | **str** | An attribute of tainted data that enables the data flow analyzer to discriminate between different types of taint | [optional] 
**tips** | **str** | Issue tips | 
**trace_nodes** | **list[list[TraceNodeDto]]** | Issue trace nodes | 
**trigger_string** | **str** | Trigger string | 
**url** | **str** | Issue url | 
**vulnerable_parameter** | **str** | Vulnerable parameter | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

