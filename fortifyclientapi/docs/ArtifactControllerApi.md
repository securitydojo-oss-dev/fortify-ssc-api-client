# fortifyclientapi.sscclientapi.ArtifactControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                | HTTP request                       | Description                                                                                                                                                                                              |
| --------------------------------------------------------------------- | ---------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [**approve_artifact**](ArtifactControllerApi.md#approve_artifact)     | **POST** /artifacts/action/approve | Approve the artifact for processing in spite of failing                                                                                                                                                  |
| [**delete_artifact**](ArtifactControllerApi.md#delete_artifact)       | **DELETE** /artifacts/{id}         | delete                                                                                                                                                                                                   |
| [**do_action_artifact**](ArtifactControllerApi.md#do_action_artifact) | **POST** /artifacts/{id}/action    | doAction                                                                                                                                                                                                 |
| [**purge_artifact**](ArtifactControllerApi.md#purge_artifact)         | **POST** /artifacts/action/purge   | Purge the specified artifact from the system to recover space without affecting issue metrics. (Use the &#x27;delete&#x27; operation instead if you want to completely revert all traces of an artifact) |
| [**read_artifact**](ArtifactControllerApi.md#read_artifact)           | **GET** /artifacts/{id}            | read                                                                                                                                                                                                     |

# **approve_artifact**
> ApiResultVoid approve_artifact(body)

Approve the artifact for processing in spite of failing 

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ArtifactControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ArtifactApproveRequest() # ArtifactApproveRequest | resource

try:
    # Approve the artifact for processing in spite of failing 
    api_response = api_instance.approve_artifact(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtifactControllerApi->approve_artifact: %s\n" % e)
```

### Parameters

| Name     | Type                                                    | Description | Notes |
| -------- | ------------------------------------------------------- | ----------- | ----- |
| **body** | [**ArtifactApproveRequest**](ArtifactApproveRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_artifact**
> ApiResultVoid delete_artifact(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ArtifactControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_artifact(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtifactControllerApi->delete_artifact: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_action_artifact**
> ApiResultApiActionResponse do_action_artifact(body, id)

doAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ArtifactControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiResourceAction() # ApiResourceAction | resourceAction
id = 789 # int | id

try:
    # doAction
    api_response = api_instance.do_action_artifact(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtifactControllerApi->do_action_artifact: %s\n" % e)
```

### Parameters

| Name     | Type                                          | Description    | Notes |
| -------- | --------------------------------------------- | -------------- | ----- |
| **body** | [**ApiResourceAction**](ApiResourceAction.md) | resourceAction |
| **id**   | **int**                                       | id             |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **purge_artifact**
> ApiResultVoid purge_artifact(body)

Purge the specified artifact from the system to recover space without affecting issue metrics. (Use the 'delete' operation instead if you want to completely revert all traces of an artifact)

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ArtifactControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ArtifactPurgeRequest() # ArtifactPurgeRequest | resource

try:
    # Purge the specified artifact from the system to recover space without affecting issue metrics. (Use the 'delete' operation instead if you want to completely revert all traces of an artifact)
    api_response = api_instance.purge_artifact(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtifactControllerApi->purge_artifact: %s\n" % e)
```

### Parameters

| Name     | Type                                                | Description | Notes |
| -------- | --------------------------------------------------- | ----------- | ----- |
| **body** | [**ArtifactPurgeRequest**](ArtifactPurgeRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_artifact**
> ApiResultArtifact read_artifact(id, fields=fields, embed=embed)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ArtifactControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)
embed = 'embed_example' # str | Fields to embed (optional)

try:
    # read
    api_response = api_instance.read_artifact(id, fields=fields, embed=embed)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ArtifactControllerApi->read_artifact: %s\n" % e)
```

### Parameters

| Name       | Type    | Description     | Notes      |
| ---------- | ------- | --------------- | ---------- |
| **id**     | **int** | id              |
| **fields** | **str** | Output fields   | [optional] |
| **embed**  | **str** | Fields to embed | [optional] |

### Return type

[**ApiResultArtifact**](ApiResultArtifact.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

