# ProjectVersionBugTracker

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assigned_bugtracker_short_name_if_known** | **str** | the short display name of the bug tracker. (will be null if no bugtracker is assigned or if there is no currently installed plugin matching the assigned pluginId.) | 
**assigned_plugin_id** | **str** | identifier of the bug tracker plugin assigned to the application version. (Bug tracker integration will be active only if the plugin is also enabled in the system.) | 
**bug_state_management_config** | [**BugStateManagementCfg**](BugStateManagementCfg.md) |  | 
**bug_tracker** | [**BugTracker**](BugTracker.md) |  | 
**bugtracker_is_assigned** | **bool** | indicates whether a bug tracker is assigned to the application version | 
**clear_app_version_bugs** | **bool** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

