# fortifyclientapi.sscclientapi.CloudJobControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                  | HTTP request                          | Description  |
| ----------------------------------------------------------------------- | ------------------------------------- | ------------ |
| [**cancel_cloud_job**](CloudJobControllerApi.md#cancel_cloud_job)       | **POST** /cloudjobs/action/cancel     | Cancel a job |
| [**do_action_cloud_job**](CloudJobControllerApi.md#do_action_cloud_job) | **POST** /cloudjobs/{jobToken}/action | doAction     |
| [**list_cloud_job**](CloudJobControllerApi.md#list_cloud_job)           | **GET** /cloudjobs                    | list         |
| [**read_cloud_job**](CloudJobControllerApi.md#read_cloud_job)           | **GET** /cloudjobs/{jobToken}         | read         |

# **cancel_cloud_job**
> ApiResultVoid cancel_cloud_job(body)

Cancel a job

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudJobControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.CloudJobCancelRequest() # CloudJobCancelRequest | resource

try:
    # Cancel a job
    api_response = api_instance.cancel_cloud_job(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudJobControllerApi->cancel_cloud_job: %s\n" % e)
```

### Parameters

| Name     | Type                                                  | Description | Notes |
| -------- | ----------------------------------------------------- | ----------- | ----- |
| **body** | [**CloudJobCancelRequest**](CloudJobCancelRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_action_cloud_job**
> ApiResultApiActionResponse do_action_cloud_job(body, job_token)

doAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudJobControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiResourceAction() # ApiResourceAction | resourceAction
job_token = 'job_token_example' # str | jobToken

try:
    # doAction
    api_response = api_instance.do_action_cloud_job(body, job_token)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudJobControllerApi->do_action_cloud_job: %s\n" % e)
```

### Parameters

| Name          | Type                                          | Description    | Notes |
| ------------- | --------------------------------------------- | -------------- | ----- |
| **body**      | [**ApiResourceAction**](ApiResourceAction.md) | resourceAction |
| **job_token** | **str**                                       | jobToken       |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_cloud_job**
> ApiResultListCloudJob list_cloud_job(fields=fields, start=start, limit=limit, q=q, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudJobControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_cloud_job(fields=fields, start=start, limit=limit, q=q, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudJobControllerApi->list_cloud_job: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields**  | **str** | Output fields                                                                                           | [optional]                  |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **orderby** | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListCloudJob**](ApiResultListCloudJob.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_cloud_job**
> ApiResultCloudJob read_cloud_job(job_token, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudJobControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
job_token = 'job_token_example' # str | jobToken
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_cloud_job(job_token, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudJobControllerApi->read_cloud_job: %s\n" % e)
```

### Parameters

| Name          | Type    | Description   | Notes      |
| ------------- | ------- | ------------- | ---------- |
| **job_token** | **str** | jobToken      |
| **fields**    | **str** | Output fields | [optional] |

### Return type

[**ApiResultCloudJob**](ApiResultCloudJob.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

