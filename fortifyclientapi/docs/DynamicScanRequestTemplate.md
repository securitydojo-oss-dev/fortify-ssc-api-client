# DynamicScanRequestTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parameters** | [**list[DynamicScanRequestParameter]**](DynamicScanRequestParameter.md) | Parameters that are needed for dynamic scan request | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

