# Fortify Client API

## SSC Client CLI

### Usage
```bash
python -m fortifyclientapi.cli.main [options] [command] [arguments]
```

### Get the command help messages
```bash
# Get the global CLI help
python -m fortifyclientapi.cli.main -h

# Get the command help
python -m fortifyclientapi.cli.main command -h

# Get the subcommand help
python -m fortifyclientapi.cli.main command subcommand -h
```

## Commands & SubCommands
|   Command    | Subcommand |                                              Description                                              |
| :----------: | :--------: | :---------------------------------------------------------------------------------------------------: |
| `appversion` |     -      |   Create/Update/Copy/GetIssues Project Version: command line interface for the Project Version API Endpoints    |
| `appversion` |  `create`  |                 Create: command line interface for the Project Version API Endpoints                  |
| `appversion` |  `update`  |                 Update: command line interface for the Project Version API Endpoints                  |
| `appversion` |   `copy`   |                  Copy: command line interface for the Project Version API Endpoints                   |
| `appversion` |`getIssues` |                  GetIssues: command line interface for the Project Version API Endpoints                   |
| `attribute`  |     -      | Create/Update Attribute definition: command line interface for the Attribute definition API Endpoints |
| `attribute`  |  `create`  |               Create command line interface for the Attribute definition API Endpoints                |
| `attribute`  |  `update`  |               Update command line interface for the Attribute definition API Endpoints                |


## Arguments
### Global Options
|              Options              |      Type      |                                                    Description                                                     |                     Required                      |     Default     |
| :-------------------------------: | :------------: | :----------------------------------------------------------------------------------------------------------------: | :-----------------------------------------------: | :-------------: |
|         `-v`, `--version`         |      None      |                                                  Get the version                                                   |                       False                       |                 |
|        `-u`, `--username`         |     String     |                                       Specify the username to connect to SSC                                       |           True if the Token is not set            |       ""        |
|           `--password`            |     String     |                                       Specify the password to connect to SSC                                       |           True if the Token is not set            |       ""        |
|   `-t`, `--token`, `--api-key`    |     String     |                                   Specify the token/api key of the user account                                    | True if the Username and the Password are not set |       ""        |
|       `-s`, `--server-url`        |     String     |                                      Specify the SSC base URL or SSC API URL                                       |                       True                        |       ""        |
| `--trustcerts`, `--no-ssl-verify` |    Boolean     |                                        If set, disable the SSL verification                                        |                       False                       |      False      |
|  `--cacert`, `--ca-certificate`   | String as Path | Specify the path to the application certificate or the CA certificate which has signed the application certificate |                       False                       |       ""        |
|          `--client-cert`          | String as Path |                                     Specify the path to the client certificate                                     |                       False                       |       ""        |
|          `--client-key`           | String as Path |                                   Specify the path to the client certificate key                                   |                       False                       |       ""        |
|  `@deprecated --hostname-verify`  |    Boolean     |                                          Enable SSL hostname verification                                          |                       False                       |      False      |
|             `--debug`             |    Boolean     |                                            Enable the Debug level mode                                             |                       False                       |      False      |
|            `--logfile`            | String as Path |                                          Specify the path to the logfile                                           |                       False                       | "./ssc-cli.log" |


### `appversion create` Options
|                 Options                 |  Type   |                                                 Description                                                  | Required |                       Default                       |
| :-------------------------------------: | :-----: | :----------------------------------------------------------------------------------------------------------: | :------: | :-------------------------------------------------: |
|        `--app`, `--application`         | String  |                                         Specify the application name                                         |   True   |                        None                         |
| `--appversion`, `--application-version` | String  |                                       Specify the application version                                        |   True   |                        None                         |
|             `--deactivate`             | Boolean |                                       Disable the application version                                        |  False   |                        False                        |
|            `--auto-predict`             | Boolean |                                 Enable the auto prediction - AuditAssistant                                  |  False   |                        False                        |
|        `--bug-tracker-plugin-id`        | String  |                                          The Bug Tracker Plugin Id                                           |  False   |                         ""                          |
|       `--bug-tracker-plugin-name`       | String  |                                         The Bug Tracker Plugin name                                          |  False   |                         ""                          |
|         `--enable-bug-tracker`          | Boolean |                                      Enable the given Bug Track Plugin                                       |  False   |                        False                        |
|             `--created-by`              | String  |                                 Username of the application version creator                                  |  False   | leave empty to set username who generate the API Key |
| `--enable-custom-tag-values-auto-apply` | Boolean |                             Enable Custom Tag Values Auto Apply - AuditAssistant                             |  False   |                        False                        |
|             `--description`             | String  |                                     The application version description                                      |  False   |            "REST API - FortifyClientAPI"            |
|          `--issue-template-id`          | String  |                          The issue template Id attached to the application version                           |  False   |                        None                         |
|         `--issue-template-name`         | String  |                         The issue template name attached to the application version                          |  False   |       "Prioritized High Risk Issue Template"        |
|           `--custom-tag-guid`           | String  |                           The primary custom tag GUID for the application version                            |  False   |                        None                         |
|           `--custom-tag-name`           | String  |                           The primary custom tag name for the application version                            |  False   |                     "Analysis"                      |
|           `-a`, `--attribute`           | String  |             Set the attributes of the application version as myAttr=myValue<br>Repeatable option             |  False   |                        None                         |
|        `-r`, `--processing-rule`        | String  | Set the Result Processing Rules of the application version as myProcessRule=true/falses<br>Repeatable option |  False   |                        None                         |

### `appversion update` Options
|                 Options                 |  Type   |                                                 Description                                                  |                         Required                         |                       Default                       |
| :-------------------------------------: | :-----: | :----------------------------------------------------------------------------------------------------------: | :------------------------------------------------------: | :-------------------------------------------------: |
|       `--id`, `--application-id`        | Integer |                                       Specify the application id name                                        | False if `application` and `application-version` are set |                        None                         |
|        `--app`, `--application`         | String  |                                         Specify the application name                                         |                           True                           |                        None                         |
| `--appversion`, `--application-version` | String  |                                       Specify the application version                                        |                           True                           |                        None                         |
|             `--auto-create`             | Boolean |                        Automatically create the application version if does not exist                        |                          False                           |                        False                        |
|             `--deactivate`             | Boolean |                       Disable the application version. Enable the version if not present                      |                          False                           |                        False                        |
|            `--auto-predict`             | Boolean |                                 Enable the auto prediction - AuditAssistant                                  |                          False                           |                        False                        |
|        `--bug-tracker-plugin-id`        | String  |                                          The Bug Tracker Plugin Id                                           |                          False                           |                         ""                          |
|       `--bug-tracker-plugin-name`       | String  |                                         The Bug Tracker Plugin name                                          |                          False                           |                         ""                          |
|         `--enable-bug-tracker`          | Boolean |                                      Enable the given Bug Track Plugin                                       |                          False                           |                        False                        |
|             `--created-by`              | String  |                                 Username of the application version creator                                  |                          False                           | leave empty to set username who genarte the API Key |
| `--enable-custom-tag-values-auto-apply` | Boolean |                             Enable Custom Tag Values Auto Apply - AuditAssistant                             |                          False                           |                        False                        |
|             `--description`             | String  |                                     The application version description                                      |                          False                           |            "REST API - FortifyClientAPI"            |
|          `--issue-template-id`          | String  |                          The issue template Id attached to the application version                           |                          False                           |                        None                         |
|         `--issue-template-name`         | String  |                         The issue template name attached to the application version                          |                          False                           |       "Prioritized High Risk Issue Template"        |
|           `--custom-tag-guid`           | String  |                           The primary custom tag GUID for the application version                            |                          False                           |                        None                         |
|           `--custom-tag-name`           | String  |                           The primary custom tag name for the application version                            |                          False                           |                     "Analysis"                      |
|           `-a`, `--attribute`           | String  |             Set the attributes of the application version as myAttr=myValue<br>Repeatable option             |                          False                           |                        None                         |
|        `-r`, `--processing-rule`        | String  | Set the Result Processing Rules of the application version as myProcessRule=true/falses<br>Repeatable option |                          False                           |                        None                         |

### `appversion copy` Options
|                      Options                       |  Type   |                                                      Description                                                       |                                Required                                | Default |
| :------------------------------------------------: | :-----: | :--------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------: | :-----: |
|       `--src-id`, `--source-application-id`        | Integer |                                         Specify the source application id name                                         | False if `source-application` and `source-application-version` are set |  None   |
|        `--src-app`, `--source-application`         | String  |                                          Specify the source application name                                           |                                  True                                  |  None   |
| `--src-appversion`, `--source-application-version` | String  |                                         Specify the source application version                                         |                                  True                                  |  None   |
|        `--dst-id`, `--dest-application-id`         | Integer |                                      Specify the destination application id name                                       |   False if `dest-application` and `dest-application-version` are set   |  None   |
|         `--dst-app`, `--dest-application`          | String  |                                        Specify the destination application name                                        |                                  True                                  |  None   |
|  `--dst-appversion`, `--dest-application-version`  | String  |                                      Specify the destination application version                                       |                                  True                                  |  None   |
|         `--copy-attributes`         | Boolean |                           Copy the Project Version Attributes to the new application version                            |                                 False                                  |  False  |
|         `--copy-user-access`         | Boolean |                           Copy the User Access Settings to the new application version                            |                                 False                                  |  False  |
|         `--copy-analysis-processing-rules`         | Boolean |                           Copy the Analysis Processing Rules to the new application version                            |                                 False                                  |  False  |
|         `--copy-bug-tracker-configuration`         | Boolean |                           Copy the Bug Tracker Configuration to the new application version                            |                                 False                                  |  False  |
|                `--copy-custom-tags`                | Boolean |                                  Copy the Custom Tags to the new application version                                   |                                 False                                  |  False  |
|               `--copy-current-state`               | Boolean |          Copy the Current State - Copy all issues from the source application version to the destination ones          |                                 False                                  |  False  |

### `appversion getIssues` Options
|                 Options                 |  Type   |                                                 Description                                                  | Required |                       Default                       |
| :-------------------------------------: | :-----: | :----------------------------------------------------------------------------------------------------------: | :------: | :-------------------------------------------------: |
|        `--app`, `--application`         | String  |                                         Specify the application name                                         |   True   |                        None                         |
| `--appversion`, `--application-version` | String  |                                       Specify the application version                                        |   True   |                        None                         |
|             `--show-hidden`             | Boolean |                                    Include hidden issues in search results                                   |  False   |                        False                        |
|            `--show-removed`             | Boolean |                                    Include removed issues in search results                                  |  False   |                        False                        |
|           `--show-suppressed`           | Boolean |                                   Include suppresed issues in search results                                 |  False   |                        False                        |
|                `--query`                | String  |                                         The Bug Tracker Plugin name                                          |  False   |                         ""                          |

### `attribute create` Options
|                        Options                         |                  Type                  |                                                                                     Description                                                                                     |                         Required                         |         Default          |
| :----------------------------------------------------: | :------------------------------------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------: | :----------------------: |
|                  `-f`, `--from-file`                   |             String as Path             |           Specify the filepath of the data file<br>The file is a JSON-based format containing a list of attribute definition ([Attribute Definition from JSON](#annexes))           |                                                          |          False           | None |
|                     `-n`, `--name`                     |                 String                 |                                                                          Specify the name of the attribute                                                                          |                           True                           |           None           |
|                         `--id`                         |                Integer                 |                                                                           Specify the id of the attribute                                                                           |                          False                           |           None           |
|                        `--type`                        |                 String                 |                                            Specify the type of values for the attribute from "TEXT", "SINGLE", "MULTIPLE", "LONG_TEXT", "SENSITIVE_TEXT", "BOOLEAN", "INTEGER", "DATE", "FILE" |                  True                  |                                                                                        None                                                                                         |
|                  `--app-entity-type`                   |                 String                 |                                                       Specify the entity type of the attribute from "PROJECT_VERSION", "NONE"                                                       |                          False                           |    "PROJECT_VERSION"     |
|                      `--category`                      |                 String                 |                                     Specify the category of the attribute from "TECHNICAL", "BUSINESS", "DYNAMIC_SCAN_REQUEST", "ORGANIZATION"                                      |                          False                           |       "TECHNICAL"        |
|                    `--description`                     |                 String                 |                                                                      Specify the description of the attribute                                                                       |                          False                           |            ""            |
|                        `--guid`                        |                 String                 |                                                                          Specify the guid of the attribute                                                                          |                          False                           |            ""            |
|                       `--hidden`                       |                Boolean                 |                                                                         Specify if the attribute is hidden                                                                          |                          False                           |          False           |
|                      `--required`                      |                Boolean                 |                                                                        Specify if the attribute is required                                                                         |                          False                           |          False           |
|                    `--system-usage`                    |                 String                 |              Specify the systemUsage of the attribute from "HP_DEFINED_DELETABLE", "HP_DEFINED_NON_DELETABLE", "USER_DEFINED_DELETABLE", "USER_DEFINED_NON_DELETABLE"               |                          False                           | "USER_DEFINED_DELETABLE" |
|                       `--option`                       | Tuple(String, String, String, Boolean) | Specify the options for the attribute<br>Repeatable switch<br>usage: `--option \<name\> [description                               \| ''] [guid \|      '']   [hidden       \| 'false']` | True (minimally one) if `type` is "SINGLE" or "MULTIPLE" |           None           |

### `attribute update` Options
|                        Options                         |                  Type                  |                                                                                              Description                                                                                               |                         Required                         |         Default          |
| :----------------------------------------------------: | :------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------: | :----------------------: |
|                  `-f`, `--from-file`                   |             String as Path             |                    Specify the filepath of the data file<br>The file is a JSON-based format containing a list of attribute definition ([Attribute Definition from JSON](#annexes))                     |                                                          |          False           | None |
|                     `-n`, `--name`                     |                 String                 |                                                                                   Specify the name of the attribute                                                                                    |                           True                           |           None           |
|                         `--id`                         |                Integer                 |                                                                                    Specify the id of the attribute                                                                                     |                          False                           |           None           |
|                    `--auto-create`                     |                Boolean                 |                                                                    Automatically create the attribute definition if does not exist                                                                     |                          False                           |          False           |
|                        `--type`                        |                 String                 |                                                      Specify the type of values for the attribute from "TEXT", "SINGLE", "MULTIPLE", "LONG_TEXT", "SENSITIVE_TEXT", "BOOLEAN", "INTEGER", "DATE", "FILE" |                  True                  |                                                                                                  None                                                                                                  |
|                  `--app-entity-type`                   |                 String                 |                                                                Specify the entity type of the attribute from "PROJECT_VERSION", "NONE"                                                                 |                          False                           |    "PROJECT_VERSION"     |
|                      `--category`                      |                 String                 |                                               Specify the category of the attribute from "TECHNICAL", "BUSINESS", "DYNAMIC_SCAN_REQUEST", "ORGANIZATION"                                               |                          False                           |       "TECHNICAL"        |
|                    `--description`                     |                 String                 |                                                                                Specify the description of the attribute                                                                                |                          False                           |            ""            |
|                        `--guid`                        |                 String                 |                                                                                   Specify the guid of the attribute                                                                                    |                          False                           |            ""            |
|                       `--hidden`                       |                Boolean                 |                                                                                   Specify if the attribute is hidden                                                                                   |                          False                           |          False           |
|                      `--required`                      |                Boolean                 |                                                                                  Specify if the attribute is required                                                                                  |                          False                           |          False           |
|                    `--system-usage`                    |                 String                 |                        Specify the systemUsage of the attribute from "HP_DEFINED_DELETABLE", "HP_DEFINED_NON_DELETABLE", "USER_DEFINED_DELETABLE", "USER_DEFINED_NON_DELETABLE"                        |                          False                           | "USER_DEFINED_DELETABLE" |
|                       `--option`                       | Tuple(String, String, String, Boolean) |        Specify the all options for the attribute<br>Repeatable switch<br>usage: `--option \<name\> [description                               \| ''] [guid \|      '']   [hidden       \| 'false']`         | True (minimally one) if `type` is "SINGLE" or "MULTIPLE" |           None           |
|                     `--add-option`                     | Tuple(String, String, String, Boolean) | Specify the options for adding to the existing attribute<br>Repeatable switch<br>usage: `--option \<name\> [description                               \| ''] [guid \|      '']   [hidden       \| 'false']` | True (minimally one) if `type` is "SINGLE" or "MULTIPLE" |           None           |



## Annexes

### Define attribute definition from file
```json
    [
        {
            "app_entity_type": "PROJECT_VERSION",
            "category": "TECHNICAL",
            "description": "string",
            "guid": "string",
            "has_default": false,
            "hidden": false,
            "name": "string",
            "options": [
                {
                "description": "string",
                "guid": "string",
                "hidden": false,
                "name": "string"
                },
                ...
            ],
            "required": false,
            "system_usage": "USER_DEFINED_DELETABLE",
            "type": "SINGLE"
        }, 
        ...
    ] 
```
