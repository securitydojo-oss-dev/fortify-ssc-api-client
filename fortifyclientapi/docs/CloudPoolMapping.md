# CloudPoolMapping

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cloud_pool** | [**CloudPool**](CloudPool.md) |  | [optional] 
**project_version_id** | **int** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

