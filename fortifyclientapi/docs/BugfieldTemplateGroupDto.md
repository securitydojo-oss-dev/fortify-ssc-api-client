# BugfieldTemplateGroupDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bug_tracker_plugin_id** | **str** | Identifier of the bug tracker plugin associated with this template group. | 
**deletable** | **bool** | Flag that says if bugfield template group can be deleted. | 
**description** | **str** | description for bugfield template group. | [optional] 
**id** | **int** | unique identifier of bugfield template group. | [optional] 
**name** | **str** | short name of associated bug tracker plugin. (May be null if plugin is not currently enabled.) | 
**object_version** | **int** | version of bugfield template group stored on the server. This value is used for optimistic locking to prevent concurrent modification by different users at the same time. | 
**value_list** | [**list[BugfieldTemplateDto]**](BugfieldTemplateDto.md) | Collection of all templates belonging to this bugfield template group. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

