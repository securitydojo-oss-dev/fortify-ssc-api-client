# IssueSuppressRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issues** | [**list[EntityStateIdentifier]**](EntityStateIdentifier.md) | Issues to suppress | 
**suppressed** | **bool** | Will suppress the issue | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

