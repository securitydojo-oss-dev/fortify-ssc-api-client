# FolderDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color** | **str** | Folder color. Folder will be marked by this color on UI | [optional] 
**guid** | **str** | Folder GUID. | [optional] 
**id** | **int** | Unique identifier of the folder. | [optional] 
**name** | **str** | Folder unique name. | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

