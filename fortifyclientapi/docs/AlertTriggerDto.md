# AlertTriggerDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alert_definition_id** | **int** |  | [optional] 
**monitored_attribute** | **str** |  | [optional] 
**reset_after_triggering** | **bool** |  | [optional] 
**triggered_value** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

