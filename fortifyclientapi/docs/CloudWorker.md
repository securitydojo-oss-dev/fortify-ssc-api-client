# CloudWorker

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**available_processors** | **int** |  | 
**cloud_pool** | [**CloudPool**](CloudPool.md) |  | 
**host_name** | **str** |  | 
**ip_address** | **str** |  | 
**last_activity** | **str** |  | 
**last_seen** | **datetime** |  | 
**os_architecture** | **str** |  | 
**os_name** | **str** |  | 
**os_version** | **str** |  | 
**process_uuid** | **str** |  | 
**sca_version** | **str** |  | 
**state** | **str** |  | 
**total_physical_memory** | **int** |  | 
**uuid** | **str** |  | 
**vm_name** | **str** |  | 
**worker_expiry_time** | **datetime** |  | 
**worker_start_time** | **datetime** |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

