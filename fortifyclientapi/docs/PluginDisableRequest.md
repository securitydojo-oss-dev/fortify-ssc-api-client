# PluginDisableRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**plugin_ids** | **list[int]** | List containing single plugin ID to disable | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

