# Resource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | [optional] 
**file** | [**File**](File.md) |  | [optional] 
**filename** | **str** |  | [optional] 
**input_stream** | [**InputStream**](InputStream.md) |  | [optional] 
**open** | **bool** |  | [optional] 
**readable** | **bool** |  | [optional] 
**uri** | [**URI**](URI.md) |  | [optional] 
**url** | [**URL**](URL.md) |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

