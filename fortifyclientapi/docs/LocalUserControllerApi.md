# fortifyclientapi.sscclientapi.LocalUserControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                           | HTTP request                                      | Description                                                                                             |
| -------------------------------------------------------------------------------- | ------------------------------------------------- | ------------------------------------------------------------------------------------------------------- |
| [**check_local_user**](LocalUserControllerApi.md#check_local_user)               | **POST** /localUsers/action/checkPasswordStrength | Check the provided password using the system password strength validator and the specified dictionaries |
| [**create_local_user**](LocalUserControllerApi.md#create_local_user)             | **POST** /localUsers                              | create                                                                                                  |
| [**delete_local_user**](LocalUserControllerApi.md#delete_local_user)             | **DELETE** /localUsers/{id}                       | delete                                                                                                  |
| [**list_local_user**](LocalUserControllerApi.md#list_local_user)                 | **GET** /localUsers                               | list                                                                                                    |
| [**multi_delete_local_user**](LocalUserControllerApi.md#multi_delete_local_user) | **DELETE** /localUsers                            | multiDelete                                                                                             |
| [**read_local_user**](LocalUserControllerApi.md#read_local_user)                 | **GET** /localUsers/{id}                          | read                                                                                                    |
| [**update_local_user**](LocalUserControllerApi.md#update_local_user)             | **PUT** /localUsers/{id}                          | update                                                                                                  |

# **check_local_user**
> ApiResultPasswordStrengthCheckResponse check_local_user(body)

Check the provided password using the system password strength validator and the specified dictionaries

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LocalUserControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.PasswordStrengthCheckRequest() # PasswordStrengthCheckRequest | passwordStrengthCheckRequest

try:
    # Check the provided password using the system password strength validator and the specified dictionaries
    api_response = api_instance.check_local_user(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LocalUserControllerApi->check_local_user: %s\n" % e)
```

### Parameters

| Name     | Type                                                                | Description                  | Notes |
| -------- | ------------------------------------------------------------------- | ---------------------------- | ----- |
| **body** | [**PasswordStrengthCheckRequest**](PasswordStrengthCheckRequest.md) | passwordStrengthCheckRequest |

### Return type

[**ApiResultPasswordStrengthCheckResponse**](ApiResultPasswordStrengthCheckResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **create_local_user**
> ApiResultLocalUser create_local_user(body)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LocalUserControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.LocalUser() # LocalUser | user

try:
    # create
    api_response = api_instance.create_local_user(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LocalUserControllerApi->create_local_user: %s\n" % e)
```

### Parameters

| Name     | Type                          | Description | Notes |
| -------- | ----------------------------- | ----------- | ----- |
| **body** | [**LocalUser**](LocalUser.md) | user        |

### Return type

[**ApiResultLocalUser**](ApiResultLocalUser.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_local_user**
> ApiResultVoid delete_local_user(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LocalUserControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_local_user(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LocalUserControllerApi->delete_local_user: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_local_user**
> ApiResultListLocalUser list_local_user(start=start, limit=limit, q=q, orderby=orderby, fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LocalUserControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_local_user(start=start, limit=limit, q=q, orderby=orderby, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LocalUserControllerApi->list_local_user: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **orderby** | **str** | Fields to order by                                                                                      | [optional]                  |
| **fields**  | **str** | Output fields                                                                                           | [optional]                  |

### Return type

[**ApiResultListLocalUser**](ApiResultListLocalUser.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_local_user**
> ApiResultVoid multi_delete_local_user(ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LocalUserControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_local_user(ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LocalUserControllerApi->multi_delete_local_user: %s\n" % e)
```

### Parameters

| Name    | Type    | Description                                    | Notes |
| ------- | ------- | ---------------------------------------------- | ----- |
| **ids** | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_local_user**
> ApiResultLocalUser read_local_user(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LocalUserControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_local_user(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LocalUserControllerApi->read_local_user: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultLocalUser**](ApiResultLocalUser.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_local_user**
> ApiResultLocalUser update_local_user(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.LocalUserControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.LocalUser() # LocalUser | user
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_local_user(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LocalUserControllerApi->update_local_user: %s\n" % e)
```

### Parameters

| Name     | Type                          | Description | Notes |
| -------- | ----------------------------- | ----------- | ----- |
| **body** | [**LocalUser**](LocalUser.md) | user        |
| **id**   | **int**                       | id          |

### Return type

[**ApiResultLocalUser**](ApiResultLocalUser.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

