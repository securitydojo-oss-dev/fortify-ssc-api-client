# ApiBulkResponseItemResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | [**ApiBulkResponseData**](ApiBulkResponseData.md) |  | [optional] 
**headers** | **dict(str, str)** |  | [optional] 
**request_url** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

