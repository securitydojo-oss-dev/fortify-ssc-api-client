# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class BugFilingRequirementsResponse(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'bug_filing_requirements': 'BugFilingRequirements'
    }

    attribute_map = {
        'bug_filing_requirements': 'bugFilingRequirements'
    }

    def __init__(self, bug_filing_requirements=None):  # noqa: E501
        """BugFilingRequirementsResponse - a model defined in Swagger"""  # noqa: E501
        self._bug_filing_requirements = None
        self.discriminator = None
        if bug_filing_requirements is not None:
            self.bug_filing_requirements = bug_filing_requirements

    @property
    def bug_filing_requirements(self):
        """Gets the bug_filing_requirements of this BugFilingRequirementsResponse.  # noqa: E501


        :return: The bug_filing_requirements of this BugFilingRequirementsResponse.  # noqa: E501
        :rtype: BugFilingRequirements
        """
        return self._bug_filing_requirements

    @bug_filing_requirements.setter
    def bug_filing_requirements(self, bug_filing_requirements):
        """Sets the bug_filing_requirements of this BugFilingRequirementsResponse.


        :param bug_filing_requirements: The bug_filing_requirements of this BugFilingRequirementsResponse.  # noqa: E501
        :type: BugFilingRequirements
        """

        self._bug_filing_requirements = bug_filing_requirements

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(BugFilingRequirementsResponse, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, BugFilingRequirementsResponse):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
