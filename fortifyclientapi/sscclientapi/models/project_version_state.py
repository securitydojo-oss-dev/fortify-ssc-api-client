# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six


class ProjectVersionState(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'analysis_results_exist': 'bool',
        'analysis_upload_enabled': 'bool',
        'attention_required': 'bool',
        'audit_enabled': 'bool',
        'batch_bug_submission_exists': 'bool',
        'committed': 'bool',
        'critical_priority_issue_count_delta': 'int',
        'delta_period': 'int',
        'extra_message': 'str',
        'has_custom_issues': 'bool',
        'id': 'int',
        'issue_count_delta': 'int',
        'last_fpr_upload_date': 'datetime',
        'metric_evaluation_date': 'datetime',
        'percent_audited_delta': 'float',
        'percent_critical_priority_issues_audited_delta': 'float'
    }

    attribute_map = {
        'analysis_results_exist': 'analysisResultsExist',
        'analysis_upload_enabled': 'analysisUploadEnabled',
        'attention_required': 'attentionRequired',
        'audit_enabled': 'auditEnabled',
        'batch_bug_submission_exists': 'batchBugSubmissionExists',
        'committed': 'committed',
        'critical_priority_issue_count_delta': 'criticalPriorityIssueCountDelta',
        'delta_period': 'deltaPeriod',
        'extra_message': 'extraMessage',
        'has_custom_issues': 'hasCustomIssues',
        'id': 'id',
        'issue_count_delta': 'issueCountDelta',
        'last_fpr_upload_date': 'lastFprUploadDate',
        'metric_evaluation_date': 'metricEvaluationDate',
        'percent_audited_delta': 'percentAuditedDelta',
        'percent_critical_priority_issues_audited_delta': 'percentCriticalPriorityIssuesAuditedDelta'
    }

    def __init__(self, analysis_results_exist=None, analysis_upload_enabled=None, attention_required=None, audit_enabled=None, batch_bug_submission_exists=None, committed=None, critical_priority_issue_count_delta=None, delta_period=None, extra_message=None, has_custom_issues=None, id=None, issue_count_delta=None, last_fpr_upload_date=None, metric_evaluation_date=None, percent_audited_delta=None, percent_critical_priority_issues_audited_delta=None):  # noqa: E501
        """ProjectVersionState - a model defined in Swagger"""  # noqa: E501
        self._analysis_results_exist = None
        self._analysis_upload_enabled = None
        self._attention_required = None
        self._audit_enabled = None
        self._batch_bug_submission_exists = None
        self._committed = None
        self._critical_priority_issue_count_delta = None
        self._delta_period = None
        self._extra_message = None
        self._has_custom_issues = None
        self._id = None
        self._issue_count_delta = None
        self._last_fpr_upload_date = None
        self._metric_evaluation_date = None
        self._percent_audited_delta = None
        self._percent_critical_priority_issues_audited_delta = None
        self.discriminator = None
        self.analysis_results_exist = analysis_results_exist
        self.analysis_upload_enabled = analysis_upload_enabled
        self.attention_required = attention_required
        self.audit_enabled = audit_enabled
        self.batch_bug_submission_exists = batch_bug_submission_exists
        self.committed = committed
        self.critical_priority_issue_count_delta = critical_priority_issue_count_delta
        self.delta_period = delta_period
        if extra_message is not None:
            self.extra_message = extra_message
        self.has_custom_issues = has_custom_issues
        self.id = id
        self.issue_count_delta = issue_count_delta
        if last_fpr_upload_date is not None:
            self.last_fpr_upload_date = last_fpr_upload_date
        if metric_evaluation_date is not None:
            self.metric_evaluation_date = metric_evaluation_date
        if percent_audited_delta is not None:
            self.percent_audited_delta = percent_audited_delta
        if percent_critical_priority_issues_audited_delta is not None:
            self.percent_critical_priority_issues_audited_delta = percent_critical_priority_issues_audited_delta

    @property
    def analysis_results_exist(self):
        """Gets the analysis_results_exist of this ProjectVersionState.  # noqa: E501


        :return: The analysis_results_exist of this ProjectVersionState.  # noqa: E501
        :rtype: bool
        """
        return self._analysis_results_exist

    @analysis_results_exist.setter
    def analysis_results_exist(self, analysis_results_exist):
        """Sets the analysis_results_exist of this ProjectVersionState.


        :param analysis_results_exist: The analysis_results_exist of this ProjectVersionState.  # noqa: E501
        :type: bool
        """
        if analysis_results_exist is None:
            raise ValueError("Invalid value for `analysis_results_exist`, must not be `None`")  # noqa: E501

        self._analysis_results_exist = analysis_results_exist

    @property
    def analysis_upload_enabled(self):
        """Gets the analysis_upload_enabled of this ProjectVersionState.  # noqa: E501


        :return: The analysis_upload_enabled of this ProjectVersionState.  # noqa: E501
        :rtype: bool
        """
        return self._analysis_upload_enabled

    @analysis_upload_enabled.setter
    def analysis_upload_enabled(self, analysis_upload_enabled):
        """Sets the analysis_upload_enabled of this ProjectVersionState.


        :param analysis_upload_enabled: The analysis_upload_enabled of this ProjectVersionState.  # noqa: E501
        :type: bool
        """
        if analysis_upload_enabled is None:
            raise ValueError("Invalid value for `analysis_upload_enabled`, must not be `None`")  # noqa: E501

        self._analysis_upload_enabled = analysis_upload_enabled

    @property
    def attention_required(self):
        """Gets the attention_required of this ProjectVersionState.  # noqa: E501


        :return: The attention_required of this ProjectVersionState.  # noqa: E501
        :rtype: bool
        """
        return self._attention_required

    @attention_required.setter
    def attention_required(self, attention_required):
        """Sets the attention_required of this ProjectVersionState.


        :param attention_required: The attention_required of this ProjectVersionState.  # noqa: E501
        :type: bool
        """
        if attention_required is None:
            raise ValueError("Invalid value for `attention_required`, must not be `None`")  # noqa: E501

        self._attention_required = attention_required

    @property
    def audit_enabled(self):
        """Gets the audit_enabled of this ProjectVersionState.  # noqa: E501


        :return: The audit_enabled of this ProjectVersionState.  # noqa: E501
        :rtype: bool
        """
        return self._audit_enabled

    @audit_enabled.setter
    def audit_enabled(self, audit_enabled):
        """Sets the audit_enabled of this ProjectVersionState.


        :param audit_enabled: The audit_enabled of this ProjectVersionState.  # noqa: E501
        :type: bool
        """
        if audit_enabled is None:
            raise ValueError("Invalid value for `audit_enabled`, must not be `None`")  # noqa: E501

        self._audit_enabled = audit_enabled

    @property
    def batch_bug_submission_exists(self):
        """Gets the batch_bug_submission_exists of this ProjectVersionState.  # noqa: E501


        :return: The batch_bug_submission_exists of this ProjectVersionState.  # noqa: E501
        :rtype: bool
        """
        return self._batch_bug_submission_exists

    @batch_bug_submission_exists.setter
    def batch_bug_submission_exists(self, batch_bug_submission_exists):
        """Sets the batch_bug_submission_exists of this ProjectVersionState.


        :param batch_bug_submission_exists: The batch_bug_submission_exists of this ProjectVersionState.  # noqa: E501
        :type: bool
        """
        if batch_bug_submission_exists is None:
            raise ValueError("Invalid value for `batch_bug_submission_exists`, must not be `None`")  # noqa: E501

        self._batch_bug_submission_exists = batch_bug_submission_exists

    @property
    def committed(self):
        """Gets the committed of this ProjectVersionState.  # noqa: E501

        False if application version is in an incomplete state  # noqa: E501

        :return: The committed of this ProjectVersionState.  # noqa: E501
        :rtype: bool
        """
        return self._committed

    @committed.setter
    def committed(self, committed):
        """Sets the committed of this ProjectVersionState.

        False if application version is in an incomplete state  # noqa: E501

        :param committed: The committed of this ProjectVersionState.  # noqa: E501
        :type: bool
        """
        if committed is None:
            raise ValueError("Invalid value for `committed`, must not be `None`")  # noqa: E501

        self._committed = committed

    @property
    def critical_priority_issue_count_delta(self):
        """Gets the critical_priority_issue_count_delta of this ProjectVersionState.  # noqa: E501


        :return: The critical_priority_issue_count_delta of this ProjectVersionState.  # noqa: E501
        :rtype: int
        """
        return self._critical_priority_issue_count_delta

    @critical_priority_issue_count_delta.setter
    def critical_priority_issue_count_delta(self, critical_priority_issue_count_delta):
        """Sets the critical_priority_issue_count_delta of this ProjectVersionState.


        :param critical_priority_issue_count_delta: The critical_priority_issue_count_delta of this ProjectVersionState.  # noqa: E501
        :type: int
        """
        if critical_priority_issue_count_delta is None:
            raise ValueError("Invalid value for `critical_priority_issue_count_delta`, must not be `None`")  # noqa: E501

        self._critical_priority_issue_count_delta = critical_priority_issue_count_delta

    @property
    def delta_period(self):
        """Gets the delta_period of this ProjectVersionState.  # noqa: E501


        :return: The delta_period of this ProjectVersionState.  # noqa: E501
        :rtype: int
        """
        return self._delta_period

    @delta_period.setter
    def delta_period(self, delta_period):
        """Sets the delta_period of this ProjectVersionState.


        :param delta_period: The delta_period of this ProjectVersionState.  # noqa: E501
        :type: int
        """
        if delta_period is None:
            raise ValueError("Invalid value for `delta_period`, must not be `None`")  # noqa: E501

        self._delta_period = delta_period

    @property
    def extra_message(self):
        """Gets the extra_message of this ProjectVersionState.  # noqa: E501


        :return: The extra_message of this ProjectVersionState.  # noqa: E501
        :rtype: str
        """
        return self._extra_message

    @extra_message.setter
    def extra_message(self, extra_message):
        """Sets the extra_message of this ProjectVersionState.


        :param extra_message: The extra_message of this ProjectVersionState.  # noqa: E501
        :type: str
        """
        if extra_message is None:
            raise ValueError("Invalid value for `extra_message`, must not be `None`")  # noqa: E501

        self._extra_message = extra_message

    @property
    def has_custom_issues(self):
        """Gets the has_custom_issues of this ProjectVersionState.  # noqa: E501


        :return: The has_custom_issues of this ProjectVersionState.  # noqa: E501
        :rtype: bool
        """
        return self._has_custom_issues

    @has_custom_issues.setter
    def has_custom_issues(self, has_custom_issues):
        """Sets the has_custom_issues of this ProjectVersionState.


        :param has_custom_issues: The has_custom_issues of this ProjectVersionState.  # noqa: E501
        :type: bool
        """
        if has_custom_issues is None:
            raise ValueError("Invalid value for `has_custom_issues`, must not be `None`")  # noqa: E501

        self._has_custom_issues = has_custom_issues

    @property
    def id(self):
        """Gets the id of this ProjectVersionState.  # noqa: E501


        :return: The id of this ProjectVersionState.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ProjectVersionState.


        :param id: The id of this ProjectVersionState.  # noqa: E501
        :type: int
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def issue_count_delta(self):
        """Gets the issue_count_delta of this ProjectVersionState.  # noqa: E501


        :return: The issue_count_delta of this ProjectVersionState.  # noqa: E501
        :rtype: int
        """
        return self._issue_count_delta

    @issue_count_delta.setter
    def issue_count_delta(self, issue_count_delta):
        """Sets the issue_count_delta of this ProjectVersionState.


        :param issue_count_delta: The issue_count_delta of this ProjectVersionState.  # noqa: E501
        :type: int
        """
        if issue_count_delta is None:
            raise ValueError("Invalid value for `issue_count_delta`, must not be `None`")  # noqa: E501

        self._issue_count_delta = issue_count_delta

    @property
    def last_fpr_upload_date(self):
        """Gets the last_fpr_upload_date of this ProjectVersionState.  # noqa: E501


        :return: The last_fpr_upload_date of this ProjectVersionState.  # noqa: E501
        :rtype: datetime
        """
        return self._last_fpr_upload_date

    @last_fpr_upload_date.setter
    def last_fpr_upload_date(self, last_fpr_upload_date):
        """Sets the last_fpr_upload_date of this ProjectVersionState.


        :param last_fpr_upload_date: The last_fpr_upload_date of this ProjectVersionState.  # noqa: E501
        :type: datetime
        """
        if last_fpr_upload_date is None:
            raise ValueError("Invalid value for `last_fpr_upload_date`, must not be `None`")  # noqa: E501

        self._last_fpr_upload_date = last_fpr_upload_date

    @property
    def metric_evaluation_date(self):
        """Gets the metric_evaluation_date of this ProjectVersionState.  # noqa: E501


        :return: The metric_evaluation_date of this ProjectVersionState.  # noqa: E501
        :rtype: datetime
        """
        return self._metric_evaluation_date

    @metric_evaluation_date.setter
    def metric_evaluation_date(self, metric_evaluation_date):
        """Sets the metric_evaluation_date of this ProjectVersionState.


        :param metric_evaluation_date: The metric_evaluation_date of this ProjectVersionState.  # noqa: E501
        :type: datetime
        """
        if metric_evaluation_date is None:
            raise ValueError("Invalid value for `metric_evaluation_date`, must not be `None`")  # noqa: E501

        self._metric_evaluation_date = metric_evaluation_date

    @property
    def percent_audited_delta(self):
        """Gets the percent_audited_delta of this ProjectVersionState.  # noqa: E501


        :return: The percent_audited_delta of this ProjectVersionState.  # noqa: E501
        :rtype: float
        """
        return self._percent_audited_delta

    @percent_audited_delta.setter
    def percent_audited_delta(self, percent_audited_delta):
        """Sets the percent_audited_delta of this ProjectVersionState.


        :param percent_audited_delta: The percent_audited_delta of this ProjectVersionState.  # noqa: E501
        :type: float
        """
        if percent_audited_delta is None:
            raise ValueError("Invalid value for `percent_audited_delta`, must not be `None`")  # noqa: E501

        self._percent_audited_delta = percent_audited_delta

    @property
    def percent_critical_priority_issues_audited_delta(self):
        """Gets the percent_critical_priority_issues_audited_delta of this ProjectVersionState.  # noqa: E501


        :return: The percent_critical_priority_issues_audited_delta of this ProjectVersionState.  # noqa: E501
        :rtype: float
        """
        return self._percent_critical_priority_issues_audited_delta

    @percent_critical_priority_issues_audited_delta.setter
    def percent_critical_priority_issues_audited_delta(self, percent_critical_priority_issues_audited_delta):
        """Sets the percent_critical_priority_issues_audited_delta of this ProjectVersionState.


        :param percent_critical_priority_issues_audited_delta: The percent_critical_priority_issues_audited_delta of this ProjectVersionState.  # noqa: E501
        :type: float
        """
        if percent_critical_priority_issues_audited_delta is None:
            raise ValueError("Invalid value for `percent_critical_priority_issues_audited_delta`, must not be `None`")  # noqa: E501

        self._percent_critical_priority_issues_audited_delta = percent_critical_priority_issues_audited_delta

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ProjectVersionState, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ProjectVersionState):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
