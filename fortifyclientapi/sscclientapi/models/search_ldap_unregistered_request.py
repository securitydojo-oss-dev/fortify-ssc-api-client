# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class SearchLdapUnregisteredRequest(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'filter': 'str',
        'ldap_type': 'str',
        'limit': 'int',
        'start': 'int'
    }

    attribute_map = {
        'filter': 'filter',
        'ldap_type': 'ldapType',
        'limit': 'limit',
        'start': 'start'
    }

    def __init__(self, filter=None, ldap_type=None, limit=None, start=None):  # noqa: E501
        """SearchLdapUnregisteredRequest - a model defined in Swagger"""  # noqa: E501
        self._filter = None
        self._ldap_type = None
        self._limit = None
        self._start = None
        self.discriminator = None
        if filter is not None:
            self.filter = filter
        self.ldap_type = ldap_type
        if limit is not None:
            self.limit = limit
        if start is not None:
            self.start = start

    @property
    def filter(self):
        """Gets the filter of this SearchLdapUnregisteredRequest.  # noqa: E501

        The specified query string is matched against the name, firstName, lastName, and email fields for LDAP User entities. For Group and OrgUnit entities, only the name is matched. The query string must match the entire field value. For partial matches you can add asterisk ('*') wildcards to your query.  # noqa: E501

        :return: The filter of this SearchLdapUnregisteredRequest.  # noqa: E501
        :rtype: str
        """
        return self._filter

    @filter.setter
    def filter(self, filter):
        """Sets the filter of this SearchLdapUnregisteredRequest.

        The specified query string is matched against the name, firstName, lastName, and email fields for LDAP User entities. For Group and OrgUnit entities, only the name is matched. The query string must match the entire field value. For partial matches you can add asterisk ('*') wildcards to your query.  # noqa: E501

        :param filter: The filter of this SearchLdapUnregisteredRequest.  # noqa: E501
        :type: str
        """

        self._filter = filter

    @property
    def ldap_type(self):
        """Gets the ldap_type of this SearchLdapUnregisteredRequest.  # noqa: E501

        Type of ldap objects to be matched  # noqa: E501

        :return: The ldap_type of this SearchLdapUnregisteredRequest.  # noqa: E501
        :rtype: str
        """
        return self._ldap_type

    @ldap_type.setter
    def ldap_type(self, ldap_type):
        """Sets the ldap_type of this SearchLdapUnregisteredRequest.

        Type of ldap objects to be matched  # noqa: E501

        :param ldap_type: The ldap_type of this SearchLdapUnregisteredRequest.  # noqa: E501
        :type: str
        """
        if ldap_type is None:
            raise ValueError("Invalid value for `ldap_type`, must not be `None`")  # noqa: E501
        allowed_values = ["USER", "GROUP", "ORG_UNIT"]  # noqa: E501
        if ldap_type not in allowed_values:
            raise ValueError(
                "Invalid value for `ldap_type` ({0}), must be one of {1}"  # noqa: E501
                .format(ldap_type, allowed_values)
            )

        self._ldap_type = ldap_type

    @property
    def limit(self):
        """Gets the limit of this SearchLdapUnregisteredRequest.  # noqa: E501

        A maximum number of returned objects in listing, if '-1' no limit is applied  # noqa: E501

        :return: The limit of this SearchLdapUnregisteredRequest.  # noqa: E501
        :rtype: int
        """
        return self._limit

    @limit.setter
    def limit(self, limit):
        """Sets the limit of this SearchLdapUnregisteredRequest.

        A maximum number of returned objects in listing, if '-1' no limit is applied  # noqa: E501

        :param limit: The limit of this SearchLdapUnregisteredRequest.  # noqa: E501
        :type: int
        """

        self._limit = limit

    @property
    def start(self):
        """Gets the start of this SearchLdapUnregisteredRequest.  # noqa: E501

        A start offset in object listing  # noqa: E501

        :return: The start of this SearchLdapUnregisteredRequest.  # noqa: E501
        :rtype: int
        """
        return self._start

    @start.setter
    def start(self, start):
        """Sets the start of this SearchLdapUnregisteredRequest.

        A start offset in object listing  # noqa: E501

        :param start: The start of this SearchLdapUnregisteredRequest.  # noqa: E501
        :type: int
        """

        self._start = start

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(SearchLdapUnregisteredRequest, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, SearchLdapUnregisteredRequest):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
