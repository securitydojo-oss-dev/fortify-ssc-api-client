# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class SearchIndexStatus(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'configured': 'bool',
        'healthy_index': 'bool',
        'indexing_job_running': 'bool'
    }

    attribute_map = {
        'configured': 'configured',
        'healthy_index': 'healthyIndex',
        'indexing_job_running': 'indexingJobRunning'
    }

    def __init__(self, configured=None, healthy_index=None, indexing_job_running=None):  # noqa: E501
        """SearchIndexStatus - a model defined in Swagger"""  # noqa: E501
        self._configured = None
        self._healthy_index = None
        self._indexing_job_running = None
        self.discriminator = None
        if configured is not None:
            self.configured = configured
        if healthy_index is not None:
            self.healthy_index = healthy_index
        if indexing_job_running is not None:
            self.indexing_job_running = indexing_job_running

    @property
    def configured(self):
        """Gets the configured of this SearchIndexStatus.  # noqa: E501

        Is index configured  # noqa: E501

        :return: The configured of this SearchIndexStatus.  # noqa: E501
        :rtype: bool
        """
        return self._configured

    @configured.setter
    def configured(self, configured):
        """Sets the configured of this SearchIndexStatus.

        Is index configured  # noqa: E501

        :param configured: The configured of this SearchIndexStatus.  # noqa: E501
        :type: bool
        """

        self._configured = configured

    @property
    def healthy_index(self):
        """Gets the healthy_index of this SearchIndexStatus.  # noqa: E501

        Is the index Healthy  # noqa: E501

        :return: The healthy_index of this SearchIndexStatus.  # noqa: E501
        :rtype: bool
        """
        return self._healthy_index

    @healthy_index.setter
    def healthy_index(self, healthy_index):
        """Sets the healthy_index of this SearchIndexStatus.

        Is the index Healthy  # noqa: E501

        :param healthy_index: The healthy_index of this SearchIndexStatus.  # noqa: E501
        :type: bool
        """

        self._healthy_index = healthy_index

    @property
    def indexing_job_running(self):
        """Gets the indexing_job_running of this SearchIndexStatus.  # noqa: E501

        Is the indexing job running  # noqa: E501

        :return: The indexing_job_running of this SearchIndexStatus.  # noqa: E501
        :rtype: bool
        """
        return self._indexing_job_running

    @indexing_job_running.setter
    def indexing_job_running(self, indexing_job_running):
        """Sets the indexing_job_running of this SearchIndexStatus.

        Is the indexing job running  # noqa: E501

        :param indexing_job_running: The indexing_job_running of this SearchIndexStatus.  # noqa: E501
        :type: bool
        """

        self._indexing_job_running = indexing_job_running

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(SearchIndexStatus, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, SearchIndexStatus):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
